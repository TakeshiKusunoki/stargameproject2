#pragma once
#include "Scene.h"
#include "Lib_3D\sprite2D.h"
class SceneTitle : public Scene, public Singleton<SceneTitle>
{
	Lib_3D::Sprite2D* spr[100];
public:

	void Init()override;
	BaseLoop* LoopFunc(BaseLoop* loopFunc)override;
	void Draw()override;
	void UnInit()override;
};
#define pSceneTitle SceneTitle::getInstance();

#pragma once
//#include <DirectXMath.h>
//#include <math.h>
#include "Lib_Vassel/Wrapped_Vassel.h"
#include "Lib_Render/Wrapped_Render.h"
#include "Lib_3D/Wrapped.h"
#include "Common.h"
#include "Lib_Vassel/LocationManager.h"
//#include "ItemObject.h"

using Lib_Vassel::BaseObject;
//1:2:√3
class BaseObject;
#define TRIANGLE_Y(x) (x*static_cast<float>(sqrt(3)))


	class DummyObject : public BaseObject
	{
	public:
		DummyObject() : BaseObject()
		{}
		DummyObject(const DummyObject&) {}
		DummyObject(DummyObject&&) {}
		~DummyObject(){}
	private:
	};

	/*template<class T>
	class ItemManager
	{
	public:
		std::list<T*> _itemList;
	public:
		ItemManager();
		~ItemManager();
		void Add(T* item)
		{
			_itemList.push_back(item);
		}
	private:

	};*/

	struct PoligonObject : public BaseObject
	{
		//friend class ItemObject;
	protected:
		static const size_t VERTEX_NUM = 3;
		static const size_t RANGE = 10;//１っ辺の長さ
		static const size_t MAX = 5;//周りのポリゴンの数
	private:
		struct PoligonObjectData
		{
			VertexPoligon3D* _pVertexData;
			DirectX::XMFLOAT3 _vertex[VERTEX_NUM];//! 頂点
			DirectX::XMFLOAT2 _texpos[VERTEX_NUM];//! テクスチャ
			DirectX::XMFLOAT3	_pos;				 //! 真ん中の位置
			DirectX::XMFLOAT3	_scale;				 //! 大きさ
			DirectX::XMFLOAT3 _angle;
			DirectX::XMVECTOR _orienttion;//! 姿勢行列のためのクォータニオン
			DirectX::XMVECTOR _localOrientation;//! ローカル回転角
			DirectX::XMFLOAT3 _localPos;//! ローカル位置座標
		};
	public:
		PoligonObjectData _data;
	};



	//! @brief 頂点が可変する三角形ポリゴン
	//! @details 当たったアイテムの頂点の大きさまで頂点を伸ばす
	//! 伸びきったらそのインスタンスを破棄する
	//! 途中でほかのアイテムに当たったら今持ってる、リストを破棄する
	class PlayerPoligonValiable : public PoligonObject
	{
		friend class PlayerPoligon;
	private:
		static constexpr size_t TopVertex = 2;
		std::list<DummyObject> _itemList;//! くっついたポリゴンのリスト(BaseObjectManagerからこのクラスの実体に管理を移行する)
		PlayerPoligon* _pPlayerPoligon;
	public:
		PlayerPoligonValiable();
		PlayerPoligonValiable(const PlayerPoligonValiable&) {}
		PlayerPoligonValiable(PlayerPoligonValiable&&);

		~PlayerPoligonValiable();

	public:
		void Initialize()override;

		void UnInitialize()override;


	private:
		//! 変数の初期化
		void Init_();

		void MultiTask()override;

		//! @brief 姿勢制御
		void OrientationCaluculate();

		//! メッセージ処理
		void MsgCmnd();

		//! 姿勢を与える(姿勢をアイテムリストに伝播させる)
		//! @details このクラスの実態をハックさせる
		void LetOrientasion();

		//! このクラスの実体が管理するアイテムリストから情報を得る
		void ItemListHucked();

		void HuckPlayerPoligonObject(PlayerPoligon* obj);

	};




















	//! @brief 作成されたと同時にObjRenderクラスリストにrenderObjを追加する
	//! @details プレイヤーの周りについてる三角ポリゴン
	class PlayerPoligon : public PoligonObject
	{
		friend class PlayerPiliod;
		friend class PlayerPoligonValiable;
	private:
		PlayerPiliod* _pPlayerPiliod;//! プレイヤーからハック
		std::list<PlayerPoligonValiable> _itemList;//! くっついたポリゴンのリスト(BaseObjectManagerからこのクラスの実体に管理を移行する)
		static size_t callCount;//呼ばれた回数
	public:
		PlayerPoligon();
		PlayerPoligon(const PlayerPoligon&){}
		// ムーブコンストラクタありの場合
		PlayerPoligon(PlayerPoligon&& o);

		~PlayerPoligon();


	public:
		void Initialize()override;

		void UnInitialize()override;

		void LetData()override;
	private:
		//! 変数の初期化
		void Init_();

		void MultiTask()override;
		//! メッセージ処理
		void MsgCmnd();
		//! @brief 姿勢制御
		void OrientationCaluculate();

		//! 姿勢を与える(姿勢をアイテムリストに伝播させる)
		//! @details このクラスの実態をハックさせる
		void LetOrientasion();

		//! @brief プレイヤーからハック
		void HuckPlayerObject(PlayerPiliod* obj);

	};

















	//! @brief プレイヤー頂点
	class PlayerPiliod : public PoligonObject
	{
		friend class PlayerPoligon;
	private:
		std::list<PlayerPoligon> _itemList;//! くっついたポリゴンのリスト(BaseObjectManagerからこのクラスの実体に管理を移行する)

	public:
		PlayerPiliod();
		PlayerPiliod(const  PlayerPiliod&){}
		PlayerPiliod(PlayerPiliod&&);
		~PlayerPiliod();

	public:
		//! @brief コンストラクタが呼ばれた後。別に呼ぶ
		void Initialize()override;


		void UnInitialize()override;

		void LetData()override;
	private:
		//! 変数の初期化
		void Init_();


		void MultiTask()override;
		//! メッセージ処理
		void MsgCmnd();

		//! @brief 姿勢制御
		void OrientationCaluculate();

		//! アイテムリストのオブジェクトにデータを参照させる
		void LetOrientasion();

	};









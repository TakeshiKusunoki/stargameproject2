#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <Windows.h>
#include "SceneTitle.h"
#include "SceneGame.h"
#include "Lib_Base\Wrapped_Base.h"
#include "Lib_3D\Wrapped.h"

void SceneTitle::Init()
{
	for (int i = 0; i < 100; i++)
	{
		spr[i] = nullptr;
	}
	spr[0] = new Lib_3D::Sprite2D(Lib_3D::pDirectXWindowManager->GetDevice(), L"DATA\\picture\\title.png");
}

BaseLoop * SceneTitle::LoopFunc(BaseLoop * loopFunc)
{
	if (GetAsyncKeyState(VK_SPACE)< 0)
	{
		return pSceneGame;
	}
	//spr[0]->Render3(Lib_3D::pDirectXWindowManager->GetDeviceContext(), 0, 0, 0, 1, 1, 0, 0, 596, 843);

	return this;
}

void SceneTitle::Draw()
{
	auto win = Lib_Base::pWindowCreateManager->GetWindowWithClassName(WINMAIN_CLASS_NAME);
	HWND hwnd = win->GetHWnd();

#define VIEW_NUM 1
	// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
	D3D11_VIEWPORT Viewport[VIEW_NUM];
	Viewport[0].TopLeftX = 0;
	Viewport[0].TopLeftY = 0;
	Viewport[0].Width = static_cast<FLOAT>(Lib_3D::pDirectXWindowManager->GetScreenWidth(hwnd));
	Viewport[0].Height = static_cast<FLOAT>(Lib_3D::pDirectXWindowManager->GetScreenHeight(hwnd));//1lo0O10O0O8sSBloO
	Viewport[0].MinDepth = 0.0f;
	Viewport[0].MaxDepth = 1.0f;

	// 描画ターゲット・ビューの設定
	Lib_3D::pDirectXWindowManager->GetDeviceContext()->OMSetRenderTargets(VIEW_NUM, Lib_3D::pDirectXWindowManager->GetRenderTargetViewA(hwnd), Lib_3D::pDirectXWindowManager->GetDepthStencilView(hwnd));
	Lib_3D::pDirectXWindowManager->GetDeviceContext()->RSSetViewports(VIEW_NUM, Viewport);



#undef VIEW_NUM
	// 描画ターゲットのクリア
	float ClearColor[4] = { 0.0f,255.0f,0.0f,0.0f };//クリアする値
	Lib_3D::pDirectXWindowManager->GetDeviceContext()->ClearRenderTargetView(Lib_3D::pDirectXWindowManager->GetRenderTargetView(hwnd), ClearColor);
	// 深度ステンシル リソースをクリアします。
	Lib_3D::pDirectXWindowManager->GetDeviceContext()->ClearDepthStencilView(Lib_3D::pDirectXWindowManager->GetDepthStencilView(hwnd), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	spr[0]->Render2(Lib_3D::pDirectXWindowManager->GetDeviceContext(), 0, 0, 0, 0, 596, 843, 1.7f, 1.2f);
	Lib_3D::pDirectXWindowManager->GetSwapChain(hwnd)->Present(0, 0);
}

void SceneTitle::UnInit()
{
	for (int i = 0; i < 100; i++)
	{
		if (spr[i])
			delete spr[i];
		spr[i] = nullptr;
	}
}

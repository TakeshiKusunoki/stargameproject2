#include "ObjectPoligon.h"
#include "Lib_Vassel\Renderer.h"
#include "Common.h"

size_t  PlayerPoligon::callCount = 0;

PlayerPoligonValiable::PlayerPoligonValiable()
{
	//Lib_Vassel::pRendererManager->Add(this);
	Init();
	Init_();

}

PlayerPoligonValiable::PlayerPoligonValiable(PlayerPoligonValiable &&)
{
	//Initialize();
}

PlayerPoligonValiable::~PlayerPoligonValiable()
{
	//UnInitialize();
}

void PlayerPoligonValiable::Initialize()
{
	const float RANGE_ = (RANGE >> 1);//辺の半分
	const float POST = TRIANGLE_Y((RANGE_)) * 2;//高さ

	DirectX::XMMATRIX m = DirectX::XMMatrixRotationQuaternion(_data._orienttion);
	VECTOR3 axisZ(m.r[2].m128_f32[0], m.r[2].m128_f32[1], m.r[2].m128_f32[2]);//y軸
	DirectX::XMVECTOR axis = DirectX::XMLoadFloat3(&axisZ);
	VECTOR3 pos(0, 0, 0);//位置座標

						 // 方向分
	{
		pos.y += POST / 300;
		DirectX::XMVECTOR qurtnion = DirectX::XMQuaternionIdentity();//姿勢
		qurtnion = _pPlayerPoligon->_data._orienttion;
		DirectX::XMVECTOR pos_ = pos;

		pos = pos_;
		pos += _pPlayerPoligon->_data._pos;
		DirectX::XMVECTOR q = DirectX::XMQuaternionRotationAxis(axis, (180 * 0.01745f));
		qurtnion = DirectX::XMQuaternionMultiply(qurtnion, q);
		pos_ = DirectX::XMVector3Rotate(pos_, qurtnion);

		_data._scale = _pPlayerPoligon->_data._scale;
		_data._orienttion = qurtnion;
		_data._pos = pos;
		_data._localPos = _data._pos;
		_data._localOrientation = _data._orienttion;
		//周りのポリゴン座標
		/*for (size_t i2 = 0; i2 < VERTEX_NUM; i2++)
		{
			_data._vertex[i2] = _pPlayerPoligon->_data._vertex[i2];
			_data._texpos[i2] = _pPlayerPoligon->_data._texpos[i2];
		}*/
		_data._vertex[2].y /= 5;//頂点をちじめる
		_data._vertex[0].x *= 2;
		_data._vertex[1].x *= 2;
		//
		_data._pVertexData = _pPlayerPoligon->_data._pVertexData;
		_public._label = _pPlayerPoligon->_public._label;

	}
	if (!_protected._pRenderer && _data._pVertexData)
	{
		_protected._pRenderer = new Lib_Render::RenderPoligonMesh(_data._pVertexData, _data._vertex, _data._texpos, &_data._pos, &_data._orienttion, &_data._scale);
		Lib_Vassel::pRendererManager->Add(_protected._pRenderer);
	}

	/*for (auto& it : _itemList)
	{
		it.Initialize();
	}*/
}

void PlayerPoligonValiable::UnInitialize()
{
	_itemList.clear();

	if (_protected._pRenderer)
	{
		delete _protected._pRenderer;
		_protected._pRenderer = nullptr;
	}
}

void PlayerPoligonValiable::Init_()
{
	SecureZeroMemory(&_data, sizeof(_data));
	_itemList.clear();
	_pPlayerPoligon = nullptr;

	_data._pos = VECTOR3(0, 0, 0);
	_data._scale = VECTOR3(1, 1, 1);
	_data._orienttion = DirectX::XMQuaternionIdentity();

	//頂点座標定義
	const float POST = TRIANGLE_Y((10 >> 1));//高さ
	VECTOR3 v[VERTEX_NUM] = {//頂点▲左１右２頂点３
		VECTOR3(-static_cast<float>(10 >> 1) * 10,(POST / 2) * 10 ,0),
		VECTOR3((10 >> 1) * 10,(POST / 2) * 10,0),
		VECTOR3(0     ,-(POST / 2) * 10 ,0)
		/*VECTOR3(100,100,0),
		VECTOR3(0 ,0,0),
		VECTOR3(200     ,0,0)*/
	};
	//テクスチャ座標定義
	VECTOR2 t[VERTEX_NUM] = { VECTOR2(200,0),VECTOR2(120,200),VECTOR2(280,200) };//テクスチャ
	for (size_t i = 0; i < VERTEX_NUM; i++)
	{
		_data._vertex[i] = v[i];
		_data._texpos[i] = t[i];
		_public._vertexList.push_back(&_data._vertex[i]);
	}
	_public._position.second = (&_data._pos);
	//
	_public._label = (USER::LABEL[Enum::PLAYER]);//プレイヤーラベル
}

void PlayerPoligonValiable::MultiTask()
{
	static int timer = 0;
	printf("PlayerPoligonValiable\t%d\n", ++timer);
	OrientationCaluculate();//このオブジェクトの姿勢制御
	MsgCmnd();//メッセージを受け取った後の処理
	LetOrientasion();//アイテムリストに情報を与える、そしてアイテムリストの更新処理を行う
	ItemListHucked();//アイテムリストからこのオブジェクトが受ける影響
}

void PlayerPoligonValiable::OrientationCaluculate()
{
	//回転
	{
		_data._orienttion = DirectX::XMQuaternionMultiply(_data._localOrientation, _pPlayerPoligon->_data._orienttion);
		DirectX::XMVECTOR thisObjLocalPos = DirectX::XMLoadFloat3(&_data._localPos);
		DirectX::XMVECTOR afterRoatedPos = DirectX::XMVector3Rotate(thisObjLocalPos, _pPlayerPoligon->_data._orienttion);// 親分の回転
																														//位置
		{
			VECTOR3 targetPos = _pPlayerPoligon->_data._pos;// 親の位置座標
			VECTOR3 afterRoatedPos_;
			DirectX::XMStoreFloat3(&afterRoatedPos_, afterRoatedPos);
			VECTOR3 afterMovedPos = targetPos + afterRoatedPos_;// 動いた後の位置
			_data._pos = afterMovedPos;
		}
	}

}

void PlayerPoligonValiable::MsgCmnd()
{
	//アイテムオブジェクトの管理を奪う
	if (_public._msg == USER::MSG[Enum::HIT_ITEM])
	{
		//くっつくのは一つまで
		//for (auto& it : _itemList)
		//{
		//	//it.SetLabel(nullptr);
		//}
		_itemList.clear();
		ShowMsg();
		//DummyObject nuu;//= new DummyObject;//ダミーオブジェクト
		//_public._pBaseObjectHucked->SwapInstance(&nuu);//_pBaseObjectHuckedとnuuの_my_instanceを入れ替える//managerのほうのインスタンスモードで呼ぶのがnuuになる
		//nuu.SetLabel(_public._label);//ラベルをプレイヤーに変更
		//_itemList.push_back(nuu);//くっついたオブジェクトのインスタンスを持った、器をリストに加える
		_itemList.emplace_back();

		_public._pBaseObjectHucked->SwapInstance(&(*_itemList.begin()));
		_itemList.begin()->SetLabel(_public._label);
		_itemList.begin()->HuckBaseObject(this);
	}
	if (_public._msg == USER::MSG[Enum::HIT_ENEMY])
	{
		ShowMsg();
	}
	_public._msg = nullptr;
	_public._pBaseObjectHucked = nullptr;
	printf("%s\n", _public._msg);
}

void PlayerPoligonValiable::LetOrientasion()
{
	for (auto& it : _itemList)
	{
		it.HuckBaseObjectVassel(this);
		it.LoopFunc(nullptr);
	}
}

void PlayerPoligonValiable::ItemListHucked()
{
	if (_itemList.size() == 0)
		return;
	//posから一番遠い頂点へのびる
	size_t num = 0;// posから一番遠い頂点の番号
	for (auto& it : _itemList)//アイテムリストは常に１個だろう・・
	{
		auto itemVertexList = it.GetVertxList();

		float  mostFarVertex = 0;
		for (size_t i = 0; i < itemVertexList.size(); i++)
		{
			//１番遠い頂点を調べる
			VECTOR3 VectorTwoVertexInterval = static_cast<VECTOR3>(*itemVertexList[i]) - _data._vertex[TopVertex];
			float l = VectorTwoVertexInterval.Length();
			if (l > mostFarVertex)
			{
				num = i;
				mostFarVertex = l;
			}
		}
	}
	VECTOR3 ShouldExtendToVertexPosition;// 伸びるべき頂点座標
	for (auto& it : _itemList)
	{
		auto itemVertexList = it.GetVertxList();
		if (itemVertexList.size()==0)
			break;
		ShouldExtendToVertexPosition = (VECTOR3)((VECTOR3)_data._pos * 1000)+ *itemVertexList.at(num) ;
	}
	//頂点位置を毎フレーム徐々に伸ばす
	VECTOR3 VectorOfDistanceOfToTarget = ShouldExtendToVertexPosition - _data._vertex[TopVertex];// ターゲットへの差分ベクトル
	//伸ばす
	//(VECTOR3)_data._vertex[TopVertex] += VectorOfDistanceOfToTarget.Normalize();
	_data._vertex[TopVertex].x += VectorOfDistanceOfToTarget.Normalize().x;
	_data._vertex[TopVertex].y += VectorOfDistanceOfToTarget.Normalize().y;

	//のびきったら
	if ((VECTOR3)((VECTOR3)_data._vertex[TopVertex] + (VECTOR3)_data._pos * 1000) == (VECTOR3)ShouldExtendToVertexPosition)
	{
		/*for (auto& it : _itemList)
		{
			it.SetLabel(nullptr);
		}*/
		_itemList.clear();
	}
	//(VECTOR3)*_public._vertexList.at(TopVertex) += VectorOfDistanceOfToTarget.Normalize();
}

void PlayerPoligonValiable::HuckPlayerPoligonObject(PlayerPoligon * obj)
{
	_pPlayerPoligon = obj;
	_public._pLom = obj->_public._pLom;
	//_public._msg = _pPlayerPoligon->_public._msg;
	//HuckBaseObjectVassel(obj->_public._pBaseObjectHucked);
}











PlayerPoligon::PlayerPoligon()
{
	Init();
	Init_();
	//Initialize();
}

PlayerPoligon::PlayerPoligon(PlayerPoligon && )
{
	//Initialize();
}

PlayerPoligon::~PlayerPoligon()
{
	//UnInitialize();
}

void PlayerPoligon::Initialize()
{

	_data._pVertexData = Lib_Render::LoderPoligonMesh::getInstance()->GetMeshData(USER::poligon_filename[0]);

	//位置を求める
	//プレイやー周りのポリゴンのデータを作りアイテムリストに保管
	//PlayerPoligon plPoligon[MAX] = {};//プレイヤー点の周りのポリゴン
	{
		const float RANGE_ = (RANGE >> 1);//辺の半分
		const float POST = TRIANGLE_Y((RANGE_));//高さ

		DirectX::XMMATRIX m = DirectX::XMMatrixRotationQuaternion(_pPlayerPiliod->_data._orienttion);
		VECTOR3 axisZ(m.r[2].m128_f32[0], m.r[2].m128_f32[1], m.r[2].m128_f32[2]);//y軸
		DirectX::XMVECTOR axis = DirectX::XMLoadFloat3(&axisZ);
		VECTOR3 pos;//位置座標

				//上から時計回り
				//for (size_t i = 0; i < MAX; i++)
		pos = _pPlayerPiliod->_data._pos;
		pos.y += POST / 200;
		DirectX::XMVECTOR qurtnion;//姿勢
		qurtnion = _pPlayerPiliod->_data._orienttion;
		DirectX::XMVECTOR pos_ = DirectX::XMLoadFloat3(&pos);
		DirectX::XMVECTOR q = DirectX::XMQuaternionRotationAxis(axis, (callCount * 72 * 0.01745f));
		qurtnion = DirectX::XMQuaternionMultiply(qurtnion, q);
		pos_ = DirectX::XMVector3Rotate(pos_, qurtnion);
		DirectX::XMStoreFloat3(&pos, pos_);


		//PlayerPoligon* p = new PlayerPoligon(_public._pLom);
		//_data._pos = _pPlayerPiliod->_data._pos;
		_data._scale = _pPlayerPiliod->_data._scale;
		_data._orienttion = qurtnion;
		_data._pos = pos + _pPlayerPiliod->_data._pos;
		_data._localPos = _data._pos;
		_data._localOrientation = _data._orienttion;
		//周りのポリゴン座標
		for (size_t i2 = 0; i2 < VERTEX_NUM; i2++)
		{
			_data._vertex[i2] = _pPlayerPiliod->_data._vertex[i2];
			_data._texpos[i2] = _pPlayerPiliod->_data._texpos[i2];
		}
		//
		_data._pVertexData = _pPlayerPiliod->_data._pVertexData;
		_public._label = _pPlayerPiliod->_public._label;


	}
	//rendererにadd
	if (!_protected._pRenderer && _data._pVertexData)
	{
		_protected._pRenderer = new Lib_Render::RenderPoligonMesh(_data._pVertexData, _data._vertex, _data._texpos, &_data._pos, &_data._orienttion, &_data._scale);
		Lib_Vassel::pRendererManager->Add(_protected._pRenderer);
	}

	for (auto& it : _itemList)
	{
		it.HuckPlayerPoligonObject(this);
		it.Initialize();
	}

	callCount++;


	/*for (auto& it : _itemList)
	{
		if (!it->_protected._pRenderer && it->_data._pVertexData)
			it->_protected._pRenderer = new Lib_Render::RenderPoligonMesh(it->_data._pVertexData, it->_data._vertex, it->_data._texpos, &it->_data._pos, &it->_data._orienttion, &it->_data._scale);
		Lib_Vassel::pRendererManager->Add(it->_protected._pRenderer);
	}*/
}

void PlayerPoligon::UnInitialize()
{
	for (auto& it : _itemList)
	{
		it.UnInitialize();
	}
	_itemList.clear();
	if (_protected._pRenderer)
	{
		delete _protected._pRenderer;
		_protected._pRenderer = nullptr;
	}

}

void PlayerPoligon::LetData()
{
	LetData_();
	for (auto& it : _itemList)
	{
		it.LetData();
	}
}

void PlayerPoligon::Init_()
{
	SecureZeroMemory(&_data, sizeof(_data));
	_itemList.clear();
	_pPlayerPiliod = nullptr;

	_data._pos = VECTOR3(0, 0, 0);
	_data._scale = VECTOR3(1, 1, 1);
	_data._orienttion = DirectX::XMQuaternionIdentity();
	DirectX::XMQuaternionIdentity();


	//頂点座標定義
	const float POST = TRIANGLE_Y((10 >> 1));//高さ
	VECTOR3 v[VERTEX_NUM] = {//頂点▲右上左
		VECTOR3(-static_cast<float>(10 >> 1) * 10,(POST / 2) * 10 ,0),
		VECTOR3((10 >> 1) * 10,(POST / 2) * 10,0),
		VECTOR3(0     ,-(POST / 2) * 10 ,0)
		/*VECTOR3(100,100,0),
		VECTOR3(0 ,0,0),
		VECTOR3(200     ,0,0)*/
	};
	//テクスチャ座標定義
	VECTOR2 t[VERTEX_NUM] = { VECTOR2(200,0),VECTOR2(120,200),VECTOR2(280,200) };//テクスチャ
	for (size_t i = 0; i < VERTEX_NUM; i++)
	{
		_data._vertex[i] = v[i];
		_data._texpos[i] = t[i];
		_public._vertexList.push_back(&_data._vertex[i]);
	}
	_public._position.second = (&_data._pos);
	//
	_public._label = (USER::LABEL[Enum::PLAYER]);//プレイヤーラベル

	//リストにプッシュ
	{
		//PlayerPoligonValiable p;//コンストラクタ呼び出し
		//_itemList.push_back(p);
		_itemList.emplace_back();
	}

}

void PlayerPoligon::MultiTask()
{
	static int timer = 0;
	printf(" PlayerPoligon\t%d\n", ++timer);
	OrientationCaluculate();
	LetOrientasion();
}

void PlayerPoligon::MsgCmnd()
{
}

void PlayerPoligon::OrientationCaluculate()
{
	//回転
	{
		_data._orienttion = DirectX::XMQuaternionMultiply(_data._localOrientation, _pPlayerPiliod->_data._orienttion);
		DirectX::XMVECTOR thisObjLocalPos = DirectX::XMLoadFloat3(&_data._localPos);
		DirectX::XMVECTOR afterRoatedPos = DirectX::XMVector3Rotate(thisObjLocalPos, _pPlayerPiliod->_data._orienttion);// 親分の回転
		//位置
		{
			VECTOR3 targetPos = _pPlayerPiliod->_data._pos;// 親の位置座標
			VECTOR3 afterRoatedPos_;
			DirectX::XMStoreFloat3(&afterRoatedPos_, afterRoatedPos);
			VECTOR3 afterMovedPos = targetPos + afterRoatedPos_;// 動いた後の位置
			_data._pos = afterMovedPos;
		}
	}

}

void PlayerPoligon::LetOrientasion()
{
	for (auto& it : _itemList)
	{
		it.HuckPlayerPoligonObject(this);
		it.LoopFunc(nullptr);
		//it.LetData();
	}
}

void PlayerPoligon::HuckPlayerObject(PlayerPiliod * obj)
{
	_pPlayerPiliod = obj;
	_public._pLom = obj->_public._pLom;
	//_public._msg = _pPlayerPiliod->_public._msg;
	//HuckBaseObjectVassel(obj->_public._pBaseObjectHucked);
}


















PlayerPiliod::PlayerPiliod()
{
	Init();
	Init_();
}

PlayerPiliod::PlayerPiliod(PlayerPiliod &&)
{
	//Initialize();
}

PlayerPiliod::~PlayerPiliod()
{
	//UnInitialize();
}

void PlayerPiliod::Initialize()
{

	_data._pVertexData = Lib_Render::LoderPoligonMesh::getInstance()->GetMeshData(USER::poligon_filename[0]);
	if (!_protected._pRenderer && _data._pVertexData)
		_protected._pRenderer = new Lib_Render::RenderPoligonMesh(_data._pVertexData, _data._vertex, _data._texpos, &_data._pos, &_data._orienttion, &_data._scale);
	Lib_Vassel::pRendererManager->Add(_protected._pRenderer);

	//アイテムリストデータに値を入れる
	for (auto& it = _itemList.begin(); it != _itemList.end(); it++)
	{
		it->HuckPlayerObject(this);
		it->Initialize();
	}


		/*for (auto& it : _itemList){
			if (!it._protected._pRenderer && it->_data._pVertexData)
				it->_protected._pRenderer = new Lib_Render::RenderPoligonMesh(it->_data._pVertexData, it->_data._vertex, it->_data._texpos, &it->_data._pos, &it->_data._orienttion, &it->_data._scale);

			Lib_Vassel::pRendererManager->Add(it->_protected._pRenderer);
		}*/
}

void PlayerPiliod::UnInitialize()
{
	for (auto& it : _itemList)
	{
		it.UnInitialize();
	}
	_itemList.clear();

	if (_protected._pRenderer)
	{
		delete _protected._pRenderer;
		_protected._pRenderer = nullptr;
	}
}

void PlayerPiliod::LetData()
{
	LetData_();
	for (auto& it : _itemList)
	{
		it.LetData();
	}
}

void PlayerPiliod::Init_()
{
	{
		SecureZeroMemory(&_data, sizeof(_data));
		_itemList.clear();

		_data._pos = VECTOR3(0.0f, 0.0f, 0);
		_data._scale = VECTOR3(1, 1, 1);
		_data._orienttion = DirectX::XMQuaternionIdentity();
		_data._localPos = _data._pos;
		_data._localOrientation = _data._orienttion;
		//頂点座標定義
		const float POST = TRIANGLE_Y((RANGE >> 1));//高さ
		VECTOR3 v[VERTEX_NUM] = {//頂点▲
			VECTOR3(-static_cast<float>(RANGE >> 1) * 14,(POST / 2) * 10 ,0),
			VECTOR3((RANGE >> 1) * 12,(POST / 2) * 12,0),
			VECTOR3(0     ,-(POST / 2) * 12 ,0)
			/*VECTOR3(100,100,0),
			VECTOR3(0 ,0,0),
			VECTOR3(200     ,0,0)*/
		};
		//テクスチャ座標定義
		VECTOR2 t[VERTEX_NUM] = { VECTOR2(200,0),VECTOR2(120,200),VECTOR2(280,200) };//テクスチャ
		for (size_t i = 0; i < VERTEX_NUM; i++)
		{
			_data._vertex[i] = v[i];
			_data._texpos[i] = t[i];
			_public._vertexList.push_back(&_data._vertex[i]);
		}
		_public._position.second = (&_data._pos);
		//
		_public._label = (USER::LABEL[Enum::PLAYER]);//プレイヤーラベル
	}
	//アイテムリスト初期化
	for (size_t i = 0; i < MAX; i++)
	{
		//PlayerPoligon p;//コンストラクタ呼び出し
		//_itemList.push_back(p);
		//p._itemList.size();
		_itemList.emplace_back();
	}

}

void PlayerPiliod::MultiTask()
{
	static int timer = 0;
	printf("PlayerPiliod\t%d\n", ++timer);
	OrientationCaluculate();
	LetOrientasion();
}

void PlayerPiliod::MsgCmnd()
{
}

void PlayerPiliod::OrientationCaluculate()
{
	DirectX::XMMATRIX m = DirectX::XMMatrixRotationQuaternion(_data._orienttion);
	//VECTOR3 axisX(m.r[0].m128_f32[0], m.r[0].m128_f32[1], m.r[0].m128_f32[2]);
	VECTOR3 axisZ(m.r[3].m128_f32[0], m.r[3].m128_f32[1], m.r[3].m128_f32[2]+1);//y軸
																			  //VECTOR3 axisZ(m.r[2].m128_f32[0], m.r[2].m128_f32[1], m.r[2].m128_f32[2]);

	if (GetKeyState('Z') < 0)//左回転
	{
		DirectX::XMVECTOR axis = DirectX::XMLoadFloat3(&axisZ);
		DirectX::XMVECTOR q = DirectX::XMQuaternionRotationAxis(axis, (-1 * 0.01745f));
		_data._orienttion = DirectX::XMQuaternionMultiply(_data._orienttion, q);
	}
	if (GetKeyState('X') < 0)//右回転
	{
		DirectX::XMVECTOR axis = DirectX::XMLoadFloat3(&axisZ);
		DirectX::XMVECTOR q = DirectX::XMQuaternionRotationAxis(axis, (1 * 0.01745f));
		_data._orienttion = DirectX::XMQuaternionMultiply(_data._orienttion, q);
	}
	if (GetAsyncKeyState(VK_UP))
	{
		_data._pos.y -= 0.01f;
	}
	if (GetAsyncKeyState(VK_DOWN))
	{
		_data._pos.y += 0.01f;
	}
	if (GetAsyncKeyState(VK_LEFT))
	{
		_data._pos.x -= 0.01f;
	}
	if (GetAsyncKeyState(VK_RIGHT))
	{
		_data._pos.x += 0.01f;
	}
}

void PlayerPiliod::LetOrientasion()
{
	for (auto& it : _itemList)
	{
		it.HuckPlayerObject(this);
		it.LoopFunc(nullptr);
		//it.LetData();
	}
}

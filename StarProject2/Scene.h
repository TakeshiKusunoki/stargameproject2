#pragma once

#include "Lib_Base\Template.h"
#include "Lib_Vassel\BaseClass.h"



using Lib_Vassel::BaseLoop;

class Scene : public BaseLoop
{
public:
	Scene* _pNext;
public:
	virtual void Init() = 0;
	virtual BaseLoop* LoopFunc(BaseLoop* loopFunc)override = 0;
	virtual void Draw() = 0;
	virtual void UnInit() = 0;
};


//! Scene管理クラス
class SceneManager :public Singleton<SceneManager>
{
public:
	static BaseLoop* _pNext;//! 遷移するシーン、nullptrなら完全終了
	static BaseLoop* _pCurrent;//! 現在のインスタンス
public:
	bool execute(BaseLoop* scene);
private:
};
// インスタンス取得マクロ
#define pSceneManager (SceneManager::getInstance())


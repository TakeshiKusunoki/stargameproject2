#pragma once
#include "Lib_Base\WinMainClass.h"
#include "StanbyPresant.h"
#include "Lib_Base\WindowEatherProcess.h"
#include "Lib_Base\Template.h"
class StanbyPresant;
class Lib_Base::WindowCleate;

//! @brief ゲームモードで呼ばれる
class DrawEachWindow : public Lib_Base::WindowEatherProcess , public Singleton<DrawEachWindow>
{
public:
	//! @brief ウインドウの数だけ呼び出される
	//! @brief リストごとの描画するウインドウターゲットを決める
	void WinEatherProcess(Lib_Base::WindowCleate* win)override;
private:

};

//! @brief タイトルで呼ばれる
class DrawEachWindowInTitle : public Lib_Base::WindowEatherProcess, public Singleton<DrawEachWindowInTitle>
{
public:
	//! @brief ウインドウの数だけ呼び出される
	//! @brief リストごとの描画するウインドウターゲットを決める
	void WinEatherProcess(Lib_Base::WindowCleate* win)override {}
private:

};

//! @brief リザルトで呼ばれる
class DrawEachWindowInResult : public Lib_Base::WindowEatherProcess, public Singleton<DrawEachWindowInResult>
{
public:
	//! @brief ウインドウの数だけ呼び出される
	//! @brief リストごとの描画するウインドウターゲットを決める
	void WinEatherProcess(Lib_Base::WindowCleate* win)override {}
private:

};

//! シーンでの描画
class Drawer : public Singleton<Drawer>
{
	//スタンバイモード
	static StanbyPresant stanbyPresant;
	static StanbyPresant stanbyPresantForTitle;
	static StanbyPresant stanbyPresantForResult;
public:

public:
	void GameDraw()
	{
		Lib_Base::pWindowCreateManager->ProcessExectuteEachWindow(&stanbyPresant);
	}
	void TitleDraw()
	{
		Lib_Base::pWindowCreateManager->ProcessExectuteEachWindow(&stanbyPresantForTitle);
	}
	void ResultDraw()
	{
		Lib_Base::pWindowCreateManager->ProcessExectuteEachWindow(&stanbyPresantForResult);
	}
private:
};
#define pDrawer Drawer::getInstance();

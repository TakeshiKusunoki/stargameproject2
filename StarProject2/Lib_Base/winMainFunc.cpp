#include "winMainFunc.h"
#include "WinMainClass.h"


namespace Lib_Base {
	//+ 追加ウインドウ--------------------------------------
	/**
	* @namespace WinFunc
	* @brief ウインドウ定義をまとめたもの
	* @details 詳細な説明
	*/


	//=============================================================================
	//  機能概要：コンソール画面の表示
	//　引数		：
	//  戻り値　：正常終了のとき１、以上終了のとき０
	//=============================================================================
	BOOL Console()
	{
		// コンソールを作成する
		AllocConsole();
		// 標準入出力に割り当てる
		FILE* fp = NULL;
		// 昔のコード
		// 現在のコード
		freopen_s(&fp, "CONOUT$", "w", stdout);
		freopen_s(&fp, "CONIN$", "r", stdin);
		printf("hello world\n");
		//FreeConsole();
		return (TRUE);
	}

	LRESULT CALLBACK WinProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
	{
		switch (msg)
		{
			case WM_DESTROY:
				PostQuitMessage(0);
				return 0;
		}
		return DefWindowProc(hwnd, msg, wp, lp);
	}
	// ウインドウ定義
	BOOL InitWindow_(HWND& hWnd, char* className, int left, int top, size_t width, size_t height)
	{
		WNDCLASS winc;

		winc.style = CS_HREDRAW | CS_VREDRAW;
		winc.lpfnWndProc = WinProc;
		winc.cbClsExtra = winc.cbWndExtra = 0;
		winc.hInstance = WinMainClass::_hInstance;
		winc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		winc.hCursor = LoadCursor(NULL, IDC_ARROW);
		winc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		winc.lpszMenuName = NULL;
		winc.lpszClassName = className;

		if (!RegisterClass(&winc)) return 0;

		hWnd = CreateWindow(
			className, TEXT("☆Star Project"),
			WS_OVERLAPPEDWINDOW,
			left, top, width, height, NULL, NULL,
			WinMainClass::_hInstance, NULL
		);

		if (hWnd == NULL) return 0;

		ShowWindow(hWnd, SW_SHOW);

		return (TRUE);
	}

	BOOL InitWindow2(HWND& hWnd, char* className, int left, int top, size_t width, size_t height)
	{
		// ウインドウクラスを定義する
		WNDCLASSEX WindowClass;
		WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS | CS_CLASSDC/*|CS_GLOBALCLASS| CS_DBLCLKS*/;//このクラスのウインドウ同士で１つのデバイスコンテキストを共有する
		WindowClass.lpfnWndProc = WinProc;								  //ウィンドウプロシージャ関数
		WindowClass.cbSize = sizeof(WNDCLASSEX);
		WindowClass.cbClsExtra = 0;									  //エキストラ（なしに設定）
		WindowClass.cbWndExtra = 0;									  //必要な情報（なしに設定）
		WindowClass.hInstance = WinMainClass::_hInstance;								  //このインスタンスへのハンドル
		WindowClass.hIcon = LoadIcon(WinMainClass::_hInstance, IDI_APPLICATION);		  //ラージアイコン
		WindowClass.hIconSm = LoadIcon(WinMainClass::_hInstance, IDI_WINLOGO);			 //スモールアイコン
		WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);				  //カーソルスタイル
		WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	  //ウィンドウの背景（黒に設定）
		WindowClass.lpszMenuName = NULL;								  //メニュー（なしに設定）
		WindowClass.lpszClassName = className;					 //このウインドウクラスにつける名前

																 // ウインドウクラスを登録する
		if (!RegisterClassEx(&WindowClass))
		{
			OutputDebugString(TEXT("Error: ウィンドウクラスの登録ができません。\n"));
			return false;
		}
		// ウインドウクラスの登録ができたので、ウィンドウを生成する。

		hWnd = CreateWindowEx(
			WS_EX_ACCEPTFILES | WS_EX_CLIENTEDGE | WS_EX_LAYERED,			   //拡張ウィンドウスタイル
			className,								//ウィンドウクラスの名前
			TEXT("☆Star Project2"),							//ウィンドウタイトル
			WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU,	   //ウィンドウスタイル
			left,												//
			top,												//
			width,									   //
			height,									   //
			NULL,											//親ウィンドウ（なし）
			NULL,											//メニュー（なし）
			WinMainClass::_hInstance,										//このプログラムのインスタンスのハンドル
			NULL											   //追加引数（なし）
		);
		if (!hWnd)
		{
			OutputDebugString(TEXT("Error: ウィンドウが作成できません。\n"));
			return false;
		}

		ShowWindow(hWnd, WinMainClass::_nShowCmd); // ウィンドウを表示する
		UpdateWindow(hWnd);

		return (TRUE);

	}

}
#pragma once
class Counter
{
private:
	int _count;
	bool AdvancedFlag;////進行フラグ
public:
	Counter()
		: _count(0)
		, AdvancedFlag(false)
	{
	}
	~Counter() {}
	//! カウンター進行、カウンター進行が止まるとカウンターをリセットする
	//! ループ内でn回trueを返す
	//! @return カウンター進行している間、フラグtrueを返す
	bool FlagAdvanced(const size_t n)
	{
		AdvancedCounter(n);
		if (IsAdvanced())
			return true;
		CounterReset();
		return false;
	}
	//この値まで進行
	void AdvancedCounter(const size_t n)
	{
		if (static_cast<size_t>(_count) > n)
		{
			AdvancedFlag = false;
			return;
		}
		_count++;
		AdvancedFlag = true;
	}
	int GetCounter()
	{
		return _count;
	}
	//処理が進んでいるか
	bool IsAdvanced()
	{
		return AdvancedFlag;
	}
	//値リセット
	void CounterReset()
	{
		_count = 0;
	}
private:

};


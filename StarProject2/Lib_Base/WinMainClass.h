#pragma once
/**
* @file WinMainClass.h
* @brief mainウインドウの隠蔽クラスが記述されている
* @details ウインドウ定義も書かれている
*/
#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <Windows.h>
#include "WindowCleate.h"
#include "high_resolution_timer.h"
#define WINMAIN_CLASS_NAME ("MainWindow")//メインウインドウにつけるウインドウクラス名

//! @namespace Lib_Base
//! ベースライブラリ
namespace Lib_Base {

	/**
	* @brief mainウインドウの隠蔽クラス
	* @par 詳細
	* winMainがある<br>
	* このクラスを継承したクラスをnewすることで、そのクラスのメインループが行われる<br>
	* my_instanceにＮＵＬＬを入れると終了する
	*/
	class WinMainClass
	{
	private:
		static WinMainClass* _my_previnstance;//! 次に呼ばれるメインループ
	protected:
		static high_resolution_timer _Timer;//! フレームに影響されないタイマー
	public:
		static HINSTANCE _hInstance;
		static HINSTANCE _prev_instance_;
		static PSTR _cmd_lin_;
		static int _nShowCmd;
	public:
		static WinMainClass* _my_instance;//! 現在のメインループ(これを外で呼ばない!)


		WinMainClass();
		virtual ~WinMainClass() {}
		//! @brief
		//! return bool falseでこのループを最初からやり直す
		virtual bool MainLoop() { return false; }
		int Boot();

		virtual void Init() = 0;
		virtual void UnInit() = 0;
		//! @brief ループを終了する
		void endLoop() { _my_instance = nullptr; }
	private:
		//! @brief メッセージループ
		void MsgLoop();

		void Calculate_frame_stats();

	};

	namespace MainWindow {
		BOOL InitWindow(HWND& hWnd, char* className, int left, int top, size_t width, size_t height);
	}
}

#pragma once
#include <malloc.h>
//! @brief Singleton（もどき）テンプレート
template <typename T>
class Singleton
{
public:
	static T* getInstance()
	{
		static T instance;
		return &instance;
	}
};



template <size_t T>
class AlignedAllocationPolicy
{
public:
	static void* operator new(size_t i)
	{
		return _mm_malloc(i, T);
	}
	static void operator delete(void* p)
	{
		_mm_free(p);
	}
};



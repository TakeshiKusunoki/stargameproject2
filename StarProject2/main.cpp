#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <windows.h>
#include <assert.h>
#include "main.h"
#include "Lib_3D\Wrapped.h"
#include "Scene.h"
#include "SceneInit.h"
#include "Lib_Render\RenderPoligonMesh.h"


Main main_app;//メインループの実態
#ifdef _DEBUG
#pragma comment (lib,"./Lib_Live2D/Core/lib/windows/x86/140/Live2DCubismCore_MDd.lib")
#pragma comment(lib, "./Lib_Live2D/Framework/Debug/Framework.lib")
#else
#pragma comment (lib,"./Lib_Live2D/Core/lib/windows/x86/140/Live2DCubismCore_MD.lib")
#pragma comment(lib, "./Lib_Live2D/Framework/Release/Framework.lib")
#endif

#ifdef _DEBUG
//#pragma comment( lib, "./Lib_Base/Lib/Debug/DirectXTK.lib" )
#pragma comment( lib, "./Lib_Base/Lib/Debug/DirectXTKAudioWin8.lib" )
#else
#pragma comment( lib, "./Lib_Base/Lib/Release/DirectXTK.lib" )
#pragma comment( lib, "./Lib_Base/Lib/Release/DirectXTKAudioDX.lib" )
#endif
///#pragma comment (lib,"DirectXTK.lib")
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")
#ifdef _DEBUG
#pragma comment (lib,"libfbxsdk-md.lib")
#else
#pragma comment (lib,"./Lib_3D/Lib/Release/libfbxsdk-md.lib")
#endif



BaseLoop* SceneManager::_pNext = pSceneInit;
BaseLoop* SceneManager::_pCurrent = pSceneInit;

Main::Main(void)
{
}

void Main::Init()
{
	if(pSceneManager->_pNext)
	reinterpret_cast<Scene*>(pSceneManager->_pNext)->Init();
	//mainwindow(0番目のウインドウを作成)
}

//コマンドで別のゲームループへ
bool Main::MainLoop()
{
	if (!pSceneManager->_pNext)
		return false;
	return pSceneManager->execute(pSceneManager->_pNext);//return falseでリスタート
}

void Main::UnInit()
{
	if (pSceneManager->_pCurrent)
	{
		reinterpret_cast<Scene*>(pSceneManager->_pCurrent)->UnInit();
		pSceneManager->_pCurrent = pSceneManager->_pNext;
	}
	else//pSceneManager->_pNext == nullptrなら完全終了する
		this->_my_instance = nullptr;
}

//this->_my_instance = nullptr;になったとき
Main::~Main(void)
{
	Lib_Render::pLoderPoligonMesh->Release();
	/*Lib_Base::pWindowCreateManager->DestoroyAllWindow();
	Lib_3D::pDirectXWindowManager->UnInitAll();*/
}

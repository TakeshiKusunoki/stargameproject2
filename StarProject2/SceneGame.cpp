#include "SceneGame.h"
#include "Stage.h"
#include "Drawer.h"
#include "SceneTitle.h"

void SceneGame::Init()
{

	pStageGame->Init();
}

BaseLoop * SceneGame::LoopFunc(BaseLoop * loopFunc)
{
	pStageGame->LoopFunc(loopFunc);
	if (GetAsyncKeyState(VK_RETURN) < 0)
	{
		return pSceneTitle;
	}
	return this;
}

void SceneGame::Draw()
{
	Drawer::getInstance()->GameDraw();
}

void SceneGame::UnInit()
{
	pStageGame->UnInit();
}

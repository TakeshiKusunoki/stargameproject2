#ifndef INCLUDED_COMMON
#define	INCLUDED_COMMON

//******************************************************************************
//
//
//      common
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <DirectXMath.h>

//------< //定数 >---------------------------------------------------------
// enum
namespace Enum {
	enum LABEL_DEFINE
	{
		PLAYER = 0,
		ITEM,
		ENEMY,
		NONE
	};
	enum MSG_DEFINE
	{
		HIT_PLAYER = 0,
		HIT_ITEM,
		HIT_ENEMY,
		MSG_NONE
	};
	enum CMND_DEFINE
	{
		CMND_ENTER = 0,
		CMND_NONE
	};

}
namespace USER {
	constexpr wchar_t* poligon_filename[] = {
		L"DATA\\Picture\\player.png",
		L"DATA\\Picture\\point.png",
		nullptr
	};
	constexpr char* LABEL[] = {
		"PLAYER",
		"ITEM",
		"ENEMY",
		nullptr
	};
	constexpr char* MSG[] = {
		"hitPlayer",
		"hitItem",
		"hitEnemy",
		nullptr
	};
	constexpr char* CMND[] = {
		"Enter",
		nullptr
	};
}


//時計の針の向き
struct JOYPAD_KEY
{
	bool _0;//時計の針の向き
	bool _3;//時計の針の向き
	bool _6;//時計の針の向き
	bool _9;//時計の針の向き
	bool _12;//時計の針の向き
	bool _15;//時計の針の向き
	bool _18;//時計の針の向き
	bool _21;//時計の針の向き
	void operator=(JOYPAD_KEY key)
	{
		this->_0 = key._0;
		this->_3 = key._3;
		this->_6 = key._6;
		this->_9 = key._9;
		this->_12 = key._12;
		this->_15 = key._15;
		this->_18 = key._18;
		this->_21 = key._21;
	}
	void operator=(bool f)
	{
		this->_0 = f;
		this->_3 = f;
		this->_6 = f;
		this->_9 = f;
		this->_12 = f;
		this->_15 = f;
		this->_18 = f;
		this->_21 = f;
	}
	bool operator==(JOYPAD_KEY key)
	{
		if (this->_0 == key._0 &&
			this->_3 == key._3 &&
			this->_6 == key._6 &&
			this->_9 == key._9 &&
			this->_12 == key._12 &&
			this->_15 == key._15 &&
			this->_18 == key._18 &&
			this->_21 == key._21
			)
		{
			return true;
		}
		return false;
	};
	bool operator!=(JOYPAD_KEY key)
	{
		if (this->_0 != key._0 ||
			this->_3 != key._3 ||
			this->_6 != key._6 ||
			this->_9 != key._9 ||
			this->_12 != key._12 ||
			this->_15 != key._15 ||
			this->_18 != key._18 ||
			this->_21 != key._21
			)
		{
			return true;
		}
		return false;
	};
};










#define SEC (60)//１秒

//------< 関数 >----------------------------------------------------------------

static float(*const ToRadian)(float degree) = DirectX::XMConvertToRadians;  // 角度をラジアンに
static float(*const ToDegree)(float radian) = DirectX::XMConvertToDegrees;  // ラジアンを角度に

#endif // !INCLUDED_COMMON
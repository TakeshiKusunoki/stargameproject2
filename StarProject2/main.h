#pragma once
#include "Lib_Base\Wrapped_Base.h"

//! @brief メイン関数で呼ばれる関数
//! par 詳細
//! Lib_Base::からウインドウの定義を呼べる
//! @details main.cppを変えることでwinMain関数の中身を変えることができる
class Main : public Lib_Base::WinMainClass
{
public:
	Main(void);
	Main(const Main&){}
	~Main(void);

	//! @brief ループ前初期化
	void Init();
	//! @brief メインループ
	bool MainLoop();
	//! @brief ループ後終了処理
	void UnInit();

};
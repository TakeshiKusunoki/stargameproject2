#include "Sprite2D_mesh.h"
//#include <CubismDefaultParameterId.hpp>
#include <CubismFramework.hpp>
//#include <CubismModelSettingJson.hpp>
//#include <CubismFrameworkConfig.hpp>
#include <Live2DCubismCore.hpp>
//#include <ICubismAllocator.hpp>
//#include <ICubismModelSetting.hpp>
//#include <Type\csmMap.hpp>
#include <Type\CubismBasicType.hpp>
//#include <Rendering\CubismRenderer.hpp>
//#include <Rendering\OpenGL\CubismOffscreenSurface_OpenGLES2.hpp>
//#include <Rendering\OpenGL\CubismRenderer_OpenGLES2.hpp>
//#include <Rendering\D3D9\CubismRenderer_D3D9.hpp>
//#include <Rendering\D3D11\CubismNativeInclude_D3D11.hpp>
//#include <Rendering\D3D11\CubismOffscreenSurface_D3D11.hpp>
//#include <Rendering\D3D11\CubismRenderer_D3D11.hpp>
//#include <Rendering\D3D11\CubismRenderState_D3D11.hpp>
//#include <Rendering\D3D11\CubismShader_D3D11.hpp>
//#include <Rendering\D3D11\CubismType_D3D11.hpp>
using namespace Live2D::Cubism::Framework;
using namespace Csm;
using namespace Live2D::Cubism::Core;
// 相対パス
const csmChar* ResourcesPath = "Lib_Live2D/Res/";
// モデル定義------------------------------------------
// モデルを配置したディレクトリ名の配列
// ディレクトリ名とmodel3.jsonの名前を一致させておくこと
const csmChar* ModelDir[] = {
	"Mark",
	"point"
};
const csmInt32 ModelDirSize = sizeof(ModelDir) / sizeof(const csmChar*);

#include "..\Lib_Live2D\Demo\LAppDelegate.hpp"
Sprite2D_mesh::Sprite2D_mesh()
{
	LAppDelegate::InitializeCubism();
}


Sprite2D_mesh::~Sprite2D_mesh()
{
}

//#include <CubismModelSettingJson.hpp>
//#include <Motion/CubismMotion.hpp>
//#include <Physics/CubismPhysics.hpp>
//#include <CubismDefaultParameterId.hpp>
//#include <Rendering/D3D11/CubismRenderer_D3D11.hpp>
//#include <Utils/CubismString.hpp>
//#include <Id/CubismIdManager.hpp>
#include <Motion/CubismMotionQueueEntry.hpp>
void* mocMemory;
unsigned int mocSize;
#include "..\Lib_Live2D\Demo\LAppModel.hpp"
//#include "..\Lib_Live2D\Demo\LAppDefine.hpp"
//#include "..\Lib_Live2D\Demo\LAppPal.hpp"
//#include "..\Lib_Live2D\Demo\LAppTextureManager.hpp"
//#include "..\Lib_Live2D\Demo\LAppDelegate.hpp"
#include <Model\CubismModelUserData.hpp>
#include <fstream>
#include <vector>

//using namespace Live2D::Cubism::Framework::DefaultParameterId;
//using namespace LAppDefine;
void Sprite2D_mesh::loadLive2DModel(Csm::csmInt32 index)
{
	// ModelDir[]に保持したディレクトリ名から
	// model3.jsonのパスを決定する.
	// ディレクトリ名とmodel3.jsonの名前を一致させておくこと.
	std::string model = ModelDir[index];
	std::string modelPath = ResourcesPath + model + "/";
	std::string modelJsonName = ModelDir[index];
	modelJsonName += ".model3.json";


	_models.PushBack(new LAppModel());
	_models[0]->LoadAssets(modelPath.c_str(), modelJsonName.c_str());

}

// ReleaseAllModel();ロード破棄
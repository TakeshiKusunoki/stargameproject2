#pragma once
#include <d3d11.h>//追加

//////////////////////////////////////////////
//Unit7
////////////////////////////////////////////////

namespace Lib_3D {
	class BlendMode
	{
	public:
		BlendMode()
		{
			BlendMode::BlendState[BlendMode::MODE_MAX] = { nullptr };
			BlendMode::bLoad = false;
			BlendMode::enumMode;
		};
		~BlendMode() { Release(); };
		enum BLEND_MODE
		{
			NONE = 0,//合成なし（デフォルト）
			ALPHA,	    //α合成
			ADD,	    //加算合成
			SUB,	    //減産合成
			REPLACE,    //置き換え
			MULTIPLY,   //乗算
			LIGHTEN,    //比較（明）
			DARKEN,	    //比較（暗）
			SCREEN,	    //スクリーン
			MODE_MAX,   //
		};
	private:
		ID3D11BlendState* BlendState[MODE_MAX];//ブレンド設定配列
		bool bLoad;//true:設定配列作成済み
		BLEND_MODE enumMode;//現在使用してるブレンドモード

	public:
		bool Initializer(ID3D11Device* p_Device);
		void Release()
		{
			if (!bLoad)
				return;
			for (BLEND_MODE mode = NONE; mode < MODE_MAX; mode = (BLEND_MODE)(mode + 1))
			{
				if (BlendState[mode])
				{
					BlendState[mode]->Release();
				}
			}
			bLoad = false;
		}
		//ブレンド設定用関数
		void Set(ID3D11DeviceContext* p_DeviceContext, BLEND_MODE mode = NONE)
		{
			if (!bLoad)return;
			if (mode < 0 || mode >= MODE_MAX) return;
			if (mode == enumMode)return;
			p_DeviceContext->OMSetBlendState(BlendState[mode], nullptr, 0xffffffff);
			enumMode = mode;
		}
		//static void BlendSwitch(D3D11_BLEND_OP mode);//ブレンドモードスイッチ
		//static void BlendAlpha();
		//static void BlendAdd();
		//static void BlendSubtract();
		//static void BlendReplace();
		//static void BlendMultiply(D3D11_BLEND mode1, D3D11_BLEND mode2);
		//static void BlendLighten();
		//static void BlendDarken();
		//static void BlendScreen();
	private:
		/*D3D11_BLEND dst;
		D3D11_BLEND src;*/
		/* static DirectX::XMFLOAT4 dst;
		static DirectX::XMFLOAT4 src;
		static DirectX::XMFLOAT4 res;*/
	};
}



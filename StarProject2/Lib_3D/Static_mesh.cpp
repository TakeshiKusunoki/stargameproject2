// UNIT12
#include "Static_mesh.h"
#include "misc.h"
#include <fstream>
#include "ResourceManager.h"
#include "texture.h"

namespace Lib_3D {

	Static_mesh::Static_mesh(ID3D11Device * p_Device, const wchar_t * obj_filename, bool flipping_v_coordinates/*UNIT13*/)
	{

		// ファイルロード--------------------------------
		loadObjFile(p_Device, obj_filename, flipping_v_coordinates/*UNIT13*/);


		// 設定--------------------------------------
		HRESULT hr = S_OK;

		/////////////////////////////////////////////////
		// �@頂点データの構造を記述(記載した情報をIAステージに伝える)
		/////////////////////////////////////////////////
		D3D11_INPUT_ELEMENT_DESC InputElementDesk[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		const UINT ELEMENTS_ARRAY_NUM = ARRAYSIZE(InputElementDesk);
		/////////////////////////////////////////////////
		// �Aバーテックスシェーダーオブジェクトの生成
		/////////////////////////////////////////////////
		resourceManager = new ResourceManager;
		bool f = true;
		f = resourceManager->LoadVertexShader(p_Device, "Shader\\Static_mesh_vs.cso", InputElementDesk, ELEMENTS_ARRAY_NUM, &p_VertexShader, &p_InputLayout);
		if (!f)
		{
			assert(!"データが見つからなかった");
		}
		/////////////////////////////////////////////////
		// �Bピクセルシェーダーオブジェクトの生成
		/////////////////////////////////////////////////
		f = resourceManager->LoadPixelShader(p_Device, "Shader\\Static_mesh_ps.cso", &p_PixelShader);
		if (!f)
		{
			assert(!"データが見つからなかった");
		}
		////////////////////////////////////////////////////////
		// �Cラスタライザーステートオブジェクトの生成（線描画・塗りつぶし描画)
		////////////////////////////////////////////////////////
		D3D11_RASTERIZER_DESC RasteriserDesk;
		ZeroMemory(&RasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
		//-線描画の場合
		RasteriserDesk.FillMode = D3D11_FILL_WIREFRAME;	//レンダリング時に使用する描画モードを決定します
		RasteriserDesk.CullMode = D3D11_CULL_BACK;		//特定の方向を向いている三角形の描画の有無を示します。
		RasteriserDesk.FrontCounterClockwise = TRUE;	//三角形が前向きか後ろ向きかを決定します。
		RasteriserDesk.DepthBias = 0;					//指定のピクセルに加算する深度値です。
		RasteriserDesk.DepthBiasClamp = 0;				//ピクセルの最大深度バイアスです。
		RasteriserDesk.SlopeScaledDepthBias = 0;		//指定のピクセルのスロープに対するスカラです。
		RasteriserDesk.DepthClipEnable = FALSE;			//距離に基づいてクリッピングを有効にします。
		RasteriserDesk.ScissorEnable = FALSE;			//シザーカリング
		RasteriserDesk.MultisampleEnable = FALSE;		//マルチサンプリングのアンチエイリアシング
		RasteriserDesk.AntialiasedLineEnable = TRUE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
		hr = p_Device->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStateLine);
		if (FAILED(hr))
		{
			assert(!"ラスタライザーステートオブジェクトの生成ができません");
			return;
		}

		//-塗りつぶし描画の場合
		RasteriserDesk.FillMode = D3D11_FILL_SOLID;		//レンダリング時に使用する描画モードを決定します
		RasteriserDesk.CullMode = D3D11_CULL_BACK;		//特定の方向を向いている三角形の描画の有無を示します。
		RasteriserDesk.FrontCounterClockwise = FALSE;	//三角形が前向きか後ろ向きかを決定します。
		RasteriserDesk.DepthClipEnable = FALSE;			//距離に基づいてクリッピングを有効にします。
		RasteriserDesk.MultisampleEnable = TRUE;		//マルチサンプリングのアンチエイリアシング
		RasteriserDesk.AntialiasedLineEnable = FALSE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
		hr = p_Device->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStatePaint);
		if (FAILED(hr))
		{
			assert(!"ラスタライザーステートオブジェクト(塗りつぶし描画)の生成ができません");
			return;
		}

		/////////////////////////////////////////////////
		// �D深度ステンシル ステート オブジェクトの生成
		/////////////////////////////////////////////////
		D3D11_DEPTH_STENCIL_DESC DepthDesc;
		ZeroMemory(&DepthDesc, sizeof(DepthDesc));
		DepthDesc.DepthEnable = true;									//深度テストあり
		DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//書き込む
		DepthDesc.DepthFunc = D3D11_COMPARISON_LESS;					//手前の物体を描画
		DepthDesc.StencilEnable = FALSE;								//ステンシル テストなし
		DepthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;		//ステンシル読み込みマスク
		DepthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;	//ステンシル書き込みマスク
																		//面が表を向いている場合のステンシルステートの設定
		DepthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
		DepthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
																		  //面が裏を向いている場合のステンシルステートの設定
		DepthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
		DepthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
		hr = p_Device->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
		if (FAILED(hr))
		{
			assert(!"深度ステンシル ステート オブジェクトの生成ができません");
			return;
		}

		/////////////////////////////////////////////////
		// コンスタントバッファの作成
		/////////////////////////////////////////////////
		{
			D3D11_BUFFER_DESC buffer_desc = {};

			buffer_desc.ByteWidth = sizeof(CONSTANT_BUFFER);
			buffer_desc.Usage = D3D11_USAGE_DEFAULT;
			buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			buffer_desc.CPUAccessFlags = 0;
			buffer_desc.MiscFlags = 0;
			buffer_desc.StructureByteStride = 0;
			hr = p_Device->CreateBuffer(&buffer_desc, nullptr, &p_BufferConst);
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}
		// UNIT.13
		// シェーダリソースビューのロード
		D3D11_TEXTURE2D_DESC Texture2dDesc;
		///f=ResourceManager::LoadShaderResourceView(p_Device, texture_filename.c_str(), &p_ShaderResourceView, &Texture2dDesc);
		for (auto& it : materials)
		{
			f = resourceManager->LoadShaderResourceView(p_Device, it.map_Kd.c_str(), &it.ShaderResourceView, &Texture2dDesc);
			if (!f)
			{
				assert(!"データが見つからなかった");
			}
		}

		// UNIT.13
		/////////////////////////////////////////////////
		// サンプラーステートオブジェクトの設定（テクスチャの描画）
		/////////////////////////////////////////////////
		D3D11_SAMPLER_DESC SamplerDesc;
		SamplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;		 //異方性フィルタリング
		SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;	 //「ラップ・テクスチャ」アドレシング・モード
		SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;	 //
		SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;	 //
		SamplerDesc.MipLODBias = 0.0f;						//ミップマップの詳細レベル
		SamplerDesc.MaxAnisotropy = 16;					//異方性フィルタリングの次数
		SamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER; //
		memcpy(SamplerDesc.BorderColor, &DirectX::XMFLOAT4(0, 0, 0, 0), sizeof(DirectX::XMFLOAT4));
		/*SamplerDesc.BorderColor[0] = 0.0f;
		SamplerDesc.BorderColor[1] = 0.0f;
		SamplerDesc.BorderColor[2] = 0.0f;
		SamplerDesc.BorderColor[3] = 0.0f;*/
		SamplerDesc.MinLOD = 0;						 //0が最大で最も精細//-FLT_MAX;
		SamplerDesc.MaxLOD = FLT_MAX;

		// サンプラー・ステート・オブジェクトの作成
		hr = p_Device->CreateSamplerState(&SamplerDesc, &p_SampleState);
		if (FAILED(hr))
		{
			assert(!"サンプラー・ステート オブジェクトの生成ができません");
			return;
		}

	}

#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	Static_mesh::~Static_mesh()
	{
		RELEASE_IF(p_SampleState);// UNIT13
		RELEASE_IF(p_DepthStencilState);
		RELEASE_IF(p_RasterizerStateLine);
		RELEASE_IF(p_RasterizerStatePaint);
		RELEASE_IF(p_BufferConst);
		RELEASE_IF(p_BufferIndex);
		RELEASE_IF(p_BufferVs);
		resourceManager->ReleasePixelShader(p_PixelShader);
		resourceManager->ReleaseVertexShader(p_VertexShader, p_InputLayout);
		for (auto& it : materials)
		{
			resourceManager->ReleaseShaderResourceView(it.ShaderResourceView);// UNIT13
		}
		RELEASE_IF(resourceManager);
	}
#undef RELEASE_IF
#undef DELETE_IF

	//定数
#define VERTEX_BUFFER_NUM 1//頂点バッファの数

	void Static_mesh::render(ID3D11DeviceContext * p_DeviceContext, const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 & materialColor, bool FlagPaint)
	{

		UINT stride[VERTEX_BUFFER_NUM] = { sizeof(VERTEX) };	 //頂点バッファにふくまれる頂点データのサイズ。
		UINT offset[VERTEX_BUFFER_NUM] = { 0 };					 //頂点バッファのオフセット
		p_DeviceContext->IASetVertexBuffers(0, VERTEX_BUFFER_NUM, &p_BufferVs, stride, offset);
		// �AIAにインデックスバッファを設定
		p_DeviceContext->IASetIndexBuffer(p_BufferIndex, DXGI_FORMAT_R32_UINT, 0);

		// 描画するプリミティブ種類の設定
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//３角形リストを描画する
																					   // 入力レイアウト・オブジェクトの設定
		p_DeviceContext->IASetInputLayout(p_InputLayout);

		p_DeviceContext->VSSetShader(p_VertexShader, nullptr, 0);
		p_DeviceContext->PSSetShader(p_PixelShader, nullptr, 0);
		//震度ステンシルステート
		p_DeviceContext->OMSetDepthStencilState(p_DepthStencilState, 0);//震度ステンシルステート

																		// �Cラスタライザ・ステート・オブジェクトの設定
		p_DeviceContext->RSSetState((FlagPaint ? p_RasterizerStatePaint : p_RasterizerStateLine));//ラスタライザステート
																								  ///p_DeviceContext->RSSetScissorRects();//シザー短形
																								  // UNIT13
																								  // UNIT14
																								  // シェーダリソースのセット
		for (auto& it : materials)
		{
			// �Bコンスタントバッファを設定+		light_direction	{x=0.000000000 y=-1.00000000 z=0.000000000 ...}	DirectX::XMFLOAT4

			CONSTANT_BUFFER data = {};
			data.wvp = wvp;
			data.world = world;
			data.light_direction = lightVector;
			//data.material_color = materialColor;
			data.material_color.x = materialColor.x*it.Kd.x;
			data.material_color.y = materialColor.y*it.Kd.y;
			data.material_color.z = materialColor.z*it.Kd.z;
			data.material_color.w = materialColor.w;

			p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &data, 0, 0);//情報を定数バッファへコピー
			p_DeviceContext->VSSetConstantBuffers(0, VERTEX_BUFFER_NUM, &p_BufferConst);//定数バッファをシェーダへセット
																						//
			p_DeviceContext->PSSetShaderResources(0, 1, &it.ShaderResourceView);
			// サンプラーステートのセット
			p_DeviceContext->PSSetSamplers(0, 1, &p_SampleState);
			for (auto its : subset)
			{
				if (it.newmtl == its.usemtl)
				{
					p_DeviceContext->DrawIndexed(its.index_count, its.index_start, 0);//インデックスずけされているプリミティブの描画
				}
			}
		}

		// --------------

	}
#undef VERTEX_BUFFER_NUM




	void Static_mesh::create_buffers(ID3D11Device * p_Device, VERTEX * vertices, const int NUM_VRETEX, u_int * indices, const int NUM_INDEX)
	{
		HRESULT hr = S_OK;
		/////////////////////////////////////////////////
		// �F頂点バッファオブジェクトの生成
		//////////////////////////////////////////////////
		// �E頂点情報・インデックス情報のセット// 一辺が 1.0 の正立方体データを作成する（重心を原点にする

		// 頂点バッファ定義
		D3D11_BUFFER_DESC Bufferdesk;
		ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));
		Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(VERTEX);
		Bufferdesk.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
		Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		Bufferdesk.CPUAccessFlags = 0;
		Bufferdesk.MiscFlags = 0;
		Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

										   // サブリソースの初期化に使用されるデータを指定します。
		D3D11_SUBRESOURCE_DATA SubResourceData;
		ZeroMemory(&SubResourceData, sizeof(SubResourceData));
		SubResourceData.pSysMem = vertices;				//(バッファの初期値)初期化データへのポインターです。
		SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
		SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
															// バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
		hr = p_Device->CreateBuffer(&Bufferdesk, &SubResourceData, &p_BufferVs);
		if (FAILED(hr))
		{
			assert(!"頂点バッファの作成ができません");
			return;
		}

		///////////////////////////////////////////////////
		// �Gインデックスバッファオブジェクトの生成
		///////////////////////////////////////////////////

		// インデックスバッファの定義
		D3D11_BUFFER_DESC IndexBufferDesc;
		ZeroMemory(&IndexBufferDesc, sizeof(IndexBufferDesc));
		IndexBufferDesc.ByteWidth = NUM_INDEX * sizeof(UINT);
		IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
		IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		IndexBufferDesc.CPUAccessFlags = 0;
		IndexBufferDesc.MiscFlags = 0;
		IndexBufferDesc.StructureByteStride = 0;
		//インデックスの数を補完
		numIndices = NUM_INDEX;

		// インデックス・バッファのサブリソースの定義
		D3D11_SUBRESOURCE_DATA IndexSubResource;
		ZeroMemory(&IndexSubResource, sizeof(IndexSubResource));
		IndexSubResource.pSysMem = indices;
		IndexSubResource.SysMemPitch = 0;
		IndexSubResource.SysMemSlicePitch = 0;

		// インデックス・バッファの作成
		hr = p_Device->CreateBuffer(&IndexBufferDesc, &IndexSubResource, &p_BufferIndex);
		if (FAILED(hr))
		{
			assert(!"インデックス・バッファの作成ができません");
			return;
		}

	}

	// objファイルのロード
	void Static_mesh::loadObjFile(ID3D11Device * p_Device, const wchar_t * obj_filename, bool flipping_v_coordinates/*UNIT13*/)
	{
		//+ ロード--------------------------------
		std::vector<VERTEX> vertices;
		std::vector<u_int> indices;
		u_int current_index = 0;

		std::vector<DirectX::XMFLOAT3> positions;
		std::vector<DirectX::XMFLOAT2> texcoords;//! UNIT13追加
		std::vector<DirectX::XMFLOAT3> normals;

		std::vector<std::wstring> mtl_filenames;//! UNIT13追加

												///////////////////////////
												// OBJファイルロード
												///////////////////////////
		std::wifstream fin(obj_filename);
		_ASSERT_EXPR(fin, L"OBJ file not found.");
		wchar_t command[256];
		while (fin)
		{
			fin >> command;
			if (0 == wcscmp(command, L"v"))			//頂点位置情報読み取り
			{
				float x, y, z;
				fin >> x >> y >> z;
				positions.push_back(DirectX::XMFLOAT3(x, y, z));
				fin.ignore(1024, L'\n');//? L
			}
			// UNIT.13
			else if (0 == wcscmp(command, L"vt"))		//uv座標読み取り
			{
				float u, v;
				fin >> u >> v;
				texcoords.push_back(DirectX::XMFLOAT2(u, flipping_v_coordinates ? 1.0f - v : v));
				fin.ignore(1024, L'\n');
			}
			else if (0 == wcscmp(command, L"vn"))		//法線情報読み取り
			{
				FLOAT i, j, k;
				fin >> i >> j >> k;
				normals.push_back(DirectX::XMFLOAT3(i, j, k));
				fin.ignore(1024, L'\n');
			}
			else if (0 == wcscmp(command, L"f"))		//頂点番号情報読み取り//(リソースの番号情報読み取り)
			{
				static u_int index = 0;
				for (u_int i = 0; i < 3; i++)
				{
					VERTEX vertex;
					u_int v, vt, vn;

					fin >> v;
					vertex.position = positions[v - 1];
					if (L'/' == fin.peek())
					{
						fin.ignore();
						if (L'/' != fin.peek())
						{
							fin >> vt;
							// UNIT.13
							vertex.texcoord = texcoords[vt - 1];
						}
						if (L'/' == fin.peek())
						{
							fin.ignore();
							fin >> vn;
							///if (vn > 100)vn = 1;
							vertex.normal = normals[vn - 1];

						}
					}
					vertices.push_back(vertex);
					indices.push_back(current_index++);
				}
				fin.ignore(1024, L'\n');
			}
			// UNIT.13
			else if (0 == wcscmp(command, L"mtllib"))	//mtlファイル名読み取り
			{
				wchar_t mtllib[256];
				fin >> mtllib;
				mtl_filenames.push_back(mtllib);
			}
			// UNIT.14
			else if (0 == wcscmp(command, L"usemtl"))	// Question 使うmtlをサブセット構造体にセット
			{
				wchar_t usemtl[MAX_PATH] = { 0 };
				fin >> usemtl;

				SUBSET CurrentSubset = {};
				CurrentSubset.usemtl = usemtl;
				CurrentSubset.index_start = (u_int)indices.size();
				subset.push_back(CurrentSubset);
			}
			else
			{
				fin.ignore(1024, L'\n');
			}
		}
		fin.close();

		// バッファ生成
		create_buffers(p_Device, vertices.data(), (int)vertices.size(), indices.data(), (int)indices.size());

		// UNIT.14
		std::vector<SUBSET>::reverse_iterator iterator = subset.rbegin();
		iterator->index_count = (u_int)indices.size() - iterator->index_start;
		for (iterator = subset.rbegin() + 1; iterator != subset.rend(); ++iterator)
		{
			iterator->index_count = (iterator - 1)->index_start - iterator->index_start;
		}





		// UNIT.13
		//////////////////////////
		// テクスチャの読み込み
		/////////////////////////
		std::wstring texture_filename;
		wchar_t mtl_filenameCopy[256];//mtlファイル名
									  ///const wchar_t FILENAME[] = L"Mr.Incredible.mtl";
									  // .objのファイル名と.mtlのファイル名を結合
		CombineResourcePath(mtl_filenameCopy, obj_filename, mtl_filenames[0].c_str());
		std::wifstream fin2(mtl_filenameCopy);
		_ASSERT_EXPR(fin2, L"MTL file not found.");

		wchar_t command2[256] = { 0 };
		while (fin2)
		{
			fin2 >> command2;
			if (0 == wcscmp(command2, L"#"))			//mtlファイルのコメント文を破棄
			{
				fin2.ignore(1024, L'\n');
			}
			else if (0 == wcscmp(command2, L"map_Kd"))	//テクスチャのカラー情報読み取り(マテリアルの拡散反射率にリンクされる)
			{
				fin2.ignore();
				wchar_t map_Kd[256];
				fin2 >> map_Kd;
				CombineResourcePath(map_Kd, obj_filename, map_Kd);
				//texture_filename = map_Kd;//delete UNIT.14
				materials.rbegin()->map_Kd = map_Kd;
				fin2.ignore(1024, L'\n');
			}
			// UNIT.14
			else if (0 == wcscmp(command2, L"newmtl"))	//マテリアル名の記述読み取り
			{
				fin2.ignore();
				wchar_t newmtl[256];
				MATERIAL materialCopy;
				fin2 >> newmtl;
				materialCopy.newmtl = newmtl;
				materials.push_back(materialCopy);
			}
			// UNIT.14
			else if (0 == wcscmp(command2, L"Ka"))		//アンビエント反射rgb値読み取り
			{
				//反射率 色
				float r, g, b;
				fin2 >> r >> g >> b;
				materials.rbegin()->Ka = DirectX::XMFLOAT3(r, g, b);
				fin2.ignore(1024, L'\n');
			}
			// UNIT.14
			else if (0 == wcscmp(command2, L"Kd"))		//減光反射の色読み取り
			{
				float r, g, b;
				fin2 >> r >> g >> b;
				materials.rbegin()->Kd = DirectX::XMFLOAT3(r, g, b);
				fin2.ignore(1024, L'\n');
			}
			// UNIT.14
			else if (0 == wcscmp(command2, L"Ks"))		//鏡面反射率を指定読み取り
			{
				float r, g, b;
				fin2 >> r >> g >> b;
				materials.rbegin()->Ks = DirectX::XMFLOAT3(r, g, b);
				fin2.ignore(1024, L'\n');
			}
			// UNIT.14
			else if (0 == wcscmp(command2, L"illum"))		//照明モデル読み取り(イルミネーションモデル)
			{
				u_int illum;
				fin2 >> illum;
				materials.rbegin()->illum = illum;
				fin2.ignore(1024, L'\n');
			}
			else//未実装または認識できないコマンド
			{
				fin2.ignore(1024, L'\n');
			}
		}

	}







#define VERTEX_BUFFER_NUM 1//頂点バッファの数
	//ここから別スレッドで並列処理
	//ステートなどの描画状態を引き継げないため再設定
	void Static_mesh::ThreadFunc(ID3D11CommandList** ppCommandList, ID3D11DeviceContext * pDeferredContext, const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 & materialColor, bool FlagPaint)
	{


		UINT stride[VERTEX_BUFFER_NUM] = { sizeof(VERTEX) };	 //頂点バッファにふくまれる頂点データのサイズ。
		UINT offset[VERTEX_BUFFER_NUM] = { 0 };					 //頂点バッファのオフセット
		pDeferredContext->IASetVertexBuffers(0, VERTEX_BUFFER_NUM, &p_BufferVs, stride, offset);
		// �AIAにインデックスバッファを設定
		pDeferredContext->IASetIndexBuffer(p_BufferIndex, DXGI_FORMAT_R32_UINT, 0);

		// 描画するプリミティブ種類の設定
		pDeferredContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//３角形リストを描画する
																						// 入力レイアウト・オブジェクトの設定
		pDeferredContext->IASetInputLayout(p_InputLayout);

		pDeferredContext->VSSetShader(p_VertexShader, nullptr, 0);
		pDeferredContext->PSSetShader(p_PixelShader, nullptr, 0);
		//震度ステンシルステート
		pDeferredContext->OMSetDepthStencilState(p_DepthStencilState, 0);//震度ステンシルステート

																		 // �Cラスタライザ・ステート・オブジェクトの設定
		pDeferredContext->RSSetState((FlagPaint ? p_RasterizerStatePaint : p_RasterizerStateLine));//ラスタライザステート
																								   ///pDeferredContext->RSSetScissorRects();//シザー短形
																								   // UNIT13
																								   // UNIT14
																								   // シェーダリソースのセット
		for (auto& it : materials)
		{
			// �Bコンスタントバッファを設定+		light_direction	{x=0.000000000 y=-1.00000000 z=0.000000000 ...}	DirectX::XMFLOAT4

			CONSTANT_BUFFER data;
			data.wvp = wvp;
			data.world = world;
			data.light_direction = lightVector;
			//data.material_color = materialColor;
			data.material_color.x = materialColor.x*it.Kd.x;
			data.material_color.y = materialColor.y*it.Kd.y;
			data.material_color.z = materialColor.z*it.Kd.z;
			data.material_color.w = materialColor.w;

			pDeferredContext->UpdateSubresource(p_BufferConst, 0, nullptr, &data, 0, 0);//情報を定数バッファへコピー
			pDeferredContext->VSSetConstantBuffers(0, VERTEX_BUFFER_NUM, &p_BufferConst);//定数バッファをシェーダへセット
																						 //
																						 // シェーダーリソースビュー（テクスチャ）
			pDeferredContext->PSSetShaderResources(0, 1, &it.ShaderResourceView);
			pDeferredContext->PSSetSamplers(0, 1, &p_SampleState);

			for (auto its : subset)
			{
				if (it.newmtl == its.usemtl)
				{
					pDeferredContext->DrawIndexed(its.index_count, its.index_start, 0);//インデックスずけされているプリミティブの描画
				}
			}
		}
		//コマンドリスト作成
		pDeferredContext->FinishCommandList(FALSE, ppCommandList);
		// --------------

	}
#undef VERTEX_BUFFER_NUM
}

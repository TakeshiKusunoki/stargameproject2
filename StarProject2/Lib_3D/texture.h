#pragma once
#include <d3d11.h>


HRESULT load_texture_from_file(ID3D11Device* p_Device, const wchar_t* file_name, ID3D11ShaderResourceView** ShaderResourceView, D3D11_TEXTURE2D_DESC* Texture2DDesc);
//ファイルパスを結合//(入ってるフォルダパスを１階層結合)
void CombineResourcePath(wchar_t(&combind_resource_path)[256], const wchar_t* obj_fileName, const wchar_t* resource_fileName);
//ダミーテクスチャを貼る
HRESULT make_dummy_texture(ID3D11Device *device, ID3D11ShaderResourceView **shader_resource_view);


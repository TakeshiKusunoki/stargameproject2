#ifndef INCLUDED_POLIGON3D
#define INCLUDED_POLIGON3D
/**
* @file Poligon3D.h
* @brief Poligon3Dクラスが記述されている
* @details 詳細な説明
*/

#include <d3d11.h>
#include <DirectXMath.h>//DirectX::XMFLOAT3のため
#include "VertexPoligon3D.h"
//追加



namespace Lib_3D {
	class VertexPoligon3D;
	/**
	* @brief 3頂点ポリゴン
	* @par 詳細
	* 3頂点指定型のスプライト
	*/
	class Poligon3D
	{
	private:
		//! 頂点数
		static const size_t VERTEX_NUM = 3;
		struct VERTEX_SPR3D
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT2 texcoord;
		};
		//! コンスタントバッファ
		struct CONSTANT_BUFFER
		{
			DirectX::XMFLOAT4X4 wvp;				//! ワールド・ビュー・プロジェクション合成行列
			DirectX::XMFLOAT4X4 world;			//! ワールド変換行列
			DirectX::XMFLOAT4 material_color;		//! 材質色
			DirectX::XMFLOAT4 light_direction;		//! ライト進行方向
			DirectX::XMFLOAT4 cameraPos;			//! カメラ位置
			DirectX::XMFLOAT4 lightColor;			//! 光の色
			DirectX::XMFLOAT4 nyutoralLightColor;  //! 環境光
		};
	private:
		VertexPoligon3D* _vertexPoligon3D;//! ロードしたモデルデータ(頂点バッファ)
		ID3D11Buffer* p_BufferConst;//! （定数バッファ
		ID3D11Buffer* p_Buffer;

	public:
		Poligon3D(ID3D11Device * pDevice, VertexPoligon3D* vertexPoligon3D);
		~Poligon3D();
	public:
		void VertexChange(VertexPoligon3D* vertexPoligon3D);
		//------------------------------------------------------
		//  スプライト描画
		//------------------------------------------------------
		/**引数
		* @brief ３角形ポリゴン描画
		* @param[in] p_DeviceContext	:	デバイスコンテキスト
		* @param[in] wvp				:	ワールド・ビュー・プロジェクション合成行列
		* @param[in] world				:	ワールド変換行列
		* @param[in] position[3]			:	2D座標
		* @param[in] texPos[3]			:	テクスチャ3頂点座標
		* @param[in] lightVector			:	ライト進行方向
		* @param[in] materialColor		:	材質色
		* @param[in] cameraPos 			:	カメラ位置
		* @param[in] lightColor 			:	光の色
		* @param[in] nyutoralLightColor	:	環境光
		* @param[in] FlagPaint			:	"線or塗りつぶし"描画フラグ
		* @details 変形する３角形ポリゴン<br>
		* 光の色も反映する
		*/
		void RenderPoligon3D(ID3D11DeviceContext* p_DeviceContext,
			const DirectX::XMFLOAT4X4& wvp,
			const DirectX::XMFLOAT4X4& world,
			const DirectX::XMFLOAT3 position[VERTEX_NUM],
			const DirectX::XMFLOAT2 texPos[VERTEX_NUM],
			const DirectX::XMFLOAT4& lightVector = { 1,1,1,0 },
			const DirectX::XMFLOAT4& materialColor = { 1,1,1,1 },
			const DirectX::XMFLOAT4& cameraPos = { 0, 0, 20,1 },
			const DirectX::XMFLOAT4& lightColor = { 0.995f,0.995f,0.999f,0.99f },
			const DirectX::XMFLOAT4& nyutoralLightColor = { 0.20f,0.21f,0.20f,1 },
			bool  FlagPaint = false
		) const;
	};
}



#endif// !INCLUDED_POLIGON3D
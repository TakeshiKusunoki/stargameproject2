//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
#include "Sprite3D.h"
#include "vector.h"
#include <cstdio>
//追加
//#define ToRadian(x) DirectX::XMConvertToRadians(x)
namespace Lib_3D {
	// 画像
	Sprite3D::Sprite3D(ID3D11Device* p_Device, const wchar_t* texture_filename)
	{
		HRESULT hr = S_OK;

		//頂点生成
		{
			VECTOR3 Normal = { 0.0f, 0.0f, -1.0f };//法線
			VERTEX_SPR3D vertices[] =
			{
				{ DirectX::XMFLOAT3(-0.5,+0.5,0),Normal,DirectX::XMFLOAT2(0,0) },
				{ DirectX::XMFLOAT3(+0.5,+0.5,0),Normal,DirectX::XMFLOAT2(0,1) },
				{ DirectX::XMFLOAT3(-0.5,-0.5,0),Normal,DirectX::XMFLOAT2(1,0) },
				{ DirectX::XMFLOAT3(+0.5,-0.5,0),Normal,DirectX::XMFLOAT2(1,1) },
			};


			// 頂点バッファ作成(頂点データをDirect3Dのパイプラインに流し込む為のバッファーを作成します)
			D3D11_BUFFER_DESC bufferdesk;
			ZeroMemory(&bufferdesk, sizeof(bufferdesk));
			bufferdesk.ByteWidth = sizeof(vertices);
			bufferdesk.Usage = D3D11_USAGE_DYNAMIC;
			bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			bufferdesk.MiscFlags = 0;
			bufferdesk.StructureByteStride = sizeof(DirectX::XMFLOAT3);//float?

																	   // サブリソースの初期化に使用されるデータを指定します。
			D3D11_SUBRESOURCE_DATA subResourceData;
			ZeroMemory(&subResourceData, sizeof(subResourceData));
			subResourceData.pSysMem = vertices;//初期化データへのポインターです。
			subResourceData.SysMemPitch = 0;//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
			subResourceData.SysMemSlicePitch = 0;//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。

												 // バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
			hr = p_Device->CreateBuffer(&bufferdesk, &subResourceData, &p_Buffer);
			if (FAILED(hr))
			{
				assert(!"!  sprite3D CreateBuffer faile");
				return;
			}
		}

		// 入力オブジェクトの生成
		// 入力レイアウト定義(インプットレイアウト)

		// 頂点データの構造を記述
		D3D11_INPUT_ELEMENT_DESC inputElementDesk[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
		};
		UINT numElements = ARRAYSIZE(inputElementDesk);

		//リソースマネージャー生成
		resourceManager = new ResourceManager;
		bool f = true;
		f = resourceManager->LoadVertexShader(p_Device, "Shader\\Sprite3D_vs.cso", inputElementDesk, numElements, &p_VertexShader, &p_InputLayout);
		if (!f)
		{
			assert(!"! sprite3D LoadVertexShader faile");
			return;
		}
		//////////////////////////////////////
		//Custmized by UNIT5
		//////////////////////////////////////
		// ピクセルシェーダーオブジェクトの生成
		f = resourceManager->LoadPixelShader(p_Device, "Shader\\Sprite3D_ps.cso", &p_PixelShader);
		if (!f)
		{
			assert(!"! sprite3D LoadPixelShader faile");
			return;
		}
		// ラスタライザーステート
		D3D11_RASTERIZER_DESC rasteriserDesk;
		ZeroMemory(&rasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
		rasteriserDesk.FillMode = D3D11_FILL_SOLID;//レンダリング時に使用する描画モードを決定します
		rasteriserDesk.CullMode = D3D11_CULL_NONE;//特定の方向を向いている三角形の描画の有無を示します。
		rasteriserDesk.DepthClipEnable = false;//。UNIT5
		rasteriserDesk.FrontCounterClockwise = true;//三角形が前向きか後ろ向きかを決定します。
													//rasteriserDesk.DepthBias =
		hr = p_Device->CreateRasterizerState(&rasteriserDesk, &p_RasterizerState);
		if (FAILED(hr))
		{
			assert(!"! sprite3D CreateRasterizerState faile");
			return;
		}

		////////////////////////////////////////////////////
		//Added by U4  画像について
		//Custmized by UNIT5
		////////////////////////////////////////////////////
		// テクスチャ画像読み込み
		f = resourceManager->LoadShaderResourceView(p_Device, texture_filename, &p_ShaderResourceView, &TEXTURE2D_DESC);
		if (!f)
		{
			assert(!"! sprite3D LoadShaderResourceView faile");
			return;
		}
		//TEXTURE2D_DESC.Width �A画像幅

		// サンプラーステートオブジェクト(ID3D11SampleState)の生成
		D3D11_SAMPLER_DESC samplerDesk;
		ZeroMemory(&samplerDesk, sizeof(samplerDesk));
		samplerDesk.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		samplerDesk.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesk.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesk.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesk.MipLODBias = 0;//みっぷマップレベルからのバイアス
		samplerDesk.MaxAnisotropy = 16;//違法性保管を使用している場合の限界値
		samplerDesk.ComparisonFunc = D3D11_COMPARISON_ALWAYS;//比較オプション
		samplerDesk.BorderColor[0] = 1;
		samplerDesk.BorderColor[1] = 1;
		samplerDesk.BorderColor[2] = 1;
		samplerDesk.BorderColor[3] = 1;
		samplerDesk.MinLOD = 0;
		samplerDesk.MaxLOD = D3D11_FLOAT32_MAX;//アクセス可能なみっぷマップの上限値

		hr = p_Device->CreateSamplerState(&samplerDesk, &p_SamplerState);//ピクセルシェーダーに送る
		if (FAILED(hr))
		{
			assert(!"! sprite3D CreateSamplerState faile");
			return;
		}

		////////////////////////////////////////
		//Added by Unit6
		///////////////////////////////////////
		//深度ステンシルステート
		//D3D11_DEPTH_STENCIL_DESC DepthDesc;
		//ZeroMemory(&DepthDesc, sizeof(DepthDesc));
		//DepthDesc.DepthEnable = false;						//深度テストを使用可能にします。
		//DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//深度ステンシル バッファーの中で、深度データによる変更が可能な部分を識別します
		//DepthDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;				//深度データを既存の深度データと比較する関数です。
		//DepthDesc.StencilEnable = false;						//ステンシル テストを使用可能にします。							//法線がカメラと逆方向を向いているサーフェスを持つピクセルの深度テストとステンシル テストの結果を使用する方法を識別します
		//hr = p_Device->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
		//if (FAILED(hr))
		//{
		//	assert(!"! sprite3D CreateDepthStencilState faile");
		//	return;
		//}
		D3D11_DEPTH_STENCIL_DESC DepthDesc;
		ZeroMemory(&DepthDesc, sizeof(DepthDesc));
		DepthDesc.DepthEnable = TRUE;									//深度テストあり
		DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//書き込む
		DepthDesc.DepthFunc = D3D11_COMPARISON_LESS;					//手前の物体を描画
		DepthDesc.StencilEnable = FALSE;								//ステンシル テストなし
		DepthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;		//ステンシル読み込みマスク
		DepthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;	//ステンシル書き込みマスク
																		//面が表を向いている場合のステンシルステートの設定
		DepthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
		DepthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
																		  //面が裏を向いている場合のステンシルステートの設定
		DepthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR/*D3D11_STENCIL_OP_KEEP*/;	  //維持
		DepthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
		hr = p_Device->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
		if (FAILED(hr))
		{
			assert(!"深度ステンシル ステート オブジェクトの生成ができません");
			return;
		}

		// �H定数バッファオブジェクトの生成
		D3D11_BUFFER_DESC ConstantBufferDesc;
		ZeroMemory(&ConstantBufferDesc, sizeof(ConstantBufferDesc));
		ConstantBufferDesc.Usage = D3D11_USAGE_DEFAULT;			//動的使用法
		ConstantBufferDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;//定数バッファ
		ConstantBufferDesc.CPUAccessFlags = 0;//CPUから書き込む
		ConstantBufferDesc.MiscFlags = 0;
		ConstantBufferDesc.ByteWidth = sizeof(CONSTANT_BUFFER);
		ConstantBufferDesc.StructureByteStride = 0;


		hr = p_Device->CreateBuffer(&ConstantBufferDesc, nullptr, &p_BufferConst);
		if (FAILED(hr))
		{
			assert(!"定数バッファオブジェクトの生成ができません");
			return;
		}
	}




#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	Sprite3D::~Sprite3D()
	{
		////////////////////////////////////////
		//Added by Unit6
		///////////////////////////////////////
		RELEASE_IF(p_DepthStencilState);
		RELEASE_IF(p_SamplerState);
		RELEASE_IF(p_RasterizerState);
		RELEASE_IF(p_Buffer);
		RELEASE_IF(p_BufferConst);
		resourceManager->ReleaseShaderResourceView(p_ShaderResourceView);
		resourceManager->ReleasePixelShader(p_PixelShader);
		resourceManager->ReleaseVertexShader(p_VertexShader, p_InputLayout);
		RELEASE_IF(resourceManager);
	}
#undef RELEASE_IF
#undef DELETE_IF


	//
	////--------------------------------
	////  スプライト2D描画
	////--------------------------------
	//void Sprite3D::render(ID3D11DeviceContext* context,
	//	const VECTOR2& position, const VECTOR2& scale,
	//	const VECTOR2& texPos, const VECTOR2& texSize,
	//	const VECTOR2& center, float angle,
	//	const VECTOR4& color) const
	//{
	//	if (scale.x * scale.y == 0.0f) return;
	//
	//	D3D11_VIEWPORT viewport;
	//	UINT numViewports = 1;
	//	context->RSGetViewports(&numViewports, &viewport);
	//
	//	float tw = texSize.x;
	//	float th = texSize.y;
	//	if (tw <= 0.0f || th <= 0.0f)
	//	{
	//		tw = (float)TEXTURE2D_DESC.Width;
	//		th = (float)TEXTURE2D_DESC.Height;
	//	}
	//
	//	VERTEX_SPR3D vertices[] = {
	//		{ VECTOR3(-0.0f, +1.0f, 0), color, VECTOR2(0, 1) },
	//		{ VECTOR3(+1.0f, +1.0f, 0), color, VECTOR2(1, 1) },
	//		{ VECTOR3(-0.0f, -0.0f, 0), color, VECTOR2(0, 0) },
	//		{ VECTOR3(+1.0f, -0.0f, 0), color, VECTOR2(1, 0) },
	//	};
	//
	//	float sinValue = sinf(angle);
	//	float cosValue = cosf(angle);
	//	float mx = (tw * scale.x) / tw * center.x;
	//	float my = (th * scale.y) / th * center.y;
	//	for (int i = 0; i < 4; i++)
	//	{
	//		vertices[i].position.x *= (tw * scale.x);
	//		vertices[i].position.y *= (th * scale.y);
	//
	//		vertices[i].position.x -= mx;
	//		vertices[i].position.y -= my;
	//
	//		float rx = vertices[i].position.x;
	//		float ry = vertices[i].position.y;
	//		vertices[i].position.x = rx * cosValue - ry * sinValue;
	//		vertices[i].position.y = rx * sinValue + ry * cosValue;
	//
	//		vertices[i].position.x += mx;
	//		vertices[i].position.y += my;
	//
	//		vertices[i].position.x += (position.x - scale.x * center.x);
	//		vertices[i].position.y += (position.y - scale.y * center.y);
	//
	//		vertices[i].position.x = vertices[i].position.x * 2 / viewport.Width - 1.0f;
	//		vertices[i].position.y = 1.0f - vertices[i].position.y * 2 / viewport.Height;
	//
	//		vertices[i].texcoord.x = (texPos.x + vertices[i].texcoord.x * tw) / TEXTURE2D_DESC.Width;
	//		vertices[i].texcoord.y = (texPos.y + vertices[i].texcoord.y * th) / TEXTURE2D_DESC.Height;
	//	}
	//
	//	D3D11_MAPPED_SUBRESOURCE msr;
	//	context->Map(p_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
	//	memcpy(msr.pData, vertices, sizeof(vertices));
	//	context->Unmap(p_Buffer, 0);
	//
	//	UINT stride = sizeof(VERTEX_SPR3D);
	//	UINT offset = 0;
	//	context->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);
	//	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	//	context->IASetInputLayout(p_InputLayout);
	//	context->RSSetState(p_RasterizerState);
	//	context->VSSetShader(p_VertexShader, nullptr, 0);
	//	context->PSSetShader(p_PixelShader, nullptr, 0);
	//
	//	context->PSSetShaderResources(0, 1, &p_ShaderResourceView);
	//	context->PSSetSamplers(0, 1, &p_SamplerState);
	//
	//	context->OMSetDepthStencilState(p_DepthStencilState, 1);
	//
	//	context->Draw(4, 0);
	//}
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//void Sprite3D::RenderVertexSprite(ID3D11DeviceContext * context, const VECTOR2 & position, const VECTOR2 & scale,
	//	const VECTOR2 & texPos, const VECTOR2 & texSize, const VECTOR2 & center, float angle, const VECTOR4 & color) const
	//{
	//	if (scale.x * scale.y == 0.0f) return;
	//
	//	D3D11_VIEWPORT viewport;
	//	UINT numViewports = 1;
	//	context->RSGetViewports(&numViewports, &viewport);
	//
	//	float tw = texSize.x;
	//	float th = texSize.y;
	//	if (tw <= 0.0f || th <= 0.0f)
	//	{
	//		tw = (float)TEXTURE2D_DESC.Width;
	//		th = (float)TEXTURE2D_DESC.Height;
	//	}
	//
	//	VERTEX_SPR3D vertices[] = {
	//		{ VECTOR3(-0.0f, +1.0f, 0), color, VECTOR2(0, 1) },
	//		{ VECTOR3(+1.0f, +1.0f, 0), color, VECTOR2(1, 1) },
	//		{ VECTOR3(-0.0f, -0.0f, 0), color, VECTOR2(0, 0) },
	//		{ VECTOR3(+1.0f, -0.0f, 0), color, VECTOR2(1, 0) },
	//	};
	//
	//	float sinValue = sinf(angle);
	//	float cosValue = cosf(angle);
	//	float mx = (tw * scale.x) / tw * center.x;
	//	float my = (th * scale.y) / th * center.y;
	//	for (int i = 0; i < 4; i++)
	//	{
	//		vertices[i].position.x *= (tw * scale.x);
	//		vertices[i].position.y *= (th * scale.y);
	//
	//		vertices[i].position.x -= mx;
	//		vertices[i].position.y -= my;
	//
	//		float rx = vertices[i].position.x;
	//		float ry = vertices[i].position.y;
	//		vertices[i].position.x = rx * cosValue - ry * sinValue;
	//		vertices[i].position.y = rx * sinValue + ry * cosValue;
	//
	//		vertices[i].position.x += mx;
	//		vertices[i].position.y += my;
	//
	//		vertices[i].position.x += (position.x - scale.x * center.x);
	//		vertices[i].position.y += (position.y - scale.y * center.y);
	//
	//		vertices[i].position.x = vertices[i].position.x * 2 / viewport.Width - 1.0f;
	//		vertices[i].position.y = 1.0f - vertices[i].position.y * 2 / viewport.Height;
	//
	//		vertices[i].texcoord.x = (texPos.x + vertices[i].texcoord.x * tw) / TEXTURE2D_DESC.Width;
	//		vertices[i].texcoord.y = (texPos.y + vertices[i].texcoord.y * th) / TEXTURE2D_DESC.Height;
	//	}
	//
	//	D3D11_MAPPED_SUBRESOURCE msr;
	//	context->Map(p_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
	//	memcpy(msr.pData, vertices, sizeof(vertices));
	//	context->Unmap(p_Buffer, 0);
	//
	//	UINT stride = sizeof(VERTEX_SPR3D);
	//	UINT offset = 0;
	//	context->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);
	//	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	//	context->IASetInputLayout(p_InputLayout);
	//	context->RSSetState(p_RasterizerState);
	//	context->VSSetShader(p_VertexShader, nullptr, 0);
	//	context->PSSetShader(p_PixelShader, nullptr, 0);
	//
	//	context->PSSetShaderResources(0, 1, &p_ShaderResourceView);
	//	context->PSSetSamplers(0, 1, &p_SamplerState);
	//
	//	context->OMSetDepthStencilState(p_DepthStencilState, 1);
	//
	//	context->Draw(4, 0);
	//}
	//










	// 3Dのスプライト(4頂点)
	//0　1
	//
	//2　3
	void Sprite3D::RenderSprite3D(ID3D11DeviceContext * p_DeviceContext, const DirectX::XMFLOAT4X4 & wvp,
		const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT3 position[4], const DirectX::XMFLOAT2 texPos[4],
		const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 & materialColor, const DirectX::XMFLOAT4 & cameraPos,
		const DirectX::XMFLOAT4 & lightColor, const DirectX::XMFLOAT4 & nyutoralLightColor, bool FlagPaint) const
	{
		D3D11_VIEWPORT viewport;
		UINT numViewports = 1;
		p_DeviceContext->RSGetViewports(&numViewports, &viewport);

		VECTOR3 No = { 0.0f, 0.0f, -1.0f };//法線
										   //頂点シェーダーへの引数
		VERTEX_SPR3D vertices[] = {
			{ VECTOR3(-0.0f, +1.0f, 0),  No, VECTOR2(0, 1) },
			{ VECTOR3(+1.0f, +1.0f, 0),  No, VECTOR2(1, 1) },
			{ VECTOR3(-0.0f, -0.0f, 0),  No, VECTOR2(0, 0) },
			{ VECTOR3(+1.0f, -0.0f, 0),  No, VECTOR2(1, 0) },
		};
		//頂点シェーダーに引数を渡す
		for (int i = 0; i < 4; i++)
		{
			vertices[i].position.x = position[i].x / viewport.Width;
			vertices[i].position.y = position[i].y / viewport.Height;
			vertices[i].position.z = position[i].z;

			vertices[i].texcoord.x = texPos[i].x / TEXTURE2D_DESC.Width;
			vertices[i].texcoord.y = texPos[i].y / TEXTURE2D_DESC.Height;

		}
		if (rand() % 501 == 0)
		{
			printf("%f ", vertices[1].position.x);
			printf("%f ", vertices[1].position.y);
			printf("%f\n", vertices[1].position.z);
		}

		// �Bコンスタントバッファを設定+		light_direction	{x=0.000000000 y=-1.00000000 z=0.000000000 ...}	DirectX::XMFLOAT4
		CONSTANT_BUFFER constantData = {};//コンスタントバッファデータ
		constantData.wvp = wvp;
		constantData.world = world;
		constantData.light_direction = lightVector;
		constantData.material_color = materialColor;
		constantData.cameraPos = cameraPos;
		constantData.lightColor = lightColor;
		constantData.nyutoralLightColor = nyutoralLightColor;
		p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &constantData, 0, 0);//情報を定数バッファへコピー

		p_DeviceContext->VSSetConstantBuffers(0, 1, &p_BufferConst);//定数バッファをシェーダへセット
																	// 頂点を動的生成
		D3D11_MAPPED_SUBRESOURCE msr;
		p_DeviceContext->Map(p_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
		memcpy(msr.pData, vertices, sizeof(vertices));
		p_DeviceContext->Unmap(p_Buffer, 0);


		// 各種ステートを設定
		UINT stride = sizeof(VERTEX_SPR3D);
		UINT offset = 0;
		p_DeviceContext->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
		p_DeviceContext->IASetInputLayout(p_InputLayout);
		p_DeviceContext->RSSetState(p_RasterizerState);
		p_DeviceContext->VSSetShader(p_VertexShader, nullptr, 0);
		p_DeviceContext->PSSetShader(p_PixelShader, nullptr, 0);
		p_DeviceContext->PSSetShaderResources(0, 1, &p_ShaderResourceView);
		p_DeviceContext->PSSetSamplers(0, 1, &p_SamplerState);
		p_DeviceContext->OMSetDepthStencilState(p_DepthStencilState, 1);

		p_DeviceContext->Draw(4, 0);
	}


}


//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
#include "Poligon3D.h"
#include "vector.h"
#include <cstdio>
//#define ToRadian(x) DirectX::XMConvertToRadians(x)
//#define SCREEN_W(x) (framework::SCREEN_WIDTH[x])
//#define SCREEN_H(x) (framework::SCREEN_HEIGHT[x])
namespace Lib_3D {
	// 画像
	Poligon3D::Poligon3D(ID3D11Device * pDevice, VertexPoligon3D * vertexPoligon3D)
		:_vertexPoligon3D(vertexPoligon3D)
	{
		HRESULT hr = S_OK;
		//頂点生成
		{
			VECTOR3 Normal = { 0.0f, 0.0f, -1.0f };//法線
			VERTEX_SPR3D vertices[] =
			{
				{ DirectX::XMFLOAT3(-0.5,+0.5,0),Normal,DirectX::XMFLOAT2(0,0) },
				{ DirectX::XMFLOAT3(+0.5,+0.5,0),Normal,DirectX::XMFLOAT2(0,1) },
				{ DirectX::XMFLOAT3(0.0,0.0,0),Normal,DirectX::XMFLOAT2(0.5f,0.5f) },
			};


			// 頂点バッファ作成(頂点データをDirect3Dのパイプラインに流し込む為のバッファーを作成します)
			D3D11_BUFFER_DESC bufferdesk;
			ZeroMemory(&bufferdesk, sizeof(bufferdesk));
			bufferdesk.ByteWidth = sizeof(vertices);
			bufferdesk.Usage = D3D11_USAGE_DYNAMIC;
			bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			bufferdesk.MiscFlags = 0;
			bufferdesk.StructureByteStride = sizeof(DirectX::XMFLOAT3);//float?

																	   // サブリソースの初期化に使用されるデータを指定します。
			D3D11_SUBRESOURCE_DATA subResourceData;
			ZeroMemory(&subResourceData, sizeof(subResourceData));
			subResourceData.pSysMem = vertices;//初期化データへのポインターです。
			subResourceData.SysMemPitch = 0;//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
			subResourceData.SysMemSlicePitch = 0;//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。

												 // バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
			hr = pDevice->CreateBuffer(&bufferdesk, &subResourceData, &p_Buffer);
			if (FAILED(hr))
			{
				assert(!"!  Poligon3D CreateBuffer faile");
				return;
			}
		}



		// �H定数バッファオブジェクトの生成
		D3D11_BUFFER_DESC ConstantBufferDesc;
		ZeroMemory(&ConstantBufferDesc, sizeof(ConstantBufferDesc));
		ConstantBufferDesc.Usage = D3D11_USAGE_DEFAULT;			//動的使用法
		ConstantBufferDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;//定数バッファ
		ConstantBufferDesc.CPUAccessFlags = 0;//CPUから書き込む
		ConstantBufferDesc.MiscFlags = 0;
		ConstantBufferDesc.ByteWidth = sizeof(CONSTANT_BUFFER);
		ConstantBufferDesc.StructureByteStride = 0;


		hr = pDevice->CreateBuffer(&ConstantBufferDesc, nullptr, &p_BufferConst);
		if (FAILED(hr))
		{
			assert(!"定数バッファオブジェクトの生成ができません");
			return;
		}
	}




#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	Poligon3D::~Poligon3D()
	{
		RELEASE_IF(p_BufferConst);
		RELEASE_IF(p_Buffer);
	}
	void Poligon3D::VertexChange(VertexPoligon3D* vertexPoligon3D)
	{
		_vertexPoligon3D = vertexPoligon3D;
	}
#undef RELEASE_IF
#undef DELETE_IF










	// 3Dのスプライト(3頂点)
	//0　1
	// 2
	void Poligon3D::RenderPoligon3D(ID3D11DeviceContext * p_DeviceContext, const DirectX::XMFLOAT4X4 & wvp,
		const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT3 position[VERTEX_NUM], const DirectX::XMFLOAT2 texPos[VERTEX_NUM],
		const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 & materialColor, const DirectX::XMFLOAT4 & cameraPos,
		const DirectX::XMFLOAT4 & lightColor, const DirectX::XMFLOAT4 & nyutoralLightColor, bool FlagPaint) const
	{
		D3D11_VIEWPORT viewport;
		UINT numViewports = 1;
		p_DeviceContext->RSGetViewports(&numViewports, &viewport);

		VECTOR3 No = { 0.0f, 0.0f, -1.0f };//法線
										   //頂点シェーダーへの引数
		VERTEX_SPR3D vertices[] = {
			{ VECTOR3(-0.0f, +1.0f, 0),  No, VECTOR2(0, 1) },
			{ VECTOR3(+1.0f, +1.0f, 0),  No, VECTOR2(1, 1) },
			{ VECTOR3(0.5f, 0.5f, 0),  No, VECTOR2(0.5f, 0.5f) },
		};
		//頂点シェーダーに引数を渡す
		for (int i = 0; i < VERTEX_NUM; i++)
		{
			vertices[i].position.x = position[i].x / viewport.Width;
			vertices[i].position.y = position[i].y / viewport.Height;
			vertices[i].position.z = position[i].z;

			vertices[i].texcoord.x = texPos[i].x / _vertexPoligon3D->TEXTURE2D_DESC.Width;
			vertices[i].texcoord.y = texPos[i].y / _vertexPoligon3D->TEXTURE2D_DESC.Height;

		}
	/*	if (rand() % 501 == 0)
		{
			printf("\npoligon3D\n");
			for (int i = 0; i < VERTEX_NUM; i++)
			{
				printf("%f ", vertices[i].position.x);
				printf("%f ", vertices[i].position.y);
				printf("%f\n", vertices[i].position.z);
			}
		}*/

		// �Bコンスタントバッファを設定+		light_direction	{x=0.000000000 y=-1.00000000 z=0.000000000 ...}	DirectX::XMFLOAT4
		CONSTANT_BUFFER constantData = {};//コンスタントバッファデータ
		constantData.wvp = wvp;
		constantData.world = world;
		constantData.light_direction = lightVector;
		constantData.material_color = materialColor;
		constantData.cameraPos = cameraPos;
		constantData.lightColor = lightColor;
		constantData.nyutoralLightColor = nyutoralLightColor;
		p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &constantData, 0, 0);//情報を定数バッファへコピー

		p_DeviceContext->VSSetConstantBuffers(0, 1, &p_BufferConst);//定数バッファをシェーダへセット
																	// 頂点を動的生成
		D3D11_MAPPED_SUBRESOURCE msr;
		p_DeviceContext->Map(p_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
		memcpy(msr.pData, vertices, sizeof(vertices));
		p_DeviceContext->Unmap(p_Buffer, 0);


		// 各種ステートを設定
		UINT stride = sizeof(VERTEX_SPR3D);
		UINT offset = 0;
		p_DeviceContext->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
		p_DeviceContext->IASetInputLayout(_vertexPoligon3D->p_InputLayout);
		p_DeviceContext->RSSetState(_vertexPoligon3D->p_RasterizerState);
		p_DeviceContext->VSSetShader(_vertexPoligon3D->p_VertexShader, nullptr, 0);
		p_DeviceContext->PSSetShader(_vertexPoligon3D->p_PixelShader, nullptr, 0);
		p_DeviceContext->PSSetShaderResources(0, 1, &_vertexPoligon3D->p_ShaderResourceView);
		p_DeviceContext->PSSetSamplers(0, 1, &_vertexPoligon3D->p_SamplerState);
		p_DeviceContext->OMSetDepthStencilState(_vertexPoligon3D->p_DepthStencilState, 1);

		p_DeviceContext->Draw(VERTEX_NUM, 0);
	}

}


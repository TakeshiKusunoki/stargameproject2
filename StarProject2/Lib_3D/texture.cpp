#include "texture.h"
#include "misc.h"
#include "WICTextureLoader.h"
#include <wrl.h>
#include <map>

	// UNIT13
	HRESULT load_texture_from_file(ID3D11Device* p_Device, const wchar_t* file_name, ID3D11ShaderResourceView** ShaderResourceView, D3D11_TEXTURE2D_DESC* Texture2DDesc)
	{
		HRESULT hr = S_OK;
		Microsoft::WRL::ComPtr<ID3D11Resource> resource;
		static std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> cache;
		auto it = cache.find(file_name);
		if (it != cache.end())
		{
			*ShaderResourceView = it->second.Get();
			(*ShaderResourceView)->AddRef();
			(*ShaderResourceView)->GetResource(resource.GetAddressOf());
		}
		else
		{
			hr = DirectX::CreateWICTextureFromFile(p_Device, file_name, resource.GetAddressOf(), ShaderResourceView);
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
			cache.insert(std::make_pair(file_name, *ShaderResourceView));
		}
		Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2d;
		hr = resource.Get()->QueryInterface<ID3D11Texture2D>(texture2d.GetAddressOf());
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		texture2d->GetDesc(Texture2DDesc);

		return hr;
	}

	// UNIT.16
	HRESULT make_dummy_texture(ID3D11Device *device, ID3D11ShaderResourceView **shader_resource_view)
	{
		HRESULT hr = S_OK;

		D3D11_TEXTURE2D_DESC texture2d_desc = {};
		texture2d_desc.Width = 1;
		texture2d_desc.Height = 1;
		texture2d_desc.MipLevels = 1;
		texture2d_desc.ArraySize = 1;
		texture2d_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texture2d_desc.SampleDesc.Count = 1;
		texture2d_desc.SampleDesc.Quality = 0;
		texture2d_desc.Usage = D3D11_USAGE_DEFAULT;
		texture2d_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texture2d_desc.CPUAccessFlags = 0;
		texture2d_desc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA subresource_data = {};
		u_int color = 0xFFFFFFFF;
		subresource_data.pSysMem = &color;
		subresource_data.SysMemPitch = 4;
		subresource_data.SysMemSlicePitch = 4;

		ID3D11Texture2D *texture2d;
		hr = device->CreateTexture2D(&texture2d_desc, &subresource_data, &texture2d);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		D3D11_SHADER_RESOURCE_VIEW_DESC shader_resource_view_desc = {};
		shader_resource_view_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		shader_resource_view_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		shader_resource_view_desc.Texture2D.MipLevels = 1;

		hr = device->CreateShaderResourceView(texture2d, &shader_resource_view_desc, shader_resource_view);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		texture2d->Release();

		return hr;
	}

	/*
	e.g.
	obj_filename <= L"data/bison.obj"
	resource_filename <= L"/user/textures/bison.png"
	combined_resource_path => L"/data/bison.png"
	*/
	void CombineResourcePath(wchar_t(&combind_resource_path)[256], const wchar_t* obj_fileName, const wchar_t* resource_fileName)
	{
		const wchar_t delimiters[] = { L'\\', L'/' };
		// obj_filename からディレクトリを抽出する
		wchar_t directory[256] = {};
		for (wchar_t it : delimiters)
		{
			wchar_t* p = wcsrchr(const_cast<wchar_t*>(obj_fileName), it);//ストリング内でワイド文字が最後に現れる位置の検出
			if (p)
			{
				// ディレクトリに名前コピー
				memcpy_s(directory, 256, obj_fileName, (p - obj_fileName + 1) * sizeof(wchar_t));
				break;
			}
		}
		// resource_filename からファイル名を抽出
		wchar_t filename[256];
		wcscpy_s(filename, resource_fileName);
		for (wchar_t it : delimiters)
		{
			wchar_t* p = wcsrchr(filename, it);//ストリング内でワイド文字が最後に現れる位置の検出
			if (p)
			{
				wcscpy_s(filename, p + 1);//ワイド文字列をコピーする。
				break;
			}
		}
		// ディレクトリとファイル名を結合する
		wcscpy_s(combind_resource_path, directory);
		wcscat_s(combind_resource_path, filename);

	}




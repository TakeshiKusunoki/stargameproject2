#ifndef INCLUDED_VECTOR
#define INCLUDED_VECTOR
//******************************************************************************
//
//
//      Vectorクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <DirectXMath.h>

//==============================================================================
//
//      DirectX::XMFLOAT2クラス
//
//==============================================================================

class VECTOR2 : public DirectX::XMFLOAT2
{
public:
	VECTOR2() : DirectX::XMFLOAT2(0, 0) {}
	VECTOR2(float x, float y) : DirectX::XMFLOAT2(x, y) {}
	VECTOR2(const DirectX::XMFLOAT2& v) { x = v.x; y = v.y; }
	~VECTOR2() {}

	DirectX::XMFLOAT2 &operator=(const VECTOR2&);
	DirectX::XMFLOAT2 &operator+=(const DirectX::XMFLOAT2&);
	DirectX::XMFLOAT2 &operator-=(const DirectX::XMFLOAT2&);
	DirectX::XMFLOAT2 &operator+=(const VECTOR2&);
	DirectX::XMFLOAT2 &operator-=(const VECTOR2&);
	DirectX::XMFLOAT2 &operator*=(float);
	DirectX::XMFLOAT2 &operator*=(const DirectX::XMFLOAT2&);
	DirectX::XMFLOAT2 &operator*=(const VECTOR2&);
	DirectX::XMFLOAT2 &operator/=(float);
	DirectX::XMFLOAT2 &operator/=(const DirectX::XMFLOAT2&);
	DirectX::XMFLOAT2 &operator/=(const VECTOR2&);

	DirectX::XMFLOAT2 operator+() const;
	DirectX::XMFLOAT2 operator-() const;

	DirectX::XMFLOAT2 operator+(const DirectX::XMFLOAT2&) const;
	DirectX::XMFLOAT2 operator-(const DirectX::XMFLOAT2&) const;
	DirectX::XMFLOAT2 operator*(float) const;
	DirectX::XMFLOAT2 operator*(const DirectX::XMFLOAT2&) const;
	DirectX::XMFLOAT2 operator*(const VECTOR2&) const;
	friend DirectX::XMFLOAT2 operator*(float, const DirectX::XMFLOAT2&);
	DirectX::XMFLOAT2 operator/(float) const;
	DirectX::XMFLOAT2 operator/(const DirectX::XMFLOAT2&) const;
	DirectX::XMFLOAT2 operator/(const VECTOR2&) const;

	bool operator == (const DirectX::XMFLOAT2&) const;
	bool operator != (const DirectX::XMFLOAT2&) const;
	bool operator == (const VECTOR2&) const;
	bool operator != (const VECTOR2&) const;
};

//------< プロトタイプ宣言 >-----------------------------------------------------

float vec2LengthSq(const DirectX::XMFLOAT2&);
float vec2Length(const DirectX::XMFLOAT2&);
const DirectX::XMFLOAT2& vec2Normalize(const DirectX::XMFLOAT2&, DirectX::XMFLOAT2&);

//==============================================================================
//
//      DirectX::XMFLOAT3クラス
//
//==============================================================================

class VECTOR3: public DirectX::XMFLOAT3
{
public:
	VECTOR3() : DirectX::XMFLOAT3(0, 0, 0) {}
	VECTOR3(float x, float y, float z) : DirectX::XMFLOAT3(x, y, z) {}
	VECTOR3(const DirectX::XMFLOAT3& v) { x = v.x; y = v.y; z = v.z; }
	VECTOR3(const DirectX::XMVECTOR& other) :DirectX::XMFLOAT3(){DirectX::XMVECTOR temp = other;DirectX::XMStoreFloat3(this, temp);}//! @brief ストア用のコンストラクタ

	~VECTOR3() {}

	//! @brief ロード用のキャスト
	operator DirectX::XMVECTOR() const;


	DirectX::XMFLOAT3 &operator=(const VECTOR3&);
	VECTOR3& operator=(const DirectX::XMVECTOR& other);//! @brief ストア用の代入

	DirectX::XMFLOAT3 &operator+=(const DirectX::XMFLOAT3&);
	DirectX::XMFLOAT3 &operator+=(const VECTOR3&);
	DirectX::XMFLOAT3 &operator-=(const DirectX::XMFLOAT3&);
	DirectX::XMFLOAT3 &operator-=(const VECTOR3&);
	DirectX::XMFLOAT3 &operator*=(float);
	DirectX::XMFLOAT3 &operator/=(float);
	DirectX::XMFLOAT3 &operator*=(const DirectX::XMFLOAT3&);
	DirectX::XMFLOAT3 &operator/=(const DirectX::XMFLOAT3&);
	DirectX::XMFLOAT3 &operator*=(const VECTOR3&);
	DirectX::XMFLOAT3 &operator/=(const VECTOR3&);

	DirectX::XMFLOAT3 operator+() const;
	DirectX::XMFLOAT3 operator-() const;

	DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3&) const;
	DirectX::XMFLOAT3 operator+(const VECTOR3&) const;
	DirectX::XMFLOAT3 operator+(float) const;
	DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3&) const;
	DirectX::XMFLOAT3 operator-(const VECTOR3&) const;
	DirectX::XMFLOAT3 operator-(float) const;
	DirectX::XMFLOAT3 operator*(float) const;
	DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3&) const;
	DirectX::XMFLOAT3 operator*(const VECTOR3&) const;
	friend DirectX::XMFLOAT3 operator*(float, const DirectX::XMFLOAT3&);
	DirectX::XMFLOAT3 operator/(float) const;
	DirectX::XMFLOAT3 operator/(const DirectX::XMFLOAT3&) const;
	DirectX::XMFLOAT3 operator/(const VECTOR3&) const;
	//VECTOR3 operator*();

	bool operator == (const DirectX::XMFLOAT3&) const;
	bool operator != (const DirectX::XMFLOAT3&) const;
	bool operator == (const VECTOR3&) const;
	bool operator != (const VECTOR3&) const;

	//! @brief 長さ
	float Length() const
	{
		return ((VECTOR3)DirectX::XMVector3Length(DirectX::XMVECTOR(*this))).x;
	}
	//! @brief 正規化
	VECTOR3 Normalize()
	{
		return (VECTOR3)DirectX::XMVector3Normalize(DirectX::XMVECTOR(*this));
	}
};

//==============================================================================
//
//      VECTOR4クラス
//
//==============================================================================

class VECTOR4 : public DirectX::XMFLOAT4
{
public:
	VECTOR4() : DirectX::XMFLOAT4(0, 0, 0, 0) {}
	VECTOR4(float x, float y, float z, float w) : DirectX::XMFLOAT4(x, y, z, w) {}
	~VECTOR4() {}
};

//******************************************************************************

#endif // !INCLUDED_VECTOR
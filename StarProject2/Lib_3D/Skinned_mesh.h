#pragma once
#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <windows.h>
#include <d3d11.h>
#include <wrl.h>
#include <DirectXMath.h>
#include <fbxsdk.h>
#include <vector>
#include "ResourceManager.h"

namespace Lib_3D {

	class Skinned_mesh
	{
	public:

		//Material diffuse;//材質
		// 材質
		struct Material
		{
			DirectX::XMFLOAT4 color = { 0.8f,0.8f,0.8f,1.0f };
			ID3D11ShaderResourceView* p_Shader_resource_view;
		};
		// UNIT.18--------------
		struct SUBSET
		{
			UINT index_start = 0;//start number of index buffer
			UINT index_count = 0;//number of vertices(indices)
			Material diffuse;
		};
		// UNIT.22
		//ボーン構造体
		struct BONE
		{
			DirectX::XMFLOAT4X4 transform;
		};

		// UNIT.23
		typedef std::vector<BONE> SKELTAL;
		struct SKELTAL_ANIMATION :public std::vector<SKELTAL>
		{
			float sampling_time = 1 / 24.0f;
			float animation_tick = 0.0f;
		};

		//----------------------------
		// UNIT.19
		struct MESH
		{
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_VertexBuffer;
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_IndexBuffer;
			std::vector<SUBSET> Subsets;
			DirectX::XMFLOAT4X4 GloabalTransform = {
				1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				0,0,0,1,
			};
			// UNIT.22
			//std::vector<Skinned_mesh::BONE> Skeletal;//ボーン構造体の可変長配列
			// UNIT.23
			SKELTAL_ANIMATION SkeltalAnimation;
		};
		std::vector<MESH> Meshes;

		//----------------------------
		struct VERTEX
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT2 texcoord;
#define MAX_BONE_INFLUENCES 4//ひとつの頂点が影響を受ける最大ボ−ン
			FLOAT bone_weights[MAX_BONE_INFLUENCES] = { 1,0,0,0 };
			INT bone_indices[MAX_BONE_INFLUENCES] = {};
		};
		//コンスタントバッファ(定数バッファ)
#define MAX_BONES 32
		struct CONSTANT_BUFFER
		{
			DirectX::XMFLOAT4X4 wvp;  //ワールド・ビュー・プロジェクション合成行列
			DirectX::XMFLOAT4X4 world;      //ワールド変換行列
			DirectX::XMFLOAT4 material_color;    //材質色
			DirectX::XMFLOAT4 light_direction;    //ライト進行方向
												  // UNIT.21
			DirectX::XMFLOAT4X4 bone_transforms[MAX_BONES];//ボーン行列
			DirectX::XMFLOAT4 cameraPos;    //カメラ位置
			DirectX::XMFLOAT4 lightColor;    //光の色
			DirectX::XMFLOAT4 nyutoralLightColor;    //環境光
		};
	protected:
		ID3D11VertexShader* p_VertexShader;
		ID3D11PixelShader* p_PixelShader;
		ID3D11InputLayout* p_InputLayout;
		//ID3D11Buffer* p_BufferVs;//（頂点バッファ
		//ID3D11Buffer* p_BufferIndex;//（インデックバッファ
		ID3D11Buffer* p_BufferConst;//（定数バッファ
		ID3D11RasterizerState* p_RasterizerStateLine;//（線描画
		ID3D11RasterizerState* p_RasterizerStatePaint;//（塗りつぶし描画
		ID3D11DepthStencilState* p_DepthStencilState;
		//追加
		ID3D11SamplerState* p_SampleState;// サンプラーステート
		ResourceManager* resourceManager;//リソースマネージャー

										 //int numIndices;//インデックスの数
		bool flagBoneAnimationEnd;//ボーンアニメーションの終わり

								  // UNIT.21
								  // convert coordinate system from 'UP:+Z FRONT:+Y RIGHT-HAND' to 'UP:+Y FRONT:+Z LEFT-HAND'
								  // y軸とｚ軸を入れ替える
		DirectX::XMFLOAT4X4 coordinate_conversion = {
			1,0,0,0,
			0,0,1,0,
			0,1,0,0,
			0,0,0,1,
		};

	public:
		Skinned_mesh(ID3D11Device* p_Deveice, const char* fbx_filename);
		virtual ~Skinned_mesh();

		//引数
		//p_DeviceContext	:	デバイスコンテキスト
		//DirectX::XMFLOAT4X4 wvp			:	ワールド・ビュー・プロジェクション合成行列
		//DirectX::XMFLOAT4X4 world			:ワールド変換行列
		//float elapsed_time	:秒の時間
		//DirectX::XMFLOAT4 materialColor	:材質色
		//DirectX::XMFLOAT4 lightVector		:ライト進行方向
		//DirectX::XMFLOAT4 cameraPos,			//カメラ位置
		//DirectX::XMFLOAT4 lightColor,			//光の色
		//DirectX::XMFLOAT4 nyutoralLightColor,	//環境光
		//bool  FlagPaint		:"線or塗りつぶし"描画フラグ
		void render(ID3D11DeviceContext *p_DeviceContext,   //デバイスコンテキスト
			const DirectX::XMFLOAT4X4 &wvp,				//ワールド・ビュー・プロジェクション合成行列
			const DirectX::XMFLOAT4X4 &world,				//ワールド変換行列
			float elapsed_time,/*UNIT.23*/
			const DirectX::XMFLOAT4 &materialColor = { 1,1,1,1 },			//材質色
			const DirectX::XMFLOAT4 &lightVector = { 0,0,4,0 },			//ライト進行方向
			const DirectX::XMFLOAT4 &cameraPos = { 0, 0, 20,1 },			//カメラ位置
			const DirectX::XMFLOAT4 &lightColor = { 0.995f,0.995f,0.999f,0.99f },			//光の色
			const DirectX::XMFLOAT4 &nyutoralLightColor = { 0.20f,0.21f,0.20f,1 },	//環境光
			bool  FlagPaint = true								//線・塗りつぶし描画フラグ
		);

		// アニメーションの終わり
		bool GetFlagBoneAnimationEnd()
		{
			return flagBoneAnimationEnd;
		}
	protected:
		//引数
		//p_Device	:	デバイス
		//VERTEX3D* vertices:頂点
		//const int NUM_VRETEX:超点数
		//UINT* indices:頂点番号
		//const int NUM_INDEX:頂点番号数
		void create_buffer(ID3D11Device* p_Device, MESH* mesh, VERTEX* vertices, const int NUM_VRETEX, UINT* indices, const int NUM_INDEX);

		//meshデータ取得
		std::vector<fbxsdk::FbxNode*> GetFbxMesh(ID3D11Device * p_Device, const char * fbx_filename, fbxsdk::FbxManager* Manager);
		//fbxファイルのロード
		void loadFbxFile(ID3D11Device * p_Device, const char * fbx_filename);
		// Fetch material properties.
		void FetchMaterial(ID3D11Device* p_Device, const fbxsdk::FbxMesh* Fbx_mesh, MESH* mesh, const char * fbx_filename);
		// Count the polygon count of each material
		void SetIndexCount(const fbxsdk::FbxMesh* Fbx_mesh, MESH* mesh);
		// Fetch mesh data
		void FetchMeshData(ID3D11Device* p_Device, const fbxsdk::FbxMesh* Fbx_mesh, MESH* mesh, std::vector<VERTEX>* vertices, std::vector<UINT>* indices);
	};
}



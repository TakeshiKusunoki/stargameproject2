//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
#include "sprite2D.h"
#include "WICTextureLoader.h"
#include "Wrapped.h"
#include <iostream>
#define ToRadian(x) DirectX::XMConvertToRadians(x)
#define SCREEN_W (0)
#define SCREEN_H (0)
bool ReadBinaryFile2(const char* filename, BYTE** data, unsigned int& size)
{
	FILE* fp = 0;
	if (fopen_s(&fp, filename, "rb"))
	{
		return false;
	}

	fseek(fp, 0, SEEK_END);

	size = ftell(fp);

	fseek(fp, 0, SEEK_SET);
	*data = new unsigned char[size];
	fread(*data, size, 1, fp);

	fclose(fp);

	return true;
}
namespace Lib_3D {



	//図形
	Sprite2D::Sprite2D(ID3D11Device* p_Device)
	{
		HRESULT hr = S_OK;
		VERTEX2D vertices[] =
		{
			{ DirectX::XMFLOAT3(-0.5,+0.5,0),DirectX::XMFLOAT4(1,1,1,1) },
			{ DirectX::XMFLOAT3(-0.5,+0.5,0),DirectX::XMFLOAT4(1,0,0,1) },
			{ DirectX::XMFLOAT3(-0.5,-0.5,0),DirectX::XMFLOAT4(0,1,0,1) },
			{ DirectX::XMFLOAT3(+0.5,-0.5,0),DirectX::XMFLOAT4(0,0,1,1) }
		};


		// 頂点バッファ作成(頂点データをDirect3Dのパイプラインに流し込む為のバッファーを作成します)
		D3D11_BUFFER_DESC bufferdesk;
		ZeroMemory(&bufferdesk, sizeof(bufferdesk));
		bufferdesk.ByteWidth = sizeof(vertices);
		//bufferdesk.Usage = D3D11_USAGE_DEFAULT;
		bufferdesk.Usage = D3D11_USAGE_DYNAMIC;
		bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		//bufferdesk.CPUAccessFlags = 0;
		bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		//bufferdesk.MiscFlags = D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS;
		bufferdesk.MiscFlags = 0;
		bufferdesk.StructureByteStride = sizeof(DirectX::XMFLOAT3);//float?

																   // サブリソースの初期化に使用されるデータを指定します。
		D3D11_SUBRESOURCE_DATA subResourceData;
		ZeroMemory(&subResourceData, sizeof(subResourceData));
		subResourceData.pSysMem = vertices;//初期化データへのポインターです。
		subResourceData.SysMemPitch = 0;//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
		subResourceData.SysMemSlicePitch = 0;//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
											 // バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
		hr = p_Device->CreateBuffer(&bufferdesk, &subResourceData, &p_Buffer);
		if (FAILED(hr))return;

		// コンパイル済み頂点シェーダーオブジェクトの読み込み
		BYTE* conpileData = nullptr;//コンパイル済みシェーダーへのポインターです。//頂点シェーダバイナリデータ
		unsigned int conpileData_size = 0;//コンパイル済み頂点シェーダーのサイズです。
		if (!ReadBinaryFile2("sprite_vs.cso", &conpileData, conpileData_size))return;

		// 頂点シェーダーオブジェクトの作成
		hr = p_Device->CreateVertexShader(conpileData, conpileData_size, nullptr, &p_VertexShader);//コンパイル済みシェーダーから、頂点シェーダー オブジェクトを作成します。
		if (FAILED(hr))
		{
			delete[] conpileData;
			return;
		}

		// 入力オブジェクトの生成
		// 入力レイアウト定義(インプットレイアウト)
		/* UINT stride = sizeof(vertex3D);
		UINT offset = 0;*/

		// 頂点データの構造を記述
		D3D11_INPUT_ELEMENT_DESC inputElementDesk[] = {
			//{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
		};
		UINT numElements = ARRAYSIZE(inputElementDesk);
		// インプットレイアウト
		//入力アセンブラー ステージで使用される入力バッファー データを記述するための入力レイアウト オブジェクトを作成します。
		hr = p_Device->CreateInputLayout(inputElementDesk, numElements, conpileData, conpileData_size, &p_InputLayout);
		delete[] conpileData;
		if (FAILED(hr))return;

		// ピクセルシェーダーオブジェクトの生成
		if (!ReadBinaryFile2("sprite_ps.cso", &conpileData, conpileData_size))return;
		hr = p_Device->CreatePixelShader(conpileData, conpileData_size, nullptr, &p_PixelShader);
		delete[] conpileData;
		if (FAILED(hr))return;

		// ラスタライザーステート
		D3D11_RASTERIZER_DESC rasteriserDesk;
		ZeroMemory(&rasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
		rasteriserDesk.FillMode = D3D11_FILL_SOLID;//レンダリング時に使用する描画モードを決定します
		rasteriserDesk.CullMode = D3D11_CULL_NONE;//特定の方向を向いている三角形の描画の有無を示します。
		rasteriserDesk.FrontCounterClockwise = true;//三角形が前向きか後ろ向きかを決定します。
													//rasteriserDesk.DepthBias =
		hr = p_Device->CreateRasterizerState(&rasteriserDesk, &p_RasterizerState);
		if (FAILED(hr))return;
	}


	// 画像
	Sprite2D::Sprite2D(ID3D11Device* p_Device, const wchar_t* texture_filename)
	{
		HRESULT hr = S_OK;
		VERTEXSPR2D vertices[] =
		{
			{ DirectX::XMFLOAT3(-0.5,+0.5,0),DirectX::XMFLOAT4(1,1,1,1),DirectX::XMFLOAT2(0,0) },
			{ DirectX::XMFLOAT3(-0.5,+0.5,0),DirectX::XMFLOAT4(1,0,0,1),DirectX::XMFLOAT2(0,1) },
			{ DirectX::XMFLOAT3(-0.5,-0.5,0),DirectX::XMFLOAT4(0,1,0,1),DirectX::XMFLOAT2(1,0) },
			{ DirectX::XMFLOAT3(+0.5,-0.5,0),DirectX::XMFLOAT4(0,0,1,1),DirectX::XMFLOAT2(1,1) }
		};


		// 頂点データ(三角ポリゴン1枚)
		//   vertex2D vertices[] = {
		//{ (-0.5f,+0,5,0),(1,1,1,1) },//float
		//{ (-0.5f,+0,5,0),(1,0,0,1) },
		//{ (-0.5f,-0,5,0),(0,1,0,1) },
		//{ (+0.5f,-0,5,0),(0,0,1,1) }
		//   };

		// 頂点バッファ作成(頂点データをDirect3Dのパイプラインに流し込む為のバッファーを作成します)
		D3D11_BUFFER_DESC bufferdesk;
		ZeroMemory(&bufferdesk, sizeof(bufferdesk));
		bufferdesk.ByteWidth = sizeof(vertices);
		//bufferdesk.Usage = D3D11_USAGE_DEFAULT;
		bufferdesk.Usage = D3D11_USAGE_DYNAMIC;
		bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		//bufferdesk.CPUAccessFlags = 0;
		bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		//bufferdesk.MiscFlags = D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS;
		bufferdesk.MiscFlags = 0;
		bufferdesk.StructureByteStride = sizeof(DirectX::XMFLOAT3);//float?
																   // サブリソースの初期化に使用されるデータを指定します。
		D3D11_SUBRESOURCE_DATA subResourceData;
		ZeroMemory(&subResourceData, sizeof(subResourceData));
		subResourceData.pSysMem = vertices;//初期化データへのポインターです。
		subResourceData.SysMemPitch = 0;//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
		subResourceData.SysMemSlicePitch = 0;//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
											 // バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
		hr = p_Device->CreateBuffer(&bufferdesk, &subResourceData, &p_Buffer);
		if (FAILED(hr))
		{
			assert(!"!");
			return;
		}
		//////////////////////////////////////
		//Custmized by UNIT5
		//////////////////////////////////////
		//ResourceManager* p_ResourManage;
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// コンパイル済み頂点シェーダーオブジェクトの読み込み
		//   BYTE* conpileData = nullptr;//コンパイル済みシェーダーへのポインターです。//頂点シェーダバイナリデータ
		//   unsigned int conpileData_size = 0;//コンパイル済み頂点シェーダーのサイズです。
		//   if (!ReadBinaryFile("sprite_vs.cso", &conpileData, conpileData_size))return;
		//   // 頂点シェーダーオブジェクトの作成
		//   hr = p_Device->CreateVertexShader(conpileData, conpileData_size, nullptr, &p_VertexShader);//コンパイル済みシェーダーから、頂点シェーダー オブジェクトを作成します。


		//   if (FAILED(hr))
		//   {
		//delete[] conpileData;
		//return;
		//   }

		// 入力オブジェクトの生成
		// 入力レイアウト定義(インプットレイアウト)
		/* UINT stride = sizeof(vertex3D);
		UINT offset = 0;*/

		// 頂点データの構造を記述
		D3D11_INPUT_ELEMENT_DESC inputElementDesk[] =
		{
			//{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			//Added by UNIT4
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
		};



		UINT numElements = ARRAYSIZE(inputElementDesk);
		// インプットレイアウト
		//入力アセンブラー ステージで使用される入力バッファー データを記述するための入力レイアウト オブジェクトを作成します。
		/* hr = p_Device->CreateInputLayout(inputElementDesk, numElements, conpileData, conpileData_size, &p_InputLayout);
		delete[] conpileData;
		if (FAILED(hr)){return;}*/
		/*static int n = 0;
		if (n == 0)*/
		resourceManager = new ResourceManager;
		bool f = true;
		f = resourceManager->LoadVertexShader(p_Device, "Shader\\sprite_vs.cso", inputElementDesk, numElements, &p_VertexShader, &p_InputLayout);
		if (!f)
		{
			assert(!"!");
			return;
		}
		//////////////////////////////////////
		//Custmized by UNIT5
		//////////////////////////////////////
		// ピクセルシェーダーオブジェクトの生成
		/* if (!ReadBinaryFile("sprite_ps.cso", &conpileData, conpileData_size)) {return;}*/
		/*hr = p_Device->CreatePixelShader(conpileData, conpileData_size, nullptr, &p_PixelShader);
		delete[] conpileData;
		if (FAILED(hr)){return;}*/
		f = resourceManager->LoadPixelShader(p_Device, "Shader\\sprite_ps.cso", &p_PixelShader);
		if (!f)
		{
			assert(!"!");
			return;
		}
		// ラスタライザーステート
		D3D11_RASTERIZER_DESC rasteriserDesk;
		ZeroMemory(&rasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
		rasteriserDesk.FillMode = D3D11_FILL_SOLID;//レンダリング時に使用する描画モードを決定します
		rasteriserDesk.CullMode = D3D11_CULL_NONE;//特定の方向を向いている三角形の描画の有無を示します。
		rasteriserDesk.DepthClipEnable = false;//。UNIT5
		rasteriserDesk.FrontCounterClockwise = true;//三角形が前向きか後ろ向きかを決定します。
													//rasteriserDesk.DepthBias =
		hr = p_Device->CreateRasterizerState(&rasteriserDesk, &p_RasterizerState);
		if (FAILED(hr))
		{
			assert(!"!");
			return;
		}

		////////////////////////////////////////////////////
		//Added by U4  画像について
		//Custmized by UNIT5
		////////////////////////////////////////////////////

		// 画像ファイルのロード　とシェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
		//   ID3D11Resource* p_Resource;
		//   hr = DirectX::CreateWICTextureFromFile(p_Device, texture_filename, &p_Resource, &p_ShaderResourceView);
		//   if (FAILED(hr))
		//   {
		//return;
		//   }
		//   // テクスチャ情報の取得
		//   //D3D11_TEXTURE2D_DESC TEXTURE2D_DESC;
		//   ID3D11Texture2D* p_texture2d;
		//   hr = p_Resource->QueryInterface(&p_texture2d);
		//   if (FAILED(hr))
		//   {
		//p_Resource->Release();
		//return;
		//   }
		//   p_texture2d->GetDesc(&TEXTURE2D_DESC);//�@画像のプロパティ情報の保存
		//   p_texture2d->Release();
		//   p_Resource->Release();
		// テクスチャ画像読み込み
		f = resourceManager->LoadShaderResourceView(p_Device, texture_filename, &p_ShaderResourceView, &TEXTURE2D_DESC);
		if (!f)
		{
			assert(!"!");
			return;
		}
		//TEXTURE2D_DESC.Width �A画像幅

		// サンプラーステートオブジェクト(ID3D11SampleState)の生成
		D3D11_SAMPLER_DESC samplerDesk;
		ZeroMemory(&samplerDesk, sizeof(samplerDesk));
		samplerDesk.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		samplerDesk.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesk.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesk.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesk.MipLODBias = 0;//みっぷマップレベルからのバイアス
		samplerDesk.MaxAnisotropy = 16;//違法性保管を使用している場合の限界値
		samplerDesk.ComparisonFunc = D3D11_COMPARISON_ALWAYS;//比較オプション
		samplerDesk.BorderColor[0] = 1;
		samplerDesk.BorderColor[1] = 1;
		samplerDesk.BorderColor[2] = 1;
		samplerDesk.BorderColor[3] = 1;
		samplerDesk.MinLOD = 0;
		samplerDesk.MaxLOD = D3D11_FLOAT32_MAX;//アクセス可能なみっぷマップの上限値

		hr = p_Device->CreateSamplerState(&samplerDesk, &p_SamplerState);//ピクセルシェーダーに送る
		if (FAILED(hr))
		{
			assert(!"!");
			return;
		}

		////////////////////////////////////////
		//Added by Unit6
		///////////////////////////////////////
		//深度ステンシルステート
		D3D11_DEPTH_STENCIL_DESC DepthDesc;
		ZeroMemory(&DepthDesc, sizeof(DepthDesc));
		DepthDesc.DepthEnable = false;						//深度テストを使用可能にします。
		DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//深度ステンシル バッファーの中で、深度データによる変更が可能な部分を識別します
		DepthDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;				//深度データを既存の深度データと比較する関数です。
		DepthDesc.StencilEnable = false;						//ステンシル テストを使用可能にします。
																//DepthDesc.StencilReadMask = D3D10_DEFAULT_STENCIL_READ_MASK;//? ?		 //深度ステンシル バッファーの中で、ステンシル データを読み取る部分を識別します。
																//DepthDesc.StencilWriteMask = D3D10_DEFAULT_STENCIL_WRITE_MASK;//? ?		 //
																//DepthDesc.FrontFace = StencilDepthFailOp;							//法線がカメラの方向を向いているサーフェスを持つピクセルの深度テストとステンシル テストの結果を使用する方法を識別します
																//DepthDesc.BackFace = StencilFailOp;								//法線がカメラと逆方向を向いているサーフェスを持つピクセルの深度テストとステンシル テストの結果を使用する方法を識別します
		hr = p_Device->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
		if (FAILED(hr))
		{
			assert(!"!");
			return;
		}
		////////////////////////////////////////
		//Deleted by Unit7
		///////////////////////////////////////
		//   //ブレンディング ステートを記述します。
		//   D3D11_BLEND_DESC BlendDesk;
		//   //D3D11_RENDER_TARGET_BLEND_DESC *1;
		//   ZeroMemory(&BlendDesk, sizeof(BlendDesk));
		//   BlendDesk.AlphaToCoverageEnable = false;//ピクセルをレンダー ターゲットに設定するときに、アルファトゥカバレッジをマルチサンプリング テクニックとして使用するかどうかを決定します。
		//   BlendDesk.IndependentBlendEnable = false;//同時処理のレンダー ターゲットで独立したブレンディングを有効にするには、TRUE に設定します。FALSE に設定すると、RenderTarget[0] のメンバーのみが使用されます。RenderTarget[1..7] は無視されます。
		//   //レンダリング ターゲットのブレンディング ステートを記述します。
		//   //D3D11_RENDER_TARGET_BLEND_DESC RenderTargetDesk[8];
		//   //ZeroMemory(&RenderTargetDesk, sizeof(RenderTargetDesk));
		//   BlendDesk.RenderTarget[0].BlendEnable = true;
		//   BlendDesk.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		//   BlendDesk.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		//   BlendDesk.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;//RGB またはアルファのブレンディング処理です。
		//   BlendDesk.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		//   BlendDesk.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		//   BlendDesk.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		//   BlendDesk.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		//   //BlendDesk.RenderTarget[8] = RenderTargetDesk;//レンダリング ターゲットのブレンディング ステートを記述します。
		//   //RenderTargetDesk.RenderTargetWriteMask;
		//   hr = p_Device->CreateBlendState(&BlendDesk, &p_BlendState);	//
		//   if (FAILED(hr)) {
		//return;
		//   }
	}





#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	Sprite2D::~Sprite2D()
	{
		////////////////////////////////////////
		//Added by Unit6
		///////////////////////////////////////
		RELEASE_IF(p_DepthStencilState);
		////////////////////////////////////////
		//Deleted by Unit7
		///////////////////////////////////////
		//RELEASE_IF(p_BlendState);
		////////////////////////////////////////
		//Custmized by UNIT5
		//Added by UNIT4
		/////////////////////////////////////////
		RELEASE_IF(p_SamplerState);
		resourceManager->ReleaseShaderResourceView(p_ShaderResourceView);
		RELEASE_IF(p_RasterizerState);
		RELEASE_IF(p_Buffer);
		resourceManager->ReleasePixelShader(p_PixelShader);
		resourceManager->ReleaseVertexShader(p_VertexShader, p_InputLayout);
		//RELEASE_IF(resourceManager);
		//RELEASE_IF(p_ShaderResourceView);
		//RELEASE_IF(p_ShaderResourceView);
		//RELEASE_IF(p_InputLayout);
		//RELEASE_IF(p_PixelShader);
		//RELEASE_IF(p_VertexShader);
	}
#undef RELEASE_IF
#undef DELETE_IF
	void Sprite2D::Render(ID3D11DeviceContext* p_DeviceContext)
	{
		//Set Vertex buffer
		UINT stride = sizeof(VERTEX2D);
		UINT offset = 0;
		p_DeviceContext->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);//バインドに使用する最初の入力スロットです。配列内の頂点バッファーの数です。
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);//入力アセンブラー ステージの入力データを記述するためのプリミティブ タイプおよびデータの順序に関する情報をバインドします。
		p_DeviceContext->IASetInputLayout(p_InputLayout);//入力アセンブラー ステージに入力レイアウト オブジェクトをバインドします。
		p_DeviceContext->RSSetState(p_RasterizerState);//パイプラインのラスタライザー ステージのラスタライザー ステートを設定します。
		p_DeviceContext->VSSetShader(p_VertexShader, NULL, 0);
		p_DeviceContext->PSSetShader(p_PixelShader, NULL, 0);
		p_DeviceContext->Draw(4, 0);//(描画する頂点の数です。,最初の頂点のインデックスです。)
	}

	//#define RELEASE_IF(x) if(x){x->Release();}
	//sprite3D::~sprite3D() {
	//    RELEASE_IF(p_RasterizerState);
	//    RELEASE_IF(p_Buffer);
	//    RELEASE_IF(p_InputLayout);
	//    RELEASE_IF(p_PixelShader);
	//    RELEASE_IF(p_VertexShader);
	//}
	//#undef RELEASE_IF
	//
	//void sprite3D::render(ID3D11DeviceContext* p_DeviceContext) {
	//    //Set Vertex buffer
	//    UINT stride = sizeof(vertex3D);
	//    UINT offset = 0;
	//    p_DeviceContext->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);//バインドに使用する最初の入力スロットです。配列内の頂点バッファーの数です。
	//    p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);//入力アセンブラー ステージの入力データを記述するためのプリミティブ タイプおよびデータの順序に関する情報をバインドします。
	//    p_DeviceContext->IASetInputLayout(p_InputLayout);//入力アセンブラー ステージに入力レイアウト オブジェクトをバインドします。
	//    p_DeviceContext->RSSetState(p_RasterizerState);//パイプラインのラスタライザー ステージのラスタライザー ステートを設定します。
	//    p_DeviceContext->VSSetShader(p_VertexShader, NULL, 0);
	//    p_DeviceContext->PSSetShader(p_PixelShader, NULL, 0);
	//    p_DeviceContext->Draw(4, 0);//(描画する頂点の数です。,最初の頂点のインデックスです。)
	//}

	void Sprite2D::Render(ID3D11DeviceContext* p_DeviceContext,
		float x, float y,//dx,dy;Coordinate of sprite's left-top corner in screen space
		float sx, float sy,
		float w, float h,//dw,dh;Size of sprite in screen space
		float cx, float cy,
		float angle,//angle:Raotation angle(Rotation centre is sprite's each vertices)
		float r, float g, float b, float a)
	{
		VERTEX2D vertices[4];//頂点
		float vertices_copy[4][2];//原点を起点としたときの頂点格納用
								  //初期化
		for (int i = 0; i < 4; i++)
		{
			vertices[i].position = DirectX::XMFLOAT3(0, 0, .0f);
			vertices[i].color = DirectX::XMFLOAT4(r, g, b, a);
			vertices_copy[i][0] = 0;
			vertices_copy[i][1] = 0;
		}
		// 中心点を基にした拡大・縮小
		w *= sx;//高さ
		h *= sy;//幅
				// 相対位置(directXの座標系で)
				//vertices[0].position.x;//

		vertices_copy[0][0] -= w / 2; //左上頂点//ローカル座標→単位円 符号
		vertices_copy[0][1] -= h / 2; //左上頂点//DEFSIN(-)//DEFCOS(+) ToRadian(270)
		vertices_copy[1][0] -= w / 2; //左下頂点//DEFSIN(+)//DEFCOS(+) ToRadian(0)
		vertices_copy[1][1] += h / 2; //左下頂点
		vertices_copy[2][0] += w / 2; //右上頂点//DEFSIN(-)//DEFCOS(-)
		vertices_copy[2][1] -= h / 2; //右上頂点
		vertices_copy[3][0] += w / 2; //右下頂点//DEFSIN(+)//DEFCOS(-)
		vertices_copy[3][1] += h / 2; //右下頂点
									  //   // 画像回転
									  //float sin = sinf(radian), cos = cosf(radian);
		float RADIAN = ToRadian(angle);//追加角度
		for (int i = 0; i < 4; i++)
		{
			float LENGTH = CircleHalf_Length2D(vertices_copy[i]);//中心から頂点までの長さ
			float DEFCOS = vertices_copy[i][0] / LENGTH;//頂点生成時の単位ベクトルデフォルトコサイン
			float DEFSIN = vertices_copy[i][1] / LENGTH;//頂点生成時の単位ベクトルデフォルトサイン
														//頂点生成時の中心からの角度をその頂点の単位ベクトルcos(θ)のθを求める
			float shita = ToRadian(0);//頂点の中心からの角度
									  //単位円における、その頂点の単位ベクトルの位置
			if (DEFCOS >= 0)
			{
				if (DEFSIN >= 0)
				{
					shita = ToRadian(0);
				}
				else
				{
					shita = ToRadian(270);
				}
				float SHITA_MINIT = shita + ToRadian(90);//シータがあるであろう範囲
														 // シータの近似値を求める
#define DEGREE_MAX (5)//6以上だとエラーになった5はセーフ
														 //シータの角度１０刻み
				for (int i2 = 0; i2 <= DEGREE_MAX; i2++)
				{//近似する回数
					float angleDegree = 10.0f / (float)pow(10, i2);//回転頻度//----ここまでは大丈夫っぽい
					for (; shita <= SHITA_MINIT; shita += ToRadian(angleDegree))
					{
						float m = sinf(shita + ToRadian(angleDegree));
						float m2 = sinf(shita - ToRadian(angleDegree));
						//シータの角度0.05刻み
						if (m >= DEFSIN && DEFSIN >= m2)
						{//シータの近似
							if (i2 == DEGREE_MAX)
							{
								vertices[i].position.x = cosf(shita + RADIAN)*LENGTH;
								vertices[i].position.y = sinf(shita + RADIAN)*LENGTH;
							}
							break;
						}
					}

				}
			}
			else if (DEFCOS < 0)
			{
				if (DEFSIN >= 0)
				{
					shita = ToRadian(90);
				}
				else
				{
					shita = ToRadian(180);
				}
				float SHITA_MINIT = shita + ToRadian(90);//シータがあるであろう範囲
														 //シータの近似値を求める
				for (int i2 = 0; i2 <= DEGREE_MAX; i2++)
				{//近似する回数
					float angleDegree = 10.0f / (float)pow(10, i2);//回転頻度
					for (; shita <= SHITA_MINIT; shita += ToRadian(angleDegree))
					{
						float n = sinf(shita - ToRadian(angleDegree));
						float n2 = sinf(shita + ToRadian(angleDegree));
						//シータの角度0.05刻み
						if (n >= DEFSIN && DEFSIN >= n2)
						{//シータの近似
							if (i2 == DEGREE_MAX)
							{
								vertices[i].position.x = cosf(shita + RADIAN)*LENGTH;
								vertices[i].position.y = sinf(shita + RADIAN)*LENGTH;
							}
							break;
						}
					}

				}
			}
#undef DEGREE_MAX
			/*vertices[i].position.x = (vertices[0].position.x - cx)*cosf(radian) + w;
			vertices[i].position.y = vertices[0].position.y - cy*sinf(radian) + h;*/
		}
		////   // 平行移動
		for (int i = 0; i < 4; i++)
		{
			vertices[i].position.x += x;
			vertices[i].position.y += y;
		}
		// 頂点正規化
		for (int i = 0; i < 4; i++)
		{
			vertices[i].position.x = (vertices[i].position.x / Lib_3D::pDirectXWindowManager->GetScreenWidth(nullptr))*2.0f - 1.0f;
			vertices[i].position.y = (-vertices[i].position.y / Lib_3D::pDirectXWindowManager->GetScreenHeight(nullptr))*2.0f + 1.0f;
		}
		//マップ
		D3D11_MAPPED_SUBRESOURCE mapSubResorce;//マップ
		p_DeviceContext->Map(p_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapSubResorce);//サブリソースに格納されているデータへのポインターを取得して、そのサブリソースへの GPU のアクセスを拒否します。
		memcpy(mapSubResorce.pData, vertices, sizeof(vertices));//頂点のコピー
		p_DeviceContext->Unmap(p_Buffer, 0);
		// 必要なオブジェクトが準備出来たら、それらをデバイスコンテキストへセットして描画します。Set vertex buffer
		UINT stride = sizeof(VERTEX2D);
		UINT offset = 0;
		p_DeviceContext->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);//バインドに使用する最初の入力スロットです。配列内の頂点バッファーの数です。
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);//入力アセンブラー ステージの入力データを記述するためのプリミティブ タイプおよびデータの順序に関する情報をバインドします。
		p_DeviceContext->IASetInputLayout(p_InputLayout);//入力アセンブラー ステージに入力レイアウト オブジェクトをバインドします。
		p_DeviceContext->RSSetState(p_RasterizerState);//パイプラインのラスタライザー ステージのラスタライザー ステートを設定します。
		p_DeviceContext->VSSetShader(p_VertexShader, NULL, 0);
		p_DeviceContext->PSSetShader(p_PixelShader, NULL, 0);
		p_DeviceContext->Draw(4, 0);//(描画する頂点の数です。,最初の頂点のインデックスです。)
	}

	/////////////////////////////////
	//UNIT4
	/////////////////////////////////
	// すべての頂点位置を指定(四角形)
	void Sprite2D::Render(ID3D11DeviceContext* p_DeviceContext,
		DirectX::XMFLOAT3* pos,
		float x, float y,
		float sx, float sy,
		float cx, float cy,
		float angle,
		float r, float g, float b, float a)
	{




		VERTEX2D vertices[4];//頂点
		float vertices_copy[4][2];//原点を起点としたときの頂点格納用
								  //初期化
		for (int i = 0; i < 4; i++)
		{
			vertices[i].position = DirectX::XMFLOAT3(0, 0, .0f);
			vertices[i].color = DirectX::XMFLOAT4(r, g, b, a);
			vertices_copy[i][0] = 0;
			vertices_copy[i][1] = 0;
		}
		// 中心点を基にした拡大・縮小\
				// 相対位置(directXの座標系で)
//vertices[0].position.x;//
		for (int i = 0; i < 4; i++)
		{
			vertices_copy[i][0] += (pos[i].x)*sx + cx;
			vertices_copy[i][1] += (pos[i].y)*sy + cy;
		}
		//   // 画像回転
		//float sin = sinf(radian), cos = cosf(radian);
		float RADIAN = ToRadian(angle);//追加角度
		for (int i = 0; i < 4; i++)
		{
			float LENGTH = CircleHalf_Length2D(vertices_copy[i]);//中心から頂点までの長さ
			float DEFCOS = vertices_copy[i][0] / LENGTH;//頂点生成時の単位ベクトルデフォルトコサイン
			float DEFSIN = vertices_copy[i][1] / LENGTH;//頂点生成時の単位ベクトルデフォルトサイン
														//頂点生成時の中心からの角度をその頂点の単位ベクトルcos(θ)のθを求める
			float shita = ToRadian(0);//頂点の中心からの角度
									  //単位円における、その頂点の単位ベクトルの位置
			if (DEFCOS >= 0)
			{
				if (DEFSIN >= 0)
				{
					shita = ToRadian(0);
				}
				else
				{
					shita = ToRadian(270);
				}
				float SHITA_MINIT = shita + ToRadian(90);//シータがあるであろう範囲
														 // シータの近似値を求める
#define DEGREE_MAX (5)//6以上だとエラーになった5はセーフ
														 //シータの角度１０刻み
				for (int i2 = 0; i2 <= DEGREE_MAX; i2++)
				{//近似する回数
					float angleDegree = 10.0f / (float)pow(10, i2);//回転頻度//----ここまでは大丈夫っぽい
					for (; shita <= SHITA_MINIT; shita += ToRadian(angleDegree))
					{
						float m = sinf(shita + ToRadian(angleDegree));
						float m2 = sinf(shita - ToRadian(angleDegree));
						//シータの角度0.05刻み
						if (m >= DEFSIN && DEFSIN >= m2)
						{//シータの近似
							if (i2 == DEGREE_MAX)
							{
								vertices[i].position.x = cosf(shita + RADIAN)*LENGTH;
								vertices[i].position.y = sinf(shita + RADIAN)*LENGTH;
							}
							break;
						}
					}

				}
			}
			else if (DEFCOS < 0)
			{
				if (DEFSIN >= 0)
				{
					shita = ToRadian(90);
				}
				else
				{
					shita = ToRadian(180);
				}
				float SHITA_MINIT = shita + ToRadian(90);//シータがあるであろう範囲
														 //シータの近似値を求める
				for (int i2 = 0; i2 <= DEGREE_MAX; i2++)
				{//近似する回数
					float angleDegree = 10.0f / (float)pow(10, i2);//回転頻度
					for (; shita <= SHITA_MINIT; shita += ToRadian(angleDegree))
					{
						float n = sinf(shita - ToRadian(angleDegree));
						float n2 = sinf(shita + ToRadian(angleDegree));
						//シータの角度0.05刻み
						if (n >= DEFSIN && DEFSIN >= n2)
						{//シータの近似
							if (i2 == DEGREE_MAX)
							{
								vertices[i].position.x = cosf(shita + RADIAN)*LENGTH;
								vertices[i].position.y = sinf(shita + RADIAN)*LENGTH;
							}
							break;
						}
					}

				}
			}
#undef DEGREE_MAX
			/*vertices[i].position.x = (vertices[0].position.x - cx)*cosf(radian) + w;
			vertices[i].position.y = vertices[0].position.y - cy*sinf(radian) + h;*/
		}
		////   // 平行移動
		for (int i = 0; i < 4; i++)
		{
			vertices[i].position.x += x;
			vertices[i].position.y += y;
		}
		// 頂点正規化
		for (int i = 0; i < 4; i++)
		{
			vertices[i].position.x = (vertices[i].position.x / Lib_3D::pDirectXWindowManager->GetScreenWidth(nullptr))*2.0f - 1.0f;
			vertices[i].position.y = (-vertices[i].position.y / Lib_3D::pDirectXWindowManager->GetScreenHeight(nullptr))*2.0f + 1.0f;
		}
		//マップ
		D3D11_MAPPED_SUBRESOURCE mapSubResorce;//マップ
		p_DeviceContext->Map(p_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapSubResorce);//サブリソースに格納されているデータへのポインターを取得して、そのサブリソースへの GPU のアクセスを拒否します。
		memcpy(mapSubResorce.pData, vertices, sizeof(vertices));//頂点のコピー
		p_DeviceContext->Unmap(p_Buffer, 0);
		// 必要なオブジェクトが準備出来たら、それらをデバイスコンテキストへセットして描画します。Set vertex buffer
		UINT stride = sizeof(VERTEX2D);
		UINT offset = 0;
		p_DeviceContext->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);//バインドに使用する最初の入力スロットです。配列内の頂点バッファーの数です。
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);//入力アセンブラー ステージの入力データを記述するためのプリミティブ タイプおよびデータの順序に関する情報をバインドします。
		p_DeviceContext->IASetInputLayout(p_InputLayout);//入力アセンブラー ステージに入力レイアウト オブジェクトをバインドします。
		p_DeviceContext->RSSetState(p_RasterizerState);//パイプラインのラスタライザー ステージのラスタライザー ステートを設定します。
		p_DeviceContext->VSSetShader(p_VertexShader, NULL, 0);
		p_DeviceContext->PSSetShader(p_PixelShader, NULL, 0);
		p_DeviceContext->Draw(4, 0);//(描画する頂点の数です。,最初の頂点のインデックスです。)





	}



	////////////////////////////////////////////
	// 画像描画
	////////////////////////////////////////////
	//  Added by Unit4
	////////////////////////////////////////////
	//void Sprite2D::Render2(ID3D11DeviceContext* p_DeviceContext,
	//	UINT windowNum,
	//	float x, float y,   //平行移動
	//	//float w, float h,
	//	float tx, float ty,//画像切り抜き
	//	float tw, float th,//画像切り抜き幅
	//	float sx, float sy,//拡大
	//	float cx, float cy,//中心
	//	float angle,//angle:Raotation angle(Rotation centre is sprite's each vertices)
	//	float r, float g, float b, float a)
	//{
	//
	//
	//
	//
	//	VERTEXSPR2D vertices[4];//頂点
	//	float vertices_copy[4][3];//原点を起点としたときの頂点格納用
	//	float vertices_copy2[4][2];//原点を起点としたときの頂点格納用
	//	//初期化
	//	for (int i = 0; i < 4; i++) {
	//	vertices[i].position = DirectX::XMFLOAT3(0, 0, .0f);
	//	vertices[i].color = DirectX::XMFLOAT4(r, g, b, a);
	//	vertices[i].texcoord = DirectX::XMFLOAT2(tx,ty);
	//	vertices_copy[i][0] = cx;
	//	vertices_copy[i][1] = cy;
	//	vertices_copy[i][2] = 0;
	//	vertices_copy2[i][0] = 0;
	//	vertices_copy2[i][1] = 0;
	//	}
	//	// 中心点を基にした拡大・縮小
	//	tw *= sx;//高さ
	//	th *= sy;//幅
	//	// 相対位置(directXの座標系で)
	//		//vertices[0].position.x;//
	//
	//	vertices_copy[0][0] -= tw / 2; //左上頂点//ローカル座標→単位円 符号
	//	vertices_copy[0][1] -= th / 2; //左上頂点//DEFSIN(-)//DEFCOS(+) ToRadian(270)
	//	vertices_copy[1][0] -= tw / 2; //左下頂点//DEFSIN(+)//DEFCOS(+) ToRadian(0)
	//	vertices_copy[1][1] += th / 2; //左下頂点
	//	vertices_copy[2][0] += tw / 2; //右上頂点//DEFSIN(-)//DEFCOS(-)
	//	vertices_copy[2][1] -= th / 2; //右上頂点
	//	vertices_copy[3][0] += tw / 2; //右下頂点//DEFSIN(+)//DEFCOS(-)
	//	vertices_copy[3][1] += th / 2; //右下頂点
	//
	//	//   // 画像回転
	//	//float sin = sinf(radian), cos = cosf(radian);
	//	float RADIAN = ToRadian(angle);//追加角度
	//	for (int i = 0; i < 4; i++) {
	//	float LENGTH = CircleHalf_Length2D(vertices_copy[i]);//中心から頂点までの長さ
	//	float DEFCOS = vertices_copy[i][0] / LENGTH;//頂点生成時の単位ベクトルデフォルトコサイン
	//	float DEFSIN = vertices_copy[i][1] / LENGTH;//頂点生成時の単位ベクトルデフォルトサイン
	//							//頂点生成時の中心からの角度をその頂点の単位ベクトルcos(θ)のθを求める
	//	float shita = ToRadian(0);//頂点の中心からの角度
	//				  //単位円における、その頂点の単位ベクトルの位置
	//	if (DEFCOS >= 0) {
	//		if (DEFSIN >= 0) {
	//		shita = ToRadian(0);
	//		} else {
	//		shita = ToRadian(270);
	//		}
	//		float SHITA_MINIT = shita + ToRadian(90);//シータがあるであろう範囲
	//							 // シータの近似値を求める
	//#define DEGREE_MAX (5)//6以上だとエラーになった5はセーフ
	//							 //シータの角度１０刻み
	//		for (int i2 = 0; i2 <= DEGREE_MAX; i2++) {//近似する回数
	//		float angleDegree = 10.0f / (float)pow(10, i2);//回転頻度//----ここまでは大丈夫っぽい
	//		for (; shita <= SHITA_MINIT; shita += ToRadian(angleDegree)) {
	//			float m = sinf(shita + ToRadian(angleDegree));
	//			float m2 = sinf(shita - ToRadian(angleDegree));
	//			//シータの角度0.05刻み
	//			if (m >= DEFSIN && DEFSIN >= m2) {//シータの近似
	//			if (i2 == DEGREE_MAX) {
	//				vertices[i].position.x = cosf(shita + RADIAN)*LENGTH;
	//				vertices[i].position.y = sinf(shita + RADIAN)*LENGTH;
	//			}
	//			break;
	//			}
	//		}
	//
	//		}
	//	} else if (DEFCOS < 0)
	//	{
	//		if (DEFSIN >= 0)
	//		{
	//		shita = ToRadian(90);
	//		} else {
	//		shita = ToRadian(180);
	//		}
	//		float SHITA_MINIT = shita + ToRadian(90);//シータがあるであろう範囲
	//							 //シータの近似値を求める
	//		for (int i2 = 0; i2 <= DEGREE_MAX; i2++) {//近似する回数
	//		float angleDegree = 10.0f / (float)pow(10, i2);//回転頻度
	//		for (; shita <= SHITA_MINIT; shita += ToRadian(angleDegree)) {
	//			float n = sinf(shita - ToRadian(angleDegree));
	//			float n2 = sinf(shita + ToRadian(angleDegree));
	//			//シータの角度0.05刻み
	//			if (n >= DEFSIN && DEFSIN >= n2) {//シータの近似
	//			if (i2 == DEGREE_MAX) {
	//				vertices[i].position.x = cosf(shita + RADIAN)*LENGTH;
	//				vertices[i].position.y = sinf(shita + RADIAN)*LENGTH;
	//			}
	//			break;
	//			}
	//		}
	//
	//		}
	//	}
	//#undef DEGREE_MAX
	//	/*vertices[i].position.x = (vertices[0].position.x - cx)*cosf(radian) + w;
	//	vertices[i].position.y = vertices[0].position.y - cy*sinf(radian) + h;*/
	//	}
	//
	//	////   // 平行移動
	//	for (int i = 0; i < 4; i++)
	//	{
	//	vertices[i].position.x += x;
	//	vertices[i].position.y += y;
	//	}
	//
	//
	//	// 画像切り抜き
	//	//vertices[0].texcoord.x = 0;
	//	//vertices[0].texcoord.y = 0;
	//	vertices[1].texcoord.y += th;
	//	vertices[2].texcoord.x += tw;
	//	vertices[3].texcoord.x += tw;
	//	vertices[3].texcoord.y += th;
	//
	//	/*
	//	vertices[0].texcoord.x = 0;
	//	vertices[0].texcoord.y = 0;
	//	vertices[1].texcoord.x = 0;
	//	vertices[1].texcoord.y = 1;
	//	vertices[2].texcoord.x = 1;
	//	vertices[2].texcoord.y = 0;
	//	vertices[3].texcoord.x = 1;
	//	vertices[3].texcoord.y = 1;
	//	*/
	//
	//	// 頂点正規化
	//	for (int i = 0; i < 4; i++)
	//	{
	//	vertices[i].position.x = (vertices[i].position.x / GetScreenWidth(windowNum))*2.0f - 1.0f;
	//	vertices[i].position.y = (-vertices[i].position.y / GetScreenHeight(windowNum))*2.0f + 1.0f;
	//
	//	//Added by Unit4
	//	//画像幅
	//	vertices[i].texcoord.x = (vertices[i].texcoord.x / TEXTURE2D_DESC.Width );
	//	vertices[i].texcoord.y = (vertices[i].texcoord.y / TEXTURE2D_DESC.Height );
	//	}
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//	//マップ
	//	D3D11_MAPPED_SUBRESOURCE mapSubResorce;//マップ
	//	p_DeviceContext->Map(p_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapSubResorce);//サブリソースに格納されているデータへのポインターを取得して、そのサブリソースへの GPU のアクセスを拒否します。
	//	memcpy(mapSubResorce.pData, vertices, sizeof(vertices));//頂点のコピー
	//	p_DeviceContext->Unmap(p_Buffer, 0);
	//	// 必要なオブジェクトが準備出来たら、それらをデバイスコンテキストへセットして描画します。Set vertex buffer
	//	UINT stride = sizeof(VERTEXSPR2D);
	//	UINT offset = 0;
	//	p_DeviceContext->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);//バインドに使用する最初の入力スロットです。配列内の頂点バッファーの数です。
	//	p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);//入力アセンブラー ステージの入力データを記述するためのプリミティブ タイプおよびデータの順序に関する情報をバインドします。
	//	p_DeviceContext->IASetInputLayout(p_InputLayout);//入力アセンブラー ステージに入力レイアウト オブジェクトをバインドします。
	//	p_DeviceContext->RSSetState(p_RasterizerState);//パイプラインのラスタライザー ステージのラスタライザー ステートを設定します。
	//	p_DeviceContext->VSSetShader(p_VertexShader, NULL, 0);
	//	p_DeviceContext->PSSetShader(p_PixelShader, NULL, 0);
	//	////////////////////////////////////////
	//	//Added by Unit6
	//	///////////////////////////////////////
	//	p_DeviceContext->OMSetDepthStencilState(p_DepthStencilState, 0);//出力結合ステージの深度ステンシル ステートを設定します。
	//	////////////////////////////////////////
	//	//Deleted by Unit7
	//	///////////////////////////////////////
	//	//float blendFactor[4] = { D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };
	//	//p_DeviceContext->OMSetBlendState(p_BlendState, blendFactor, 0xffffffff);//出力結合ステージの深度ステンシル ステートを設定します。
	//	/////////////////////////////////
	//	//Added by Unit
	//	/////////////////////////////////
	//	p_DeviceContext->PSSetShaderResources(0,1, &p_ShaderResourceView);
	//	p_DeviceContext->PSSetSamplers(0,1,&p_SamplerState);
	//
	//	p_DeviceContext->Draw(4, 0);//(描画する頂点の数です。,最初の頂点のインデックスです。)
	//
	//
	//
	//
	//
	//
	//
	//
	//}



	//--------------------------------
	//  スプライト描画
	//--------------------------------
	void Sprite2D::render(ID3D11DeviceContext* context,
		const VECTOR2& position, const VECTOR2& scale,
		const VECTOR2& texPos, const VECTOR2& texSize,
		const VECTOR2& center, float angle,
		const VECTOR4& color) const
	{
		if (scale.x * scale.y == 0.0f) return;

		D3D11_VIEWPORT viewport;
		UINT numViewports = 1;
		context->RSGetViewports(&numViewports, &viewport);

		float tw = texSize.x;
		float th = texSize.y;
		if (tw <= 0.0f || th <= 0.0f)
		{
			tw = (float)TEXTURE2D_DESC.Width;
			th = (float)TEXTURE2D_DESC.Height;
		}

		VERTEXSPR2D vertices[] = {
			{ VECTOR3(-0.0f, +1.0f, 0), color, VECTOR2(0, 1) },
			{ VECTOR3(+1.0f, +1.0f, 0), color, VECTOR2(1, 1) },
			{ VECTOR3(-0.0f, -0.0f, 0), color, VECTOR2(0, 0) },
			{ VECTOR3(+1.0f, -0.0f, 0), color, VECTOR2(1, 0) },
		};

		float sinValue = sinf(angle);
		float cosValue = cosf(angle);
		float mx = (tw * scale.x) / tw * center.x;
		float my = (th * scale.y) / th * center.y;
		for (int i = 0; i < 4; i++)
		{
			vertices[i].position.x *= (tw * scale.x);
			vertices[i].position.y *= (th * scale.y);

			vertices[i].position.x -= mx;
			vertices[i].position.y -= my;

			float rx = vertices[i].position.x;
			float ry = vertices[i].position.y;
			vertices[i].position.x = rx * cosValue - ry * sinValue;
			vertices[i].position.y = rx * sinValue + ry * cosValue;

			vertices[i].position.x += mx;
			vertices[i].position.y += my;

			vertices[i].position.x += (position.x - scale.x * center.x);
			vertices[i].position.y += (position.y - scale.y * center.y);

			vertices[i].position.x = vertices[i].position.x * 2 / viewport.Width - 1.0f;
			vertices[i].position.y = 1.0f - vertices[i].position.y * 2 / viewport.Height;

			vertices[i].texcoord.x = (texPos.x + vertices[i].texcoord.x * tw) / TEXTURE2D_DESC.Width;
			vertices[i].texcoord.y = (texPos.y + vertices[i].texcoord.y * th) / TEXTURE2D_DESC.Height;
		}

		D3D11_MAPPED_SUBRESOURCE msr;
		context->Map(p_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
		memcpy(msr.pData, vertices, sizeof(vertices));
		context->Unmap(p_Buffer, 0);

		UINT stride = sizeof(VERTEXSPR2D);
		UINT offset = 0;
		context->IASetVertexBuffers(0, 1, &p_Buffer, &stride, &offset);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
		context->IASetInputLayout(p_InputLayout);
		context->RSSetState(p_RasterizerState);
		context->VSSetShader(p_VertexShader, nullptr, 0);
		context->PSSetShader(p_PixelShader, nullptr, 0);

		context->PSSetShaderResources(0, 1, &p_ShaderResourceView);
		context->PSSetSamplers(0, 1, &p_SamplerState);

		context->OMSetDepthStencilState(p_DepthStencilState, 1);

		context->Draw(4, 0);
	}






	//--------------------------------
	//  スプライト描画
	//--------------------------------
	void Sprite2D::Render3(ID3D11DeviceContext * p_DeviceContext, UINT windowNum, float x, float y, float sx, float sy, float tx, float ty, float tw, float th, float cx, float cy, float angle, float r, float g, float b, float a) const
	{
	}

}

//bool sprite3D::angleDegree() {
//
//}


//HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut) {
//    HRESULT hr = S_OK;
//
//    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
//#if defined( DEBUG ) || defined( _DEBUG )
//    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
//    // Setting this flag improves the shader debugging experience, but still allows
//    // the shaders to be optimized and to run exactly the way they will run in
//    // the release configuration of this program.
//    dwShaderFlags |= D3DCOMPILE_DEBUG;
//#endif
//
//    ID3DBlob* pErrorBlob;
//    hr = D3DX11CompileFromFile(szFileName, NULL, NULL, szEntryPoint, szShaderModel,
//	dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL);
//    if (FAILED(hr))
//    {
//	if (pErrorBlob != NULL)
//	    OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
//	if (pErrorBlob) pErrorBlob->Release();
//	return hr;
//    }
//    if (pErrorBlob) pErrorBlob->Release();
//
//    return S_OK;
//}

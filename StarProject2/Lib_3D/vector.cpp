//******************************************************************************
//
//
//      Vector
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "Vector.h"

//******************************************************************************
//
//      DirectX::XMFLOAT2
//
//******************************************************************************

//--------------------------------
//  =
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator=(const VECTOR2& v)
{
	x = v.x;
	y = v.y;
	return *this;
}

//--------------------------------
//  +=
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator+=(const DirectX::XMFLOAT2 &v)
{
	x += v.x;
	y += v.y;
	return *this;
}

//--------------------------------
//  -=
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator-=(const DirectX::XMFLOAT2 &v)
{
	x -= v.x;
	y -= v.y;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator+=(const VECTOR2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x += v.x;
	y += v.y;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator-=(const VECTOR2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x -= v.x;
	y -= v.y;
	return *this;
}

//--------------------------------
//  *=
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator*=(float f)
{
	x *= f;
	y *= f;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator*=(const DirectX::XMFLOAT2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x *= v.x;
	y *= v.y;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator*=(const VECTOR2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x *= v.x;
	y *= v.y;
	return *this;
}

//--------------------------------
//  /=
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator/=(float f)
{
	x /= f;
	y /= f;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator/=(const DirectX::XMFLOAT2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x /= v.x;
	y /= v.y;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator/=(const VECTOR2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x /= v.x;
	y /= v.y;
	return *this;
}

//--------------------------------
//  +（符号）
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator+() const
{
	return DirectX::XMFLOAT2(x, y);
}

//--------------------------------
//  -（符号）
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator-() const
{
	return DirectX::XMFLOAT2(-x, -y);
}

//--------------------------------
//  +（和）
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator+(const DirectX::XMFLOAT2 &v) const
{
	return DirectX::XMFLOAT2(x + v.x, y + v.y);
}

//--------------------------------
//  -（差）
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator-(const DirectX::XMFLOAT2 &v) const
{
	return DirectX::XMFLOAT2(x - v.x, y - v.y);
}

//--------------------------------
//  *
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator*(float f) const
{
	return DirectX::XMFLOAT2(x * f, y * f);
}

DirectX::XMFLOAT2 VECTOR2::operator*(const DirectX::XMFLOAT2& v) const
{
	return DirectX::XMFLOAT2(x * v.x, y * v.y);
}

DirectX::XMFLOAT2 VECTOR2::operator*(const VECTOR2& v) const
{
	return DirectX::XMFLOAT2(x * v.x, y * v.y);
}

//--------------------------------
//  *
//--------------------------------
DirectX::XMFLOAT2 operator*(float f, const DirectX::XMFLOAT2 &v)
{
	return DirectX::XMFLOAT2(v.x * f, v.y * f);
}

//--------------------------------
//  /
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator/(float f) const
{
	return DirectX::XMFLOAT2(x / f, y / f);
}

DirectX::XMFLOAT2 VECTOR2::operator/(const DirectX::XMFLOAT2 &v) const
{
	return DirectX::XMFLOAT2(x / v.x, y / v.y);
}

DirectX::XMFLOAT2 VECTOR2::operator/(const VECTOR2 &v) const
{
	return DirectX::XMFLOAT2(x / v.x, y / v.y);
}

//--------------------------------
//  ==
//--------------------------------
bool VECTOR2::operator == (const DirectX::XMFLOAT2& v) const
{
	return (x == v.x) && (y == v.y);
}

//--------------------------------
//  !=
//--------------------------------
bool VECTOR2::operator != (const DirectX::XMFLOAT2& v) const
{
	return (x != v.x) || (y != v.y);
}

//--------------------------------
//  ==
//--------------------------------
bool VECTOR2::operator==(const VECTOR2 & v) const
{
	return (x == v.x) && (y == v.y);
}

//--------------------------------
//  !=
//--------------------------------
bool VECTOR2::operator!=(const VECTOR2 &v) const
{
	return (x != v.x) || (y != v.y);
}

//--------------------------------
//  長さの2乗を取得
//--------------------------------
float vec2LengthSq(const DirectX::XMFLOAT2& v)
{
	return v.x * v.x + v.y * v.y;
}

//--------------------------------
//  長さを取得
//--------------------------------
float vec2Length(const DirectX::XMFLOAT2& v)
{
	return sqrtf(vec2LengthSq(v));
}

//--------------------------------
//  長さを1にする
//--------------------------------
const DirectX::XMFLOAT2& vec2Normalize(const DirectX::XMFLOAT2& v, DirectX::XMFLOAT2& out)
{
	float d = vec2Length(v);
	if (d == 0.0f) return v;

	float f = 1 / d;
	out = v;
	out.x *= f;
	out.y *= f;
	return out;
}

//******************************************************************************
//
//      DirectX::XMFLOAT3
//
//******************************************************************************

VECTOR3::operator DirectX::XMVECTOR() const
{
	DirectX::XMFLOAT3 temp = *this;
	DirectX::XMVECTOR Vec = DirectX::XMLoadFloat3(&temp);
	return Vec;
}

//--------------------------------
//  =
//--------------------------------
DirectX::XMFLOAT3 &VECTOR3::operator=(const VECTOR3& v)
{
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}

VECTOR3 & VECTOR3::operator=(const DirectX::XMVECTOR & other)
{
	DirectX::XMVECTOR temp = other;
	DirectX::XMStoreFloat3(this, temp);
	return *this;
}

DirectX::XMFLOAT3 & VECTOR3::operator+=(const DirectX::XMFLOAT3 & v)
{
	// TODO: return ステートメントをここに挿入します
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

//--------------------------------
//  +=
//--------------------------------
DirectX::XMFLOAT3 &VECTOR3::operator+=(const VECTOR3 &v)
{
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

DirectX::XMFLOAT3 & VECTOR3::operator-=(const DirectX::XMFLOAT3 & v)
{
	// TODO: return ステートメントをここに挿入します
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}

//--------------------------------
//  -=
//--------------------------------
DirectX::XMFLOAT3 &VECTOR3::operator-=(const VECTOR3 &v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}

//--------------------------------
//  *=
//--------------------------------
DirectX::XMFLOAT3 &VECTOR3::operator*=(float f)
{
	x *= f;
	y *= f;
	z *= f;
	return *this;
}

//--------------------------------
//  /=
//--------------------------------
DirectX::XMFLOAT3 &VECTOR3::operator/=(float f)
{
	x /= f;
	y /= f;
	z /= f;
	return *this;
}

DirectX::XMFLOAT3 & VECTOR3::operator*=(const DirectX::XMFLOAT3 & v)
{
	// TODO: return ステートメントをここに挿入します
	x *= v.x;
	y *= v.y;
	z *= v.z;
	return *this;
}

DirectX::XMFLOAT3 & VECTOR3::operator/=(const DirectX::XMFLOAT3 & v)
{
	// TODO: return ステートメントをここに挿入します
	x /= v.x;
	y /= v.y;
	z /= v.z;
	return *this;
}

DirectX::XMFLOAT3 & VECTOR3::operator*=(const VECTOR3 & v)
{
	// TODO: return ステートメントをここに挿入します
	x *= v.x;
	y *= v.y;
	z *= v.z;
	return *this;
}

DirectX::XMFLOAT3 & VECTOR3::operator/=(const VECTOR3 & v)
{
	// TODO: return ステートメントをここに挿入します
	x /= v.x;
	y /= v.y;
	z /= v.z;
	return *this;
}

//--------------------------------
//  +（符号）
//--------------------------------
DirectX::XMFLOAT3 VECTOR3::operator+() const
{
	return DirectX::XMFLOAT3(x, y, z);
}

//--------------------------------
//  -（符号）
//--------------------------------
DirectX::XMFLOAT3 VECTOR3::operator-() const
{
	return DirectX::XMFLOAT3(-x, -y, -z);
}

DirectX::XMFLOAT3 VECTOR3::operator+(const DirectX::XMFLOAT3 & v) const
{
	return   DirectX::XMFLOAT3(x + v.x, y + v.y, z + v.z);
}

//--------------------------------
//  +（和）
//--------------------------------
DirectX::XMFLOAT3 VECTOR3::operator+(const VECTOR3 &v) const
{
	return DirectX::XMFLOAT3(x + v.x, y + v.y, z + v.z);
}

DirectX::XMFLOAT3 VECTOR3::operator+(float f) const
{
	return DirectX::XMFLOAT3(x + f, y + f, z + f);
}

DirectX::XMFLOAT3 VECTOR3::operator-(const DirectX::XMFLOAT3 & v) const
{
	return  DirectX::XMFLOAT3(x - v.x, y - v.y, z - v.z);
}

//--------------------------------
//  -（差）
//--------------------------------
DirectX::XMFLOAT3 VECTOR3::operator-(const VECTOR3 &v) const
{
	return DirectX::XMFLOAT3(x - v.x, y - v.y, z - v.z);
}

DirectX::XMFLOAT3 VECTOR3::operator-(float f) const
{
	return DirectX::XMFLOAT3(x - f, y - f, z - f);
}

//--------------------------------
//  *
//--------------------------------
DirectX::XMFLOAT3 VECTOR3::operator*(float f) const
{
	return DirectX::XMFLOAT3(x * f, y * f, z * f);
}

DirectX::XMFLOAT3 VECTOR3::operator*(const DirectX::XMFLOAT3 & v) const
{
	return DirectX::XMFLOAT3(x*v.x, y*v.y, z*v.z);
}

DirectX::XMFLOAT3 VECTOR3::operator*(const VECTOR3 & v) const
{
	return DirectX::XMFLOAT3(x*v.x, y*v.y, z*v.z);
}

//--------------------------------
//  *
//--------------------------------
DirectX::XMFLOAT3 operator*(float f, const DirectX::XMFLOAT3 &v)
{
	return DirectX::XMFLOAT3(v.x * f, v.y * f, v.z * f);
}

//--------------------------------
//  /
//--------------------------------
DirectX::XMFLOAT3 VECTOR3::operator/(float f) const
{
	return DirectX::XMFLOAT3(x / f, y / f, z / f);
}

DirectX::XMFLOAT3 VECTOR3::operator/(const DirectX::XMFLOAT3 & v) const
{
	return DirectX::XMFLOAT3(x/v.x, y/v.y, z/v.z);
}

DirectX::XMFLOAT3 VECTOR3::operator/(const VECTOR3 & v) const
{
	return DirectX::XMFLOAT3(x/v.x, y/v.y, z/v.z);
}

//VECTOR3 VECTOR3::operator*()
//{
//	return *this;
//}

//--------------------------------
//  ==
//--------------------------------
bool VECTOR3::operator==(const DirectX::XMFLOAT3 & v) const
{
	return (x == v.x) && (y == v.y) && (z == v.z);
}

//--------------------------------
//  !=
//--------------------------------
bool VECTOR3::operator!=(const DirectX::XMFLOAT3 & v) const
{
	return (x != v.x) || (y != v.y) || (z != v.z);
}

//--------------------------------
//  ==
//--------------------------------
bool VECTOR3::operator == (const VECTOR3& v) const
{
	return (x == v.x) && (y == v.y) && (z == v.z);
}

//--------------------------------
//  !=
//--------------------------------
bool VECTOR3::operator != (const VECTOR3& v) const
{
	return (x != v.x) || (y != v.y) || (z != v.z);
}

//******************************************************************************

#include "Wrapped.h"



namespace Lib_3D {

	//static	framework*	activeFramework = nullptr;










	////--------------------------------------------

	////>SCREEN_WIDTH
	//LONG GetScreenWidth(UINT i)
	//{
	//	return	activeFramework->SCREEN_WIDTH[i];
	//}
	////SCREEN_HEIGHT
	//LONG GetScreenHeight(UINT i)
	//{
	//	return	activeFramework->SCREEN_HEIGHT[i];
	//}



	bool DirectXWindowManager::DirectXInitWindow(HWND hwnd_, size_t SCREEN_WIDTH_, size_t SCREEN_HEIGHT_)
	{
		if (!hwnd_)	return	false;
		DirectX11ComInit ins(hwnd_, SCREEN_WIDTH_, SCREEN_HEIGHT_);
		insList.push_back(ins);
		return	true;
	}

	ID3D11Device * DirectXWindowManager::GetDevice()
	{
		return	pDirectX11Device;
	}

	ID3D11DeviceContext * DirectXWindowManager::GetDeviceContext()
	{
		return	pDirectX11DeviceContext;
	}


	LONG DirectXWindowManager::GetScreenWidth(HWND hwnd_)
	{
		for (auto& it : insList)
		{
			if (it.GetHwnd() == hwnd_)
				return it.GetScreenWidth();
		}
		return -1;
	}

	LONG DirectXWindowManager::GetScreenHeight(HWND hwnd_)
	{
		for (auto& it : insList)
		{
			if (it.GetHwnd() == hwnd_)
				return it.GetScreenHeight();
		}
		return -1;
	}

	IDXGISwapChain * DirectXWindowManager::GetSwapChain(HWND hwnd_)
	{
		for (auto& it : insList)
		{
			if (it.GetHwnd() == hwnd_)
				return it.p_SwapChain;
		}
		return	nullptr;
	}
	ID3D11RenderTargetView * DirectXWindowManager::GetRenderTargetView(HWND hwnd_)
	{
		for (auto& it : insList)
		{
			if (it.GetHwnd() == hwnd_)
				return it.p_RenderTargetView;
		}
		return	nullptr;
	}
	ID3D11RenderTargetView ** DirectXWindowManager::GetRenderTargetViewA(HWND hwnd_)
	{
		for (auto& it : insList)
		{
			if (it.GetHwnd() == hwnd_)
				return &it.p_RenderTargetView;
		}
		return	nullptr;
	}
	ID3D11DepthStencilView * DirectXWindowManager::GetDepthStencilView(HWND hwnd_)
	{
		for (auto& it : insList)
		{
			if (it.GetHwnd() == hwnd_)
				return it.p_DepthStencilView;
		}
		return	nullptr;
	}

	void DirectXWindowManager::UnInitAll()
	{
		for (auto& it : insList)
		{
			if (!it.p_SwapChain)
				return it.UnInitialize();
		}
	}

}





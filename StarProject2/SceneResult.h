#pragma once
#include "Scene.h"
class SceneResult : public Scene, public Singleton<SceneResult>
{
public:
	public:
		void Init()override {}
		BaseLoop* LoopFunc(BaseLoop* loopFunc)override { return nullptr; }
		void Draw()override {}
		void UnInit()override {}
};
#define pSceneResult SceneResult::getInstance();

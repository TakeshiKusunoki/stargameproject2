#pragma once
#include "Scene.h"
#include "Lib_Vassel\BaseClass.h"

using Lib_Vassel::BaseLoop;
// シーン初期化
class SceneInit :public Scene, public Singleton<SceneInit>
{
public:
	void Init();
	BaseLoop* LoopFunc(BaseLoop* loopFunc)override;
	virtual void Draw() {}
	virtual void UnInit() {}
};
#define pSceneInit (SceneInit::getInstance())


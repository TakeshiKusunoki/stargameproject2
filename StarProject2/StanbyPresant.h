#pragma once
#include "Lib_Base\WinMainClass.h"
#include "Lib_Base\WindowEatherProcess.h"
#include "Lib_3D\Wrapped.h"
#include "Lib_Vassel\BaseClass.h"
class Lib_Base::WindowCleate;

//! 画面が縮小表示されているとき、描画しない
//! このクラスをウインドウ管理クラスのprocessExectuteEachWindow関数に引数で渡す
class StanbyPresant : public Lib_Base::WindowEatherProcess
{
private:
	bool _standByMode = false;//! スタンバイモード
	Lib_Base::WindowEatherProcess* _draw;//! 描画処理タスク
public:
	StanbyPresant(Lib_Base::WindowEatherProcess* task)
		: _draw(task)
	{}
	~StanbyPresant() {}
public:
	//! @brief ウインドウの数だけ呼び出される
	void WinEatherProcess(Lib_Base::WindowCleate* win)override
	{
		HRESULT hr = S_OK;
		HWND hwnd = win->GetHWnd();
		// スタンバイモード
		if (_standByMode)
		{
			hr = Lib_3D::pDirectXWindowManager->GetSwapChain(hwnd)->Present(0, DXGI_PRESENT_TEST);
			if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
				return;

			_standByMode = false;//スタンバイ・モードを解除
		}
		//描画タスク(hwndごとに呼び出すものを選定したりとか・・)
		_draw->WinEatherProcess(win);

		//レンダリングされたイメージをユーザーに表示。
		hr = Lib_3D::pDirectXWindowManager->GetSwapChain(hwnd)->Present(0, 0);

		if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
		{
			_standByMode = true;//スタンバイ・モードに入る
		}
		return;
	}
};

//// 画面の更新
//{
//#define VIEW_NUM 1
//	// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
//	D3D11_VIEWPORT Viewport[VIEW_NUM];
//	Viewport[0].TopLeftX = 0;
//	Viewport[0].TopLeftY = 0;
//	Viewport[0].Width = static_cast<FLOAT>(Lib_3D::pDirectXWindowManager->GetScreenWidth(hwnd));
//	Viewport[0].Height = static_cast<FLOAT>(Lib_3D::pDirectXWindowManager->GetScreenHeight(hwnd));//1lo0O10O0O8sSBloO
//	Viewport[0].MinDepth = 0.0f;
//	Viewport[0].MaxDepth = 1.0f;
//
//	// 描画ターゲット・ビューの設定
//	Lib_3D::pDirectXWindowManager->GetDeviceContext()->OMSetRenderTargets(VIEW_NUM, Lib_3D::pDirectXWindowManager->GetRenderTargetViewA(hwnd), Lib_3D::pDirectXWindowManager->GetDepthStencilView(hwnd));
//	Lib_3D::pDirectXWindowManager->GetDeviceContext()->RSSetViewports(VIEW_NUM, Viewport);
//
//
//
//#undef VIEW_NUM
//	// 描画ターゲットのクリア
//	float ClearColor[4] = { 0.0f,0.0f,255.0f,1.0f };//クリアする値
//	Lib_3D::pDirectXWindowManager->GetDeviceContext()->ClearRenderTargetView(Lib_3D::pDirectXWindowManager->GetRenderTargetView(hwnd), ClearColor);
//	// 深度ステンシル リソースをクリアします。
//	Lib_3D::pDirectXWindowManager->GetDeviceContext()->ClearDepthStencilView(Lib_3D::pDirectXWindowManager->GetDepthStencilView(hwnd), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
//
//}
////live2D
///*if(Enum::WINDOW_0 == i)
//LAppDelegate::GetInstance()->Run();*/
//draw(i);
//// レンダリングされたイメージをユーザーに表示します。
//hr = Lib_3D::pDirectXWindowManager->GetSwapChain(hwnd)->Present(0, 0);
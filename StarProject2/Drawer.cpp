#include "Drawer.h"
#include "Stage.h"
#include "Lib_Base\WinMainClass.h"

StanbyPresant Drawer::stanbyPresant = DrawEachWindow::getInstance();
StanbyPresant Drawer::stanbyPresantForTitle = DrawEachWindowInTitle::getInstance();
StanbyPresant Drawer::stanbyPresantForResult = DrawEachWindowInResult::getInstance();


void DrawEachWindow::WinEatherProcess(Lib_Base::WindowCleate * win)
{
	HWND hwnd = win->GetHWnd();

#define VIEW_NUM 1
	// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
	D3D11_VIEWPORT Viewport[VIEW_NUM];
	Viewport[0].TopLeftX = 0;
	Viewport[0].TopLeftY = 0;
	Viewport[0].Width = static_cast<FLOAT>(Lib_3D::pDirectXWindowManager->GetScreenWidth(hwnd));
	Viewport[0].Height = static_cast<FLOAT>(Lib_3D::pDirectXWindowManager->GetScreenHeight(hwnd));//1lo0O10O0O8sSBloO
	Viewport[0].MinDepth = 0.0f;
	Viewport[0].MaxDepth = 1.0f;

	// 描画ターゲット・ビューの設定
	Lib_3D::pDirectXWindowManager->GetDeviceContext()->OMSetRenderTargets(VIEW_NUM, Lib_3D::pDirectXWindowManager->GetRenderTargetViewA(hwnd), Lib_3D::pDirectXWindowManager->GetDepthStencilView(hwnd));
	Lib_3D::pDirectXWindowManager->GetDeviceContext()->RSSetViewports(VIEW_NUM, Viewport);



#undef VIEW_NUM
	// 描画ターゲットのクリア
	float ClearColor[4] = { 0.0f,255.0f,0.0f,0.0f };//クリアする値
	Lib_3D::pDirectXWindowManager->GetDeviceContext()->ClearRenderTargetView(Lib_3D::pDirectXWindowManager->GetRenderTargetView(hwnd), ClearColor);
	// 深度ステンシル リソースをクリアします。
	Lib_3D::pDirectXWindowManager->GetDeviceContext()->ClearDepthStencilView(Lib_3D::pDirectXWindowManager->GetDepthStencilView(hwnd), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//live2D
	/*if(Enum::WINDOW_0 == i)
	LAppDelegate::GetInstance()->Run();
	draw(i);*/


	auto dasda = win->GetClassName_();
	//if(dasda == WINMAIN_CLASS_NAME)
	pStageGame->Draw();
}


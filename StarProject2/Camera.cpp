#include "Camera.h"



DirectX::XMMATRIX Camera::SetOrthographic(float w, float h, float znear, float zfar)
{
	projection = DirectX::XMMatrixOrthographicLH(w, h, znear, zfar);		//	平行投影行列
	return projection;
}

DirectX::XMMATRIX Camera::SetPerspective(float fov, float aspect, float znear, float zfar)
{
	projection = DirectX::XMMatrixPerspectiveFovLH(fov, aspect, znear, zfar);		//	透視投影行列
	return projection;
}

DirectX::XMMATRIX Camera::GetView_()
{
	//	ビュー変換行列89
	DirectX::XMMATRIX view = GetView();//	元々あった処理を削除
									   //position = DirectX::XMVectorSet(0.0f, 0.0f, -5.0f, 1.0f);	//	カメラの位置
									   //target = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);		//	カメラの注視点
									   //DirectX::XMVECTOR	up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);		//	上ベクトル（仮）
									   //view = DirectX::XMMatrixLookAtLH(position, target, up);
	return view;
}



#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include	<Windows.h>

#define	LENGTH	15.0f


DirectX::XMMATRIX Camera::GetView()
{
	DirectX::XMVECTOR	up;

	static int mode = FRONT;

	//if (GetAsyncKeyState('1') & 1)		mode = FRONT;
	//if (GetAsyncKeyState('2') & 1)		mode = BACK;
	//if (GetAsyncKeyState('3') & 1)		mode = UP;
	//if (GetAsyncKeyState('4') & 1)		mode = DOWN;
	//if (GetAsyncKeyState('5') & 1)		mode = LEFT;
	//if (GetAsyncKeyState('6') & 1)		mode = RIGHT;



	//static DirectX::XMFLOAT3 base(0.0f, 0.0f, 0.0f);
	//static DirectX::XMFLOAT3 tBase(0.1f, -10.1f, 0.1f);
	static DirectX::XMFLOAT3 base(0.0f, 0.0f, 10.0f);
	static DirectX::XMFLOAT3 tBase(0.0f, 0.0f, -1.0f);
	//switch (mode)
	//{
	//	case FRONT:		base.z += -LENGTH;		break;		//	奥から手前へ
	//	case BACK:		base.z += +LENGTH;		break;		//	手前から奥へ
	//	case UP:		base.y += -LENGTH;		break;		//	下から上へ
	//	case DOWN:		base.y += +LENGTH;		break;		//	上から下へ
	//	case LEFT:		base.x += -LENGTH;		break;		//	右から左へ
	//	case RIGHT:		base.x += +LENGTH;		break;		//	左から右へ
	//}

#define CAMERA_MOVE_SPEED 0.1f
	if (GetAsyncKeyState('W') < 0)
	{
		base.y -= CAMERA_MOVE_SPEED;
		tBase.y -= CAMERA_MOVE_SPEED;
	}
	if (GetAsyncKeyState('S') < 0)
	{
		base.y += CAMERA_MOVE_SPEED;
		tBase.y += CAMERA_MOVE_SPEED;
	}
	if (GetAsyncKeyState('R') < 0)
	{
		base.z += CAMERA_MOVE_SPEED;
		tBase.z += CAMERA_MOVE_SPEED;
	}
	if (GetAsyncKeyState('F') < 0)
	{
		base.z -= CAMERA_MOVE_SPEED;
		tBase.z -= CAMERA_MOVE_SPEED;
	}
	if (GetAsyncKeyState('A') < 0)
	{
		base.x += CAMERA_MOVE_SPEED;
		tBase.x += CAMERA_MOVE_SPEED;
	}
	if (GetAsyncKeyState('D') < 0)
	{
		base.x -= CAMERA_MOVE_SPEED;
		tBase.x -= CAMERA_MOVE_SPEED;
	}
	cameraPos = { base.x, base.y, base.z, 1.0f };
	position = DirectX::XMLoadFloat4(&cameraPos);

	target = DirectX::XMVectorSet(tBase.x, tBase.y, tBase.z, 1.0f);
	up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	if (mode == UP || mode == DOWN)up = DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);

	return DirectX::XMMatrixLookAtLH(position, target, up);
}


#pragma once
#include "ObjectPoligon.h"
#include "Lib_Render/Wrapped_Render.h"
#include "Lib_3D/Wrapped.h"

	class ItemObject : public PoligonObject
	{

	private:
		PlayerPoligonValiable* _playerPoligonValiable;
	public:
		ItemObject();
		ItemObject(const ItemObject&) {}
		ItemObject(ItemObject&&){}
		~ItemObject() {}
	public:
		void Initialize()override;

		void UnInitialize()override;

	private:
		void Init_();

		void MultiTask()override;

		//! @brief 姿勢制御
		void OrientationCaluculate();

		//! メッセージ処理
		void MsgCmnd();

		//! 姿勢を与える(姿勢をアイテムリストに伝播させる)
		//! @details このクラスの実態をハックさせる
		void LetOrientasion();

		//! このクラスの実体が管理するアイテムリストから情報を得る
		void ItemListHucked();


		void HuckPlayerPoligonValiableObject(PlayerPoligonValiable * obj);

	};

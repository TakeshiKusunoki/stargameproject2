#pragma once
#include "Lib_Base\Template.h"
#include "Lib_Vassel\BaseObject.h"
#include "Lib_Vassel\Renderer.h"
#include "Lib_Vassel\BaseClass.h"
#include "ObjectPoligon.h"
#include "ItemObject.h"

using Lib_Vassel::BaseLoop;

class Stage : public BaseLoop
{
public:
	virtual void Init() = 0;
	virtual void UnInit() = 0;
	virtual BaseLoop* LoopFunc(BaseLoop* loopFunc)override = 0;
	virtual void Draw() = 0;
};


class StageGame : public Stage ,public Singleton<StageGame>
{
private:
	static PlayerPiliod* pleyer;
	static ItemObject* item;
	static ItemObject* item2[10];
	Lib_Vassel::ObjectManager objManager;
	//Lib_Vassel::LoopFuncExecter ocuuringEection;
	//Lib_Vassel::RendererManager renderManager;
public:
	void Init();
	void UnInit();
	BaseLoop* LoopFunc(BaseLoop* loopFunc)override;
	void Draw();

};
#define pStageGame StageGame::getInstance()
#pragma once
#include "Lib_Vassel\Compare.h"
using Lib_Vassel::Comparer;


//! @brief 当たったらtrue msgを送る
class CompareHitSerchEnemy: public Comparer
{
private:
protected:
public:
	CompareHitSerchEnemy()
	{
	}
public:
	//!
	void Compare(Lib_Vassel::LocationObjectList* list)override;


};

//! @brief 当たったらtrue msgを送る
class CompareHitSerchItem : public Comparer
{
private:
public:
	CompareHitSerchItem()
	{
	}
public:
	//!
	void Compare(Lib_Vassel::LocationObjectList* list)override;


};
#pragma once
#include "Scene.h"

class SceneGame : public Scene, public Singleton<SceneGame>
{
public:
	void Init()override;
	BaseLoop* LoopFunc(BaseLoop* loopFunc)override;
	void Draw()override;
	void UnInit()override;
};
#define pSceneGame SceneGame::getInstance();


#include "SceneInit.h"
#include "SceneTitle.h"
#include "Lib_Render/RenderPoligonMesh.h"
#include "Lib_Base\Wrapped_Base.h"
#include "Lib_3D\Wrapped.h"
#include "Lib_Base\winMainFunc.h"
#include "Common.h"
#include <ctime>


void SceneInit::Init()
{
	srand((unsigned)time(NULL));

	Lib_Base::pWindowCreateManager->AddWindow(Lib_Base::MainWindow::InitWindow, WINMAIN_CLASS_NAME, 0, 0, 1000, 1000);
	auto win = Lib_Base::pWindowCreateManager->GetWindowWithClassName(WINMAIN_CLASS_NAME);
	Lib_3D::pDirectXWindowManager->DirectXInitWindow(win->GetHWnd(), win->GetWindowWidth(), win->GetWindowHeight());

	Lib_Base::Console();

	Lib_Render::pLoderPoligonMesh->Loder(const_cast<wchar_t**>(USER::poligon_filename));
}

BaseLoop * SceneInit::LoopFunc(BaseLoop * loopFunc)
{

	return pSceneTitle;
}

#include "CompareHit.h"
#include "Lib_Vassel\BaseObject.h"
#include "Common.h"
#include "Lib_base/my_util.h"
using Lib_Vassel::BaseObject;

void CompareHitSerchEnemy::Compare(Lib_Vassel::LocationObjectList* list)
{
	for (auto& it1 : list->_playerList)//platyer in location
	{
		if (!it1->_public._judgeFlag)
			continue;
		for (auto& it2 : list->_enemyList)//enemy in location
		{
			if (!it2->_public._judgeFlag)
				continue;
			for (size_t i = 0; i < it1->_public._vertexList.size() - 1; i++)
			{
				Segment sg;//頂点の線分
				sg.start = DirectX::XMFLOAT2(it1->_public._vertexList.at(i)->x, it1->_public._vertexList.at(i)->y);
				sg.end = DirectX::XMFLOAT2(it1->_public._vertexList.at(i + 1)->x, it1->_public._vertexList.at(i + 1)->y);
				for (size_t i2 = 0; i2 < it2->_public._vertexList.size() - 1; i2++)
				{
					Segment sg2;//頂点の線分
					sg2.start = DirectX::XMFLOAT2(it2->_public._vertexList.at(i2)->x, it2->_public._vertexList.at(i2)->y);
					sg2.end = DirectX::XMFLOAT2(it2->_public._vertexList.at(i2 + 1)->x, it2->_public._vertexList.at(i2 + 1)->y);
					if (!ColSegments(sg, sg2))
						continue;
					//エネミーに当たった時の処理
					it1->_public._msg = (USER::MSG[Enum::HIT_ENEMY]);
					it2->_public._msg = (USER::MSG[Enum::HIT_ENEMY]);
					break;
				}
			}
		}
	}
}

//void CompareHitSerchEnemy::SetList(Lib_Vassel::LocationObjectList * list)
//{
//	_list1 = &list->_playerList;
//	_list2 = &list->_enemyList;
//}

void CompareHitSerchItem::Compare(Lib_Vassel::LocationObjectList* list)
{
	for (auto& it1 : list->_playerList)//platyer in location
	{
		if (!it1->_public._judgeFlag)
			continue;
		for (auto& it2 : list->_itemList)//enemy in location
		{
			if (!it2->_public._judgeFlag)
				continue;
			for (size_t i = 0; i < it1->_public._vertexList.size() - 1; i++)
			{
				VECTOR3 pos = (VECTOR3)*it1->_public._position.second * 1000;
				VECTOR3 worldPos = pos + *it1->_public._vertexList.at(i);
				VECTOR3 worldPos2 = pos + *it1->_public._vertexList.at(i + 1);
				Segment sg;//頂点の線分
				sg.start = DirectX::XMFLOAT2(worldPos.x, worldPos.y);
				sg.end = DirectX::XMFLOAT2(worldPos2.x, worldPos2.y);

				for (size_t i2 = 0; i2 < it2->_public._vertexList.size() - 1; i2++)
				{
					pos = (VECTOR3)*it2->_public._position.second * 1000;
					worldPos = pos + *it2->_public._vertexList.at(i);
					worldPos2 = pos + *it2->_public._vertexList.at(i + 1);
					Segment sg2;//頂点の線分
					sg2.start = DirectX::XMFLOAT2(worldPos.x, worldPos.y);
					sg2.end = DirectX::XMFLOAT2(worldPos2.x, worldPos2.y);

					if (!ColSegments(sg, sg2))
						continue;
					//アイテムに当たった時の処理
					it1->_public._msg = (USER::MSG[Enum::HIT_ITEM]);
					it2->_public._msg = (USER::MSG[Enum::HIT_ITEM]);
					it1->HuckBaseObjectVassel(it2);
					it2->HuckBaseObjectVassel(it1);
					break;
				}
			}
		}
	}
}

//void CompareHitSerchItem::SetList(Lib_Vassel::LocationObjectList * list)
//{
//	_list1 = &list->_playerList;
//	_list2 = &list->_itemList;
//}
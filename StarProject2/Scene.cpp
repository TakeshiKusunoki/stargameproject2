#include "Scene.h"
#include "Lib_Base\Wrapped_Base.h"

bool SceneManager::execute(BaseLoop * scene)
{
	_pNext = scene->LoopFunc(scene);//1回だけ回る
	reinterpret_cast<Scene*>(scene)->Draw();

	if (GetAsyncKeyState(VK_ESCAPE))
		_pNext = nullptr;

	if (_pNext == scene)
		return true;
	else
		return false;
}

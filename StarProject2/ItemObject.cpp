#include "ItemObject.h"
#include "Common.h"

ItemObject::ItemObject()
{
	Init();
	Init_();
}

void ItemObject::Initialize()
{
	_data._pVertexData = Lib_Render::LoderPoligonMesh::getInstance()->GetMeshData(USER::poligon_filename[1]);
	if (!_protected._pRenderer && _data._pVertexData)
		_protected._pRenderer = new Lib_Render::RenderPoligonMesh(_data._pVertexData, _data._vertex, _data._texpos, &_data._pos, &_data._orienttion, &_data._scale);
	Lib_Vassel::pRendererManager->Add(_protected._pRenderer);
}

void ItemObject::UnInitialize()
{

	if (_protected._pRenderer)
	{
		delete _protected._pRenderer;
		_protected._pRenderer = nullptr;
	}
}

void ItemObject::Init_()
{
	{
		SecureZeroMemory(&_data, sizeof(_data));

		if (rand() % 2 == 0)
		{
			float x = static_cast<float>(rand() % 5 + 2) / 10;
			float y = static_cast<float>(rand() % 5 + 2) / 10;
			_data._pos = VECTOR3(x, y, 0);
		}
		else
		{
			float x = static_cast<float>(-rand() % 5 - 2) / 10;
			float y = static_cast<float>(-rand() % 5 - 2) / 10;
			_data._pos = VECTOR3(x, y, 0);
		}

		_data._scale = VECTOR3(1, 1, 1);
		_data._orienttion = DirectX::XMQuaternionIdentity();

		//頂点座標定義
		VECTOR3 v[VERTEX_NUM] = {//頂点▲
								 /*VECTOR3(-static_cast<float>(10 >> 1) * 10,(7 / 2) * 10 ,0),
								 VECTOR3((10 >> 1) * 10,(7 / 2) * 10,0),
								 VECTOR3(0     ,-(7 / 2) * 10 ,0)*/
			VECTOR3(100,100,0),
			VECTOR3(0 ,0,0),
			VECTOR3(200     ,0,0)
		};
		//テクスチャ座標定義
		VECTOR2 t[VERTEX_NUM] = { VECTOR2(40,0),VECTOR2(0,0),VECTOR2(200,200) };//テクスチャ
		for (size_t i = 0; i < VERTEX_NUM; i++)
		{
			_data._vertex[i] = v[i];
			_data._texpos[i] = t[i];
			_public._vertexList.push_back(&_data._vertex[i]);
		}
		_public._position.second = reinterpret_cast<VECTOR3*>(&_data._pos);
		//
		_public._label = (USER::LABEL[Enum::ITEM]);//プレイヤーラベル
	}
}

void ItemObject::MultiTask()
{
	MsgCmnd();
	OrientationCaluculate();
	LetOrientasion();
	ItemListHucked();
}

void ItemObject::OrientationCaluculate()
{
	_playerPoligonValiable = reinterpret_cast<PlayerPoligonValiable*>(_public._pBaseObjectHucked);

	if (_public._label == USER::LABEL[Enum::PLAYER])
	{
		_data._orienttion = DirectX::XMQuaternionMultiply(_data._localOrientation, _playerPoligonValiable->_data._orienttion);
		DirectX::XMVECTOR thisObjLocalPos = DirectX::XMLoadFloat3(&_data._localPos);
		DirectX::XMVECTOR afterRoatedPos = DirectX::XMVector3Rotate(thisObjLocalPos, _playerPoligonValiable->_data._orienttion);// 親分の回転
																																//位置
		{
			VECTOR3 targetPos = _playerPoligonValiable->_data._pos;// 親の位置座標
			VECTOR3 afterRoatedPos_;
			DirectX::XMStoreFloat3(&afterRoatedPos_, afterRoatedPos);
			VECTOR3 afterMovedPos = targetPos + afterRoatedPos_;// 動いた後の位置
			_data._pos = afterMovedPos;
		}
	}
}

void ItemObject::MsgCmnd()
{
	if (_public._msg == USER::MSG[Enum::HIT_PLAYER])
	{
		ShowMsg();

		//DummyObject nuu;//ダミーオブジェクト
		//_protected._pBaseObjectHucked->SwapInstance(&nuu);//_pBaseObjectHuckedとnuuの_my_instanceを入れ替える//managerのほうのインスタンスモードで呼ぶのがnuuになる
		//nuu.SetLabel(_public._label);//ラベルをプレイヤーに変更
		//_itemList.push_back(nuu);//くっついたオブジェクトのインスタンスを持った、器をリストに加える
	}
}

void ItemObject::LetOrientasion()
{

}

void ItemObject::ItemListHucked()
{
}

void ItemObject::HuckPlayerPoligonValiableObject(PlayerPoligonValiable * obj)
{
	_playerPoligonValiable = obj;
	_public._pLom = obj->_public._pLom;
	//_public._msg = _pPlayerPoligon->_public._msg;
	//HuckBaseObjectVassel(obj->_public._pBaseObjectHucked);
}

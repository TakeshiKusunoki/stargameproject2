#pragma once
#include <vector>
#include "BaseClass.h"
//#include "BaseObject.h"
#include "Compare.h"
#include "../Lib_3D//vector.h"
//class BaseObject;
#include "LocationObjectList.h"
/**
* @brief Interfaceメッセージの形
* @details 位置で分割してオブジュジェクトの情報を記載する
* @par 詳細
* 範囲に来たオブジェクトはこのクラスに情報を記載(let)する
* その情報のみで情報の比較を行う、その後、比較して渡す情報を(msg)でオブジェクトに渡す
* 1f毎に情報はクリーンする
*/
namespace Lib_Vassel {
	class BaseObject;
	class Comparer;

	class Location
	{
	private:
		//ECTOR3 v;
		static int count;
		int num;
		std::vector<Comparer*> _comparerList;
		//Comparer* comp;
		//std::list<BaseObject*> infoList;

	public:
		LocationObjectList list;
	public:
		Location();
		Location(const Location&) {}
		Location(Location&&) {}
		~Location();
	public:
		//! @brief オブジェクトがletするときに呼ぶ(自発的に実行しない)
		//! Objectリストに１時的に追加(1フレーム毎にクリア)
		void AddObject(BaseObject* obj_);


		//! @brief このクラスを作るとき、コンペアも作る
		void AddCompare(Comparer* compare);

		//! @brief リスト情報を見せる
		void Show();
		//! @breif 一せいにメッセージロケーションのコンペア処理を行うので、その時呼ぶ
		void Compare();

		//! @breif 毎フレームループの処理の最初に呼ぶ
		void Clear();

		//! @brief コンペリスト消去
		void UnInit();

	};
}

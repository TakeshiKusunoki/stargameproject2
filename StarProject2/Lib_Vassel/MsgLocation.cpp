#include "MsgLocation.h"
#include <iostream>
#include "../Common.h"//後でローカルでメッセージロケーションを作れるようにする


namespace Lib_Vassel {
	//+ MsgLocation----------------

	//void Location::AddObject(BaseObject * obj_)
	//{
	//	/*OBJ obj;
	//	obj.v = obj_->v;
	//	infoList.push_back(obj);*/
	//}
	int Location::count = 0;;

	void Location::Show()
	{
		std::cout << num << "\t" ;
		std::cout << list._playerList.size() << "\t";
		std::cout << list._itemList.size() << "\t";
		std::cout << list._enemyList.size() << "\t" << std::endl;
	}

	Location::Location()
	{
		num = count;
		count++;
	}

	Location::~Location()
	{
		count = 0;
	}

	void Location::AddObject(BaseObject * obj_)
	{
		if (obj_->_public._label == USER::LABEL[Enum::LABEL_DEFINE::PLAYER])
		{
			list._playerList.push_back(obj_);
			return;
		}
		if (obj_->_public._label == USER::LABEL[Enum::LABEL_DEFINE::ENEMY])
		{
			list._enemyList.push_back(obj_);
			return;
		}
		if (obj_->_public._label == USER::LABEL[Enum::LABEL_DEFINE::ITEM])
		{
			list._itemList.push_back(obj_);
			return;
		}
	}

	void Location::AddCompare(Comparer* compare)
	{
		//compare->SetList(&list);
		//compare->SetList(&list);
		_comparerList.push_back(compare);
	}

	void Location::Compare()
	{
		for (size_t i = 0; i < _comparerList.size(); i++)
		{
			/*if (_comparerList.size() > 3)
				break;*/
			_comparerList.at(i)->Compare(&list);
		}
		/*for (auto& it : _comparerList)
		{
			if(_comparerList.size()>1)
			it->Compare();
		}*/
	}

	void Location::Clear()
	{
		list._playerList.clear();
		list._enemyList.clear();
		list._itemList.clear();
	}

	void Location::UnInit()
	{
		//for (size_t i = 0; i < _comparerList.size(); i++)
		//{
		//	/*if (_comparerList.size() > 3)
		//		break;*/
		//	if (_comparerList.at(i))
		//	{
		//		delete _comparerList.at(i);
		//		_comparerList.at(i) = nullptr;
		//	}
		//}
		_comparerList.clear();
	}

}
#pragma once
#include "MsgLocation.h"
//#include <array>
#include <list>
#include "../Lib_3D/vector.h"
#include "../Lib_Base/Template.h"

//class Lib_Vassel::Location;

namespace Lib_Vassel {
	class Location;
	//! (オブジェクトにどのLocationManagerに属するかの値を渡すので、位置だけからletできる(forループで検索しなくていい))
	//! 長方形を長方形に敷き詰める(架空なので当たり判定がいらない)
	class LocationManager
	{
	private:
	public:
		VECTOR3 _pos;//! 配列開始位置(位置差分)

		static const size_t ARRAY_WIDTH = 4;//配列幅の大きさ
		static const size_t ARRAY_SIZE = ARRAY_WIDTH*ARRAY_WIDTH*ARRAY_WIDTH;//配列長さ
		static const size_t LOCATION_SIZE = 500;//ロケーション一つの大きさ
		//std::array<Location, ARRAY_SIZE> _array;
		std::vector<Location> _array;
	public:
		LocationManager(const VECTOR3& pos)
			:_pos(pos)
		{
			_array.resize(ARRAY_SIZE);
		}
		~LocationManager() {}

		void Update(const DirectX::XMFLOAT4& pos);
		void Show();
	private:
	};
	/*class LocationManager_ : public Singleton<LocationManager_>
	{
	public:

	private:

	};

#define pLocationManager_ LocationManager_::getInstance();*/
}

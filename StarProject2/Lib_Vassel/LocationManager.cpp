#include "LocationManager.h"
#include <iostream>
namespace Lib_Vassel {
	void LocationManager::Update(const DirectX::XMFLOAT4 & pos)
	{
		_pos.x = pos.x - static_cast<float>(LOCATION_SIZE / 1000.f) * (ARRAY_WIDTH);
		_pos.y = pos.y - static_cast<float>(LOCATION_SIZE / 1000.f) * (ARRAY_WIDTH);
		_pos.z = 0 /*- LOCATION_SIZE / 1000 * (ARRAY_WIDTH)*/;
		std::cout << "LocationManager" <<_pos.x << "\t" << _pos.y << "\t" << _pos.z << std::endl;

		for (size_t i = 0; i <ARRAY_SIZE; i++)
		{
			_array[i].Compare();
			_array[i].Clear();
		}
	}

	void LocationManager::Show()
	{
		for (size_t i = 0; i <_array.size(); i++)
		{
			_array[i].Show();
		}
	}
}


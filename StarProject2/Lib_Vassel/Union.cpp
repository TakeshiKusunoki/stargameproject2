#include "Union.h"
#include <type_traits>

//! x型名
#define PUSH(x, y) { if (typeName == typeid(x).name()){setedValue = static_cast<ValueType>(y);return;}}
#define PUSH_(x, x_) { if (typeName == typeid(x).name()){setedValue = reinterpret_cast<x>(setInValue);return;}}
#define SET(x, y) \
do{\
if (typeName == typeid(x).name())\
	{\
		setedValue = y;\
		return;\
	}\
}\
while(0)

template <typename ValueType>
void UserValue::TypeSwitch(const std::string & str, ValueType& setedValue)
{

	std::is_array<class T>();

	std::is_same<T, U>::value

	const char* typeName = str.c_str();
	//型が同じでないなら返る
	if (typeid(ValueType).name() != typeName)
		return;

	UnionAllValueType setInValue = _value.second;//代入するunion
	SET(char, setInValue._char);
	SET(signed char , setInValue._char_s);
	SET(unsigned char, setInValue._char_u);
	SET(wchar_t, setInValue._wchar);
	SET(short, setInValue._short);
	SET(signed short, setInValue._char_s);
	SET(unsigned short, setInValue._short_u);
	SET(int, setInValue._int);
	SET(signed int, setInValue._int_s);
	SET(unsigned int, setInValue._int_u);
	SET(long, setInValue._long);
	SET(signed long, setInValue._long_s);
	SET(unsigned long, setInValue._long_u);
	SET(long long, setInValue._long__long);
	SET(signed long long, setInValue._long__long_s);
	SET(unsigned long long, setInValue._long__long_u);
	SET(int8_t, setInValue.__i08);
	SET(int16_t, setInValue.__i16);
	SET(int32_t, setInValue.__i32);
	SET(int64_t, setInValue.__i64);
	SET(uint8_t, setInValue.__ui08);
	SET(uint16_t, setInValue.__ui16);
	SET(uint32_t, setInValue.__ui32);
	SET(uint64_t, setInValue.__ui64);
	SET(int_least8_t, setInValue.__i_least08);
	SET(int_least16_t, setInValue.__i_least16);
	SET(int_least32_t, setInValue.__i_least32);
	SET(int_least64_t, setInValue.__i_least64);
	SET(uint_least8_t, setInValue.__ui_least08);
	SET(uint_least16_t, setInValue.__ui_least16);
	SET(uint_least32_t, setInValue.__ui_least32);
	SET(uint_least64_t, setInValue.__ui_least64);
	SET(int_fast8_t, setInValue.__i_fast08);
	SET(int_fast16_t, setInValue.__i_fast16);
	SET(int_fast32_t, setInValue.__i_fast32);
	SET(int_fast64_t, setInValue.__i_fast64);
	SET(uint_fast8_t, setInValue.__ui_fast08);
	SET(uint_fast16_t, setInValue.__ui_fast16);
	SET(uint_fast32_t, setInValue.__ui_fast32);
	SET(uint_fast64_t, setInValue.__ui_fast64);
	SET(intmax_t, setInValue.__max_int);
	SET(uintmax_t, setInValue.__max_uint);
}

//void UserValueManager::Add()
//{
//}

#pragma once
#include "BaseLoop.h"
#include "BaseClass.h"
namespace Lib_Vassel {
	class Counter
	{
	private:
		int _count;
		BaseDecide* _decide;
		BaseDecide* _decideReset;
		bool AdvancedFlag;////進行フラグ
	public:
		Counter()
			: _count(0)
			, _decide(nullptr)
			, _decideReset(nullptr)
			, AdvancedFlag(false)
		{
		}
		~Counter() {}
		//! カウンター進行、カウンター進行が止まるとカウンターをリセットする
		//! ループ内でn回trueを返す
		//! @return カウンター進行している間、フラグtrueを返す
		bool FlagAdvanced(const size_t n)
		{
			AdvancedCounter(n);
			if (IsAdvanced())
				return true;
			CounterReset();
			return false;
		}
		//この値まで進行
		void AdvancedCounter(const size_t n)
		{
			if (static_cast<size_t>(_count) > n)
			{
				AdvancedFlag = false;
				return;
			}
			_count++;
			AdvancedFlag = true;
		}
		int GetCounter()
		{
			return _count;
		}
		//処理が進んでいるか
		bool IsAdvanced()
		{
			return AdvancedFlag;
		}
		//値リセット
		void CounterReset()
		{
			_count = 0;
		}
	private:

	};


	class Sample : public BaseLoop, public  BaseTask
	{
	private:
		BaseLoop* _nextLoopFunc;//! 次に実行されるループファンク(thisでagain、 nullptrでループ抜け)
		LoopFuncExecter _executer;
		Counter counter;//! 待機カウンター
		Counter counterLoop;//! ループカウンター
	public:
		Sample(const Sample& s) {}
		Sample(BaseLoop* nextLoopFunc)
			: _nextLoopFunc(nullptr)
		{
			_nextLoopFunc = nextLoopFunc;
			Start();
		}
		~Sample() {}

	public:

		//! @brief ループファンク
		BaseLoop* LoopFunc(BaseLoop* loopFunc)
		{
			//auto if
			if (Weit(10))//10回ウェイト
				return _nextLoopFunc;
			if (!DoLoopTimes(3))//3回るーぷ
				return _nextLoopFunc;

			MultiTask();
			_executer.Executer();
			return _nextLoopFunc;
		}
		//! @brief 次のループファンク
		BaseLoop* NextLoopFunc(BaseLoop* loopFunc)
		{
			return loopFunc;
		}
	protected:
		//! @brief 実行処理
		void MultiTask()override;
	private:
		//! @brief 初期化
		void Start();
		//! @brief 指定フレーム数処理を待機
		bool Weit(const int n)
		{
			return counter.FlagAdvanced(n);
		}
		//! @brief ループするか？
		bool DoLoopTimes(const int n)
		{
			return counterLoop.FlagAdvanced(n);
		}
		//! @brief セッター
		void SetNextLoopFunc(BaseLoop* nextLoopFunc)
		{
			_nextLoopFunc = nextLoopFunc;
		}
		//! @brief アクトフレームを進めてもう一回
		void Again()
		{
			_nextLoopFunc = this;
		}
		//! @brief ループを抜ける
		void GetOut()
		{
			_nextLoopFunc = nullptr;
		}
	};
}

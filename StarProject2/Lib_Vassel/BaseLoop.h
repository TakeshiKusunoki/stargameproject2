#pragma once
#include "../Lib_Base/Flag.h"

#include <vector>
#include <list>
#include "BaseClass.h"

namespace Lib_Vassel {

	//! @brief 1fループ
	class ActFrame : public BaseLoop
	{
	public:
		BaseLoop* LoopFunc(BaseLoop* loopFunc_) override;
		//! @brief 転生関数
		void ActFrame_(FlagForOneTimeCall flag, int actFlame = 0);
		//! @brief セッター
		void SetNextLoopFunc(BaseLoop* nextLoopFunc)
		{
			//_nextLoopFunc = nextLoopFunc;
		}
	};




	//! @brief 再起関数
	//! 1つの実体につき１処理
	class Recursion
	{
		friend class CmandInterface;
	private:
		int _callNum;//! 読んだ回数
		std::vector<BaseLoop*> _loopFuncList;//実態を持たせない
		BaseLoop* _startloopFunc;//初めのループファンク
	public:
		Recursion(BaseLoop* loopFunc_)
			: _callNum(-1)
			, _startloopFunc(loopFunc_)
		{
			_loopFuncList.clear();
			OcurringSucssecion(loopFunc_);
		}
		~Recursion() {}
	private:
		//! @brief 連続した関数を呼ぶ
		//! @details BaseLoopクラスが戻り値により連なる
		void OcurringSucssecion(BaseLoop* loopFunc_);

	};
	//! @brief RecursionのリストにプッシュすることでloopFuncを実行する
	class LoopFuncExecter
	{
	private:
		std::vector<BaseLoop*> _loopFuncList;//再帰始め関数りすと
	public:
		LoopFuncExecter() {}
		~LoopFuncExecter() {}
		//Recursionをまとめて実行
		void Executer()
		{
			for (auto& it : _loopFuncList)
			{
				if (!it)
					continue;
				Recursion recursion(it);//まとめて実行
			}
		}
		//再帰始め関数を入れる
		void Add(BaseLoop* loopFunc_)
		{
			_loopFuncList.push_back(loopFunc_);
		}
		void Clear()
		{
			_loopFuncList.clear();
		}
	private:
	};
	//! @brief RecursionのリストにプッシュすることでloopFuncを実行する
	class LoopFuncExecterWhile : public BaseLoop
	{
	private:
		std::vector<BaseLoop*> _loopFuncList;//再帰始め関数りすと
		BaseLoop* _nextLoopFunc;//! 次に実行されるループファンク(thisでagain、 nullptrでループ抜け)
	public:
		LoopFuncExecterWhile() {}
		~LoopFuncExecterWhile() {}
		//Recursionをまとめて実行
		BaseLoop* LoopFunc(BaseLoop* loopFunc_) override
		{
			for (auto& it : _loopFuncList)
			{
				if (!it)
					continue;
				Recursion recursion(it);//まとめて実行
			}
			return _nextLoopFunc;
		}
		//再帰始め関数を入れる
		void Add(BaseLoop* loopFunc_)
		{
			_loopFuncList.push_back(loopFunc_);
		}
		void Clear()
		{
			_loopFuncList.clear();
		}
	private:
	};


	//! BaseLoopの実態を作りたいときや
	//! BaseLoopの戻り値として、BaseLoopの実態を選択したいとき、このリストを参照する
	class BaseLoopManager
	{
	private:
		//std::pair<int , BaseLoop*> IDつき
		std::vector<BaseLoop*> loopFuncList;
	public:
		void Add(BaseLoop* loopFunc_)
		{
			loopFuncList.push_back(loopFunc_);
		}
		//! @brief 関数名からループファンクを設定
		void SetNextLoopFunc(BaseLoop* nextLoopFunc, char* funcname)
		{
			for (size_t i = 0; i < loopFuncList.size(); i++)
			{
				if (funcname != "")
					continue;
				//loopFuncList.at(i).SetNextLoopFunc(nextLoopFunc);
				break;
			}
		}
	};

	//! BaseLoopManagerList(シングルトン)



	//
	//template <typename T>
	//BaseLoop*  LoopFunc_();
	//BaseLoop*  LoopFunc_(BaseLoop * loopFunc_);
}
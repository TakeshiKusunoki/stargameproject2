#include "Stage.h"

#include "ObjectPoligon.h"
//#include "Lib_Vassel\BaseLoop.h"

#include "Lib_3D\Wrapped.h"


#include "StanbyPresant.h"
#include "Camera.h"
#include "Lib_Base\Wrapped_Base.h"
#include "Lib_Vassel\LocationManager.h"
#include "CompareHit.h"

Lib_Vassel::LocationManager locationManager(VECTOR3(0,0,0));
CompareHitSerchEnemy compareEne;
CompareHitSerchItem compareItem;

PlayerPiliod* StageGame::pleyer = nullptr;
ItemObject* StageGame::item = nullptr;
ItemObject* StageGame::item2[] = { nullptr };

void StageGame::Init()
{
	pleyer = new PlayerPiliod;
	item = new ItemObject;
	for (size_t i = 0; i < 10; i++)
	{
		item2[i] = new ItemObject;
		objManager.Add(item2[i], &locationManager);
	}

	objManager.Add(pleyer, &locationManager);
	objManager.Add(item, &locationManager);

	//objManager.InitAll();

	for (size_t i = 0; i < locationManager.ARRAY_SIZE; i++)
	{
		locationManager._array[i].AddCompare(&compareEne);
		locationManager._array[i].AddCompare(&compareItem);
	}

	auto win = Lib_Base::pWindowCreateManager->GetWindowWithClassName(WINMAIN_CLASS_NAME);
	LONG width = win->GetWindowWidth();
	LONG height = win->GetWindowHeight();
	float aspect = (float)width / height;
	pCameraManager->SetPerspective(300 * 0.01745f, aspect, 0.1f, 1000.0f);
	//renderManager.Add(&pleyer);
}

void StageGame::UnInit()
{
	for (size_t i = 0; i < locationManager.ARRAY_SIZE; i++)
	{
		locationManager._array[i].UnInit();
	}
	Lib_Vassel::pRendererManager->Clear();
	objManager.UnInitAll();
	if (pleyer)
	{
		delete pleyer;
		pleyer = nullptr;
	}
	if (item)
	{
		delete item;
		item = nullptr;
	}
	for (size_t i = 0; i < 5; i++)
	{
		if (item2[i])
		{
			delete item2[i];
			item2[i] = nullptr;
		}
	}
}

BaseLoop * StageGame::LoopFunc(BaseLoop * loopFunc)
{
	static int timer = 0;
	printf("flamestart\t%d\n\n", ++timer);
	objManager.Let();
	//locationManager.Show();
	locationManager.Update(pCameraManager->GetCameraPos());
	objManager.Execute();

	return nullptr;
}


void StageGame::Draw()
{
	Lib_Vassel::pRendererManager->Draw();
}






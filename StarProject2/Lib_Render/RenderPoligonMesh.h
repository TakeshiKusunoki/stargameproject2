#pragma once
#include <directxmath.h>
#include <vector>

#include "BaseRender.h"
#include "../Lib_Base/Template.h"
#include "../Lib_3D/VertexPoligon3D.h"
#include "../Lib_3D/Poligon3D.h"

class Lib_3D::Poligon3D;
class Lib_3D::VertexPoligon3D;
using Lib_3D::VertexPoligon3D;
using Lib_3D::Poligon3D;
namespace Lib_Render {

	class LoderPoligonMesh : public BaseLoder, public Singleton<LoderPoligonMesh>
	{
	private:
		//! @brief VertexPoligon3D objの実態
		Lib_3D::VertexPoligon3D** _objArray;//ロードした頂点データとメッシュデータ
		size_t _size;//! ロード数
		wchar_t** _textureFilename;//読み込んだファイル名
	public:
		//! @brief ファイルネームからロード
		void Loder(wchar_t* textureFilename[]);
		//! @brief リリース
		void Release();
		//! @brief ファイル名の頂点データ取得
		VertexPoligon3D* GetMeshData(wchar_t* fileName);

	};
#define pLoderPoligonMesh LoderPoligonMesh::getInstance()




	////! @breif レンダラーの基底クラス
	////! @details T* obj;が基底クラスにある
	//template <class T>
	//class BaseRenderer_
	//{
	//protected:
	//	T* _obj;
	//public:
	//	virtual void Render() = 0;
	//
	//};

	//! @brief ３角形ポリゴン描画クラス
	//! @details この中の値は一切動的に変更しない
	class RenderPoligonMesh : public BaseRender/*, public AlignedAllocationPolicy<16>*/
	{
	private:
		bool bLoad;
		Lib_3D::Poligon3D _obj;
		const DirectX::XMFLOAT3*	_pos;//!	座標
		const DirectX::XMVECTOR*	_orientation;//!	回転角度
		const DirectX::XMFLOAT3*	_scale;//!	大きさ
		//const DirectX::XMFLOAT4*	_color;//!	メッシュの色
		//const DirectX::XMFLOAT4* _lightVector;//! 光方向
		//const DirectX::XMFLOAT4* _camerapos;//! カメラ位置
		//const DirectX::XMFLOAT4* _lightColor;//! 光の色
		//const DirectX::XMFLOAT4* _newtoralColor;//! 自然光
		DirectX::XMMATRIX _view;
		DirectX::XMMATRIX _projection;
		const DirectX::XMFLOAT3* _vertex;
		const DirectX::XMFLOAT2* _texpos;
	public:
		RenderPoligonMesh(const Lib_3D::VertexPoligon3D* vertexData,
			const DirectX::XMFLOAT3 _vertex[3], const DirectX::XMFLOAT2 _texpos[3],
			const DirectX::XMFLOAT3* pos, const DirectX::XMVECTOR*	orientation, const DirectX::XMFLOAT3*	scale
		);
		~RenderPoligonMesh() {}


		//	情報の初期化
		/*void	Initialize();*/

		//	FBXの読込
		//bool	Load(const wchar_t * fbx_filename);

		//	既存MyMeshデータの使い回し
		//	引数
		//primitive:メッシュモデル
		//void	SetMesh(RenderPoligonMesh& org);

		//	プリミティブの設定
		//	引数
		//primitive:メッシュモデル
		void	VertexChange_(VertexPoligon3D* vertexPoligon3D);

		//ゲッター
		//const Poligon3D& GetObj();

		//	メッシュの解放
		//void	Release();

		//! @brief ビューの取得(値渡し)
		void SetWorldView(const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& projection);

		//! @brief それぞれのrender関数を呼ぶ
		void Render()override;

		//	ワールド変換行列の取得
		DirectX::XMMATRIX	GetWorldMatrix();
	private:
	};

}
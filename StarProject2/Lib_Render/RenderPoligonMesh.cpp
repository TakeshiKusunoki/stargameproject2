#include "RenderPoligonMesh.h"
#include	"../Lib_3D//Wrapped.h"
#include "../Lib_Vassel/BaseClass.h"
#include <array>
#include <wchar.h>
#include "../Camera.h"

//+ ローダー
namespace Lib_Render {
	void LoderPoligonMesh::Loder(wchar_t* textureFilename[])
	{
		_textureFilename = textureFilename;
		size_t size = 0;
		for (; textureFilename[size] != nullptr; size++)
		{
		}
		_size = size;

		//実体を作る
		if (_objArray != nullptr)
			return;
		_objArray = new VertexPoligon3D*[_size];
		for (size_t i = 0; i < _size; i++)
		{
			_objArray[i] = new VertexPoligon3D(Lib_3D::pDirectXWindowManager->GetDevice(), textureFilename[i]);
		}
	}

	void LoderPoligonMesh::Release()
	{
		if (_objArray == nullptr)
			return;
		for (size_t i = 0; i < _size; i++)
		{
			delete _objArray[i];
			_objArray[i] = nullptr;
		}
		delete _objArray;
		_objArray = nullptr;
		_size = 0;
		_textureFilename = nullptr;
	}




	VertexPoligon3D * LoderPoligonMesh::GetMeshData(wchar_t * fileName)
	{
		int  result;
		for (size_t i = 0; i < _size; i++)
		{
			result = wcscmp(_textureFilename[i], fileName);
			if (result == 0)//同じなら
				return _objArray[i];
		}
		return nullptr;
	}














	//+ レンダラー

	RenderPoligonMesh::RenderPoligonMesh(const VertexPoligon3D* vertexData,
		const DirectX::XMFLOAT3 vertex[3], const DirectX::XMFLOAT2 texpos[3], const DirectX::XMFLOAT3 * pos, const DirectX::XMVECTOR * orientation, const DirectX::XMFLOAT3 * scale)
		: _pos(pos)
		, _vertex(vertex)
		, _texpos(texpos)
		, _orientation(orientation)
		, _scale(scale)
		, _obj(Lib_3D::pDirectXWindowManager->GetDevice(), const_cast<VertexPoligon3D *>(vertexData))
	{
	}

	//void RenderPoligonMesh::Initialize()
	//{
	//
	//}

	//bool RenderPoligonMesh::Load(const wchar_t * fbx_filename)
	//{
	//	_obj.Load(GetDevice(), fbx_filename);
	//	bLoad = true;
	//	return true;
	//}
	void RenderPoligonMesh::VertexChange_(VertexPoligon3D* vertexPoligon3D)
	{
		_obj.VertexChange(vertexPoligon3D);
		bLoad = true;
	}

	//const Poligon3D& RenderPoligonMesh::GetObj()
	//{
	//	return _obj;
	//}

	//void RenderPoligonMesh::SetMesh(RenderPoligonMesh & org)
	//{
	//	*this = org;
	//	bLoad = false;
	//}


	//void RenderPoligonMesh::Release()
	//{
	//	if (_obj)
	//	{
	//		delete _obj;
	//		_obj = nullptr;
	//		bLoad = false;
	//	}
	//}

	void RenderPoligonMesh::SetWorldView(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection)
	{
		_view = view;
		_projection = projection;
	}

	void RenderPoligonMesh::Render()
	{
		//モデルが無ければ描画処理を一切行わない

		SetWorldView(pCameraManager->GetProjection(), pCameraManager->GetView_());

		DirectX::XMMATRIX matrix = GetWorldMatrix();
		//	Matrix -> Float4x4 変換
		DirectX::XMFLOAT4X4 world_view_projection;
		DirectX::XMFLOAT4X4 world;
		DirectX::XMStoreFloat4x4(&world, matrix);
		DirectX::XMStoreFloat4x4(&world_view_projection, matrix * _projection * _view);

		//描画
		_obj.RenderPoligon3D(Lib_3D::pDirectXWindowManager->GetDeviceContext(), world_view_projection, world, _vertex, _texpos);
	}

	DirectX::XMMATRIX RenderPoligonMesh::GetWorldMatrix()
	{
		//軸回転角度変換
		DirectX::XMMATRIX R = DirectX::XMMatrixRotationQuaternion(*_orientation);
	/*	DirectX::XMMATRIX R = DirectX::XMMatrixRotationX(0);
		R *= DirectX::XMMatrixRotationY(0);
		R *= DirectX::XMMatrixRotationZ(0);*/
		DirectX::XMMATRIX T = DirectX::XMMatrixTranslation(_pos->x, _pos->y, _pos->z);
		DirectX::XMMATRIX S = DirectX::XMMatrixScaling(_scale->x, _scale->y, _scale->z);
		DirectX::XMMATRIX matrix = DirectX::XMMatrixIdentity();
		matrix *= R*S*T;
		return matrix;
	}
}


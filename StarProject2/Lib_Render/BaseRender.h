#pragma once
//#include "../Lib_Vassel\BaseClass.h"
//#include "../Lib_Base/Template.h"

namespace Lib_Render {
	//! @breif ローダー
	class BaseLoder
	{
	public:
	};

	//! @brief 描画オブジェクトのデータ
	//! @breif レンダラーの基底クラス
	class BaseRender
	{
	public:
		virtual void Render() = 0;
	};
}

//
////! @brief 描画オブジェクト管理クラス
//class ObjRenderManager : public Singleton<BaseRenderer>
//{
//protected:
//public:
//	ObjRenderManager();
//	~ObjRenderManager();
//	void Add();
//	void Update() override;
//protected:
//};
